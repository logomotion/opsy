<?php

	require_once 'core/config/configuration.php';

	$slConfig = new SLConfig;

	/* Init DB */

	$dbh = new PDO('mysql:host=' . $slConfig->mysql_host . ';dbname=' . $slConfig->mysql_database, $slConfig->mysql_user, $slConfig->mysql_password, array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

	$now = new DateTime();

	$fileName = __DIR__ .'/../shared/import/opsylium_'. $now->format('d-m-Y') .'.xml';

	echo $fileName;

	if (file_exists($fileName)) {

		/* Erase DB */

		$stmt = $dbh->prepare('TRUNCATE TABLE biens');
		$stmt->execute();
		$stmt = $dbh->prepare('TRUNCATE TABLE lots');
		$stmt->execute();

		/* Import */

	    $biens = simplexml_load_file($fileName);

	    for ($i = 0; $i < count($biens->bien); $i++) {

	    	$photos = [];

			$stmt = $dbh->prepare('INSERT INTO biens (id, mandat, transaction, type, ville, codepostal, prix, surface, piece, chambre, descriptif, statut_copropriete, nblots_copropriete, valeur_conso_energetique, lettre_conso_energetique, valeur_gaz_effet_serre, lettre_gaz_effet_serre, photos, nom_programme, livraison, dispositif_fiscal, fiscalite, carte, coupdecoeur, statut, home, promo, titrepromo, fichier) VALUES (:id, :mandat, :transaction, :type, :ville, :codepostal, :prix, :surface, :piece, :chambre, :descriptif, :statut_copropriete, :nblots_copropriete, :valeur_conso_energetique, :lettre_conso_energetique, :valeur_gaz_effet_serre, :lettre_gaz_effet_serre, :photos, :nom_programme, :livraison, :dispositif_fiscal, :fiscalite, :carte, :coupdecoeur, :statut, :home, :promo, :titrepromo, :fichier)');

			$stmt->bindParam(':id', $biens->bien[$i]->attributes()[0], PDO::PARAM_NULL);
			$stmt->bindParam(':mandat', $biens->bien[$i]->mandat, PDO::PARAM_NULL);
			$stmt->bindParam(':transaction', $biens->bien[$i]->transaction, PDO::PARAM_NULL);
			$stmt->bindParam(':type', $biens->bien[$i]->type, PDO::PARAM_NULL);
			$stmt->bindParam(':ville', $biens->bien[$i]->ville, PDO::PARAM_NULL);
			$stmt->bindParam(':codepostal', $biens->bien[$i]->codepostal, PDO::PARAM_NULL);
			$stmt->bindParam(':prix', $biens->bien[$i]->prix, PDO::PARAM_NULL);
			$stmt->bindParam(':surface', $biens->bien[$i]->surface, PDO::PARAM_NULL);
			$stmt->bindParam(':piece', $biens->bien[$i]->piece, PDO::PARAM_NULL);
			$stmt->bindParam(':chambre', $biens->bien[$i]->chambre, PDO::PARAM_NULL);
			$stmt->bindParam(':descriptif', $biens->bien[$i]->descriptif, PDO::PARAM_NULL);
			$stmt->bindParam(':statut_copropriete', $biens->bien[$i]->statut_copropriete, PDO::PARAM_NULL);
			$stmt->bindParam(':nblots_copropriete', $biens->bien[$i]->nblots_copropriete, PDO::PARAM_NULL);
			$stmt->bindParam(':valeur_conso_energetique', $biens->bien[$i]->valeur_conso_energetique, PDO::PARAM_NULL);
			$stmt->bindParam(':lettre_conso_energetique', $biens->bien[$i]->lettre_conso_energetique, PDO::PARAM_NULL);
			$stmt->bindParam(':valeur_gaz_effet_serre', $biens->bien[$i]->valeur_gaz_effet_serre, PDO::PARAM_NULL);
			$stmt->bindParam(':lettre_gaz_effet_serre', $biens->bien[$i]->lettre_gaz_effet_serre, PDO::PARAM_NULL);
			$stmt->bindParam(':nom_programme', $biens->bien[$i]->nom_programme, PDO::PARAM_NULL);
			$stmt->bindParam(':livraison', $biens->bien[$i]->livraison, PDO::PARAM_NULL);
			$stmt->bindParam(':dispositif_fiscal', $biens->bien[$i]->dispositif_fiscal, PDO::PARAM_NULL);
			$stmt->bindParam(':fiscalite', $biens->bien[$i]->fiscalite, PDO::PARAM_NULL);
			$stmt->bindParam(':carte', $biens->bien[$i]->carte, PDO::PARAM_NULL);
			$stmt->bindParam(':coupdecoeur', $biens->bien[$i]->coupdecoeur, PDO::PARAM_NULL);
			$stmt->bindParam(':statut', $biens->bien[$i]->statut, PDO::PARAM_NULL);
			$stmt->bindParam(':home', $biens->bien[$i]->home, PDO::PARAM_NULL);
			$stmt->bindParam(':promo', $biens->bien[$i]->promo, PDO::PARAM_NULL);
			$stmt->bindParam(':titrepromo', $biens->bien[$i]->titrepromo, PDO::PARAM_NULL);

			$fichier = (string) $biens->bien[$i]->fichiers->fichier[0];
			$stmt->bindParam(':fichier', $fichier, PDO::PARAM_NULL);

			for ($x = 0; $x < count($biens->bien[$i]->photos->photo); $x++) {

				$photos[] = (string) $biens->bien[$i]->photos->photo[$x];

			}

			if (count($photos) === 0) {

				$photos[] = 'templates/opsylium/images/fallback.png';

			}

			$stmt->bindParam(':photos', json_encode($photos), PDO::PARAM_NULL);

			$stmt->execute();

			for ($y = 0; $y < count($biens->bien[$i]->lots->lot); $y++) {

				$stmt = $dbh->prepare('INSERT INTO lots (id_bien, type, quantite, piece, surface, prix) VALUES (:id_bien, :type, :quantite, :piece, :surface, :prix)');

				$stmt->bindParam(':id_bien', $biens->bien[$i]->attributes()[0]);
				$stmt->bindParam(':type', $biens->bien[$i]->lots->lot[$y]->type);
				$stmt->bindParam(':quantite', $biens->bien[$i]->lots->lot[$y]->quantite);
				$stmt->bindParam(':piece', $biens->bien[$i]->lots->lot[$y]->piece);
				$stmt->bindParam(':surface', $biens->bien[$i]->lots->lot[$y]->surface);
				$stmt->bindParam(':prix', $biens->bien[$i]->lots->lot[$y]->prix);

				$stmt->execute();

			}

		}

	} else {

	    exit('Echec lors de l\'ouverture du fichier ' . $fileName);

	}