<?php
/**
* @package		SLASH-CMS
* @version		index.php - Version 11.5.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//include core

include ("core/slash.php");


$slash = new Slash ();
$slash->show(); //show front office
?>
