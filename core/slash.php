<?php
/**
* @package		SLASH-CMS
* @subpackage	CORE
* @internal     Slash core system
* @version		slash_core.php - Version 12.3.1
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Includes
include ("config/configuration.php"); // configuration file.
include ("languages/sl_lang.php"); // System Language

class Slash {

/**
* --------------------
* - CLASS PROPERTIES -
* --------------------
*/

	public $config = array(); //Configuration in database
	public $get_params = array(); //Get param
	public $post_params = array(); //Post param
	public $db_handle; //Slash Database Handle
	
	private $properties = array(); //Properties in file configuration.php
	private $modules = array(); //module Array
	private $request_module; //Request module
	private $mode; //Mode (admin or site)
	public $mobile; //mobile dectection class (Platforms, etc..)
	
/**
* ---------------
* - OVERLOADING -
* ---------------
*/
	
	/**
	* Constructor
	*/
	function __construct(){
		
	}
	
	/**
	* Overloading for Slash properties (configuration.php)
	*/
	public function __get($name) {
        if (array_key_exists($name, $this->properties)) {
            return $this->properties[$name];
        }
		
		//@todo : else ?
	}
	
	
/**
* -------------------
* - PRIVATE METHODS -
* -------------------
*/

	/**
	* Core front initialisation
	*/
	private function initialise() {
		
		session_start();
		
		$this->load_properties(); // load properties configuration
		$this->load_common(); // load interfaces and class
		
		//database connection
		$this->connect($this->mysql_host,$this->mysql_database,$this->mysql_user,$this->mysql_password);
		$this->load_config(); // load configuration
		$this->load_language(); // load language (core/languages/LANGUAGE SELECTED/)
		$this->load_params(); // load get and post params (load all get and post params)
		$this->mobile_detection();// mobile platforms detection
		$this->load_template($this->config["site_template_url"]."template.php"); // load front template
		$this->disconnect();
	}

	/**
	* Core backoffice initialisation
	*/
	private function initialise_admin() {
		
		session_start();
		
		$this->load_properties(); // load properties configuration
		$this->load_common(); // load interfaces and class
		
		//database connection
		$this->connect($this->mysql_host,$this->mysql_database,$this->mysql_user,$this->mysql_password);
		$this->load_config(); // load configuration
		$this->load_language(); // load language (core/languages/LANGUAGE SELECTED/)
		$this->load_params(); // load get and post params (load all get and post params)
		
		$this->load_template($this->config["admin_template_url"]."template.php"); // load admin template
		$this->disconnect();
	}

	/**
	* MySql Database connexion function
	* @param $host host server
	* @param $database database name
	* @param $user MySql user
	* @param $password MySql password
	*/
	private function connect ($host, $database, $user, $password ) {

		$this->db_handle = @mysql_connect($host, $user, $password) or $this->show_fatal_error("CONNEXION_ERROR",mysql_error());
		$db_selected = mysql_select_db($database, $this->db_handle);

		if (!$db_selected) {
			//show fatal error and quit
			$this->show_fatal_error("SELECT_DB_ERROR",mysql_error());
		}

	}

	/**
	* MySql Database disconnexion function
	*/
	private function disconnect () {
		mysql_close($this->db_handle);
	}

	
	
	/**
	* Loading Slash properties in SLConfig class
	*/
	private function load_properties() {
		$sl_config = new SLConfig();
		$class_vars = get_class_vars(get_class($sl_config));

		foreach ($class_vars as $name => $value) {
			$this->properties[$name] = $value;
		}	
	}
	
	
	
	/**
	* Loading Slash configuration
	*/
	private function load_config() {
		$result = mysql_query("SELECT * FROM ".$this->database_prefix."config",$this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
		    $this->config[$row["config_name"]] = $row["config_value"];
		}
	}
	
	


	/**
	* Loading Slash language
	*/
	private function load_language() {
		if (isset($_SESSION["user_language"]) ) {
			$url_language = "languages/".$_SESSION["user_language"];
		}else{
			$url_language = "languages/".$this->config["slash_language"];
		}
		
		if ($this->mode == "admin" && file_exists("../core/".$url_language."/interface.php") ) { 
			include ($url_language."/interface.php"); //include interface traduction
		}
		
		if ($this->mode == "site" && file_exists("core/".$url_language."/interface.php") ) { 
			include ($url_language."/interface.php"); //include interface traduction

		}
	}

	/**
	* Is a mobile platforms ?
	*/
	private function mobile_detection() {
		if ($this->config["mobile_detection"] == "true" && $this->sl_param("desktop") != 1) {
			$this->mobile = new sl_mobile();
			if ($this->mobile->isMobile() == true || $this->sl_param("mobile") == 1) {
				$this->config["site_template_url"] = $this->config["mobile_template_url"];
			}
		}else{
			$this->mobile = false;
		}
	}
	
	/**
	* Load template
	* @param $url url_template
	*/
	private function load_template($url) {
		if (file_exists($url)){
		    include ($url);
		}else{
		    $this->show_fatal_error("UNKNOWN_TEMPLATE_ERROR","No such template '$url'");
		}
	}



	/**
	* Load GET and POST params
	*/
	private function load_params() {
		
		// GET params
		foreach ( $_GET as $get => $val )  {           
		$this->get_params[$get] = $val;
		}   
		
		// POST params
		foreach ( $_POST as $post => $val )  {           
		$this->post_params[$post] = $val;
		}   
	}
	
	
	/**
	* Load module traduction
	*/
	private function load_module_language($module_url) {
	
		if ($this->mode == "admin") {
		
			if (isset($_SESSION["user_language"])) {
				$url = $module_url."languages/".$_SESSION["user_language"].".php";
			}else{
				$url = $module_url."languages/".$this->config["slash_language"].".php";
			}
			
			if (file_exists($url)){
				include ($url);
			}
		}
		
		/* @todo : Front mutli-language */
		if ($this->mode == "site") { 
		
			if (isset($_SESSION["user_language"])) {
				$url = $module_url."languages/".$_SESSION["user_language"].".php";
			}else{
				$url = $module_url."languages/".$this->config["slash_language"].".php";
			}
			
			if (file_exists($url)){
				include ($url);
			}
			
		}
	}
	
	/**
	* Load Module interface
	*/
	private function load_common(){
	
		$this->load_implements();
		$this->load_class();
	}
	
	/**
	* Load Module interface
	*/
	private function load_implements(){
	
		include ("common/implements/modules/imodel.php");
		include ("common/implements/modules/iview.php");
		include ("common/implements/modules/icontroller.php");
	
		if ($this->mode == "site") { 
			//nothing
		}
		
		if ($this->mode == "admin") { 
			//nothing
		}
	}
	
	/**
	* Load Module 
	*/
	private function load_class(){
		
		include ("common/class/functions/sl_functions.php"); // load functions
		
		if ($this->mode == "site") { 
			include ("common/class/modules/sl_model.php"); // load abstract class
			include ("common/class/modules/sl_view.php"); // load abstract class
			include ("common/class/modules/sl_controller.php"); // load abstract class
		}
		
		if ($this->mode == "admin") { 
			include ("common/class/modules/sla_model.php"); // load abstract class
			include ("common/class/modules/sla_view.php"); // load abstract class
			include ("common/class/modules/sla_controller.php"); // load abstract class
			
		}
		
		
	}

/*
* ------------------
* - PUBLIC METHODS -
* ------------------
*/
	
	/**
	* Show Front-Office
	*/
	public function show() {
		$this->mode = "site";
		$this->initialise();
	}

	/**
	* Show Admin - Back-Office
	*/
	public function show_admin () {
		$this->mode = "admin";
		$this->initialise_admin ();
	}
	
	
	/**
	* Show fatal errors and quit
	* @param $message error message
	* @param $code technical message error
	*/
	public function show_fatal_error ($message,$code) {	
		echo "<br /><table style='border: 1px solid #FF0000;' align='center'><tr><td>";
		echo "<font color='#FF0000' size='2'>".constant($message)." - ERROR CODE : ".$code."</font>";
		echo "</td></tr></table>";
		exit;
	}
	
	
	/**
	 * User admin infos
	 * @return $row_user:Array Return User datas
	 */
	public function get_admin_infos (){
	    if ($this->mode == "admin" && $_SESSION["id_user"] != null) {   
		$result_user = mysql_query("SELECT * FROM ".$this->database_prefix."users WHERE id=".$_SESSION["id_user"],$this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		$row_user = mysql_fetch_array($result_user, MYSQL_ASSOC);
		return $row_user;
	    } else { return null; }
	}
	
	/**
	 * Active lang infos
	 * @return $row_lang:Array Return lang datas
	 */
	public function get_active_lang (){ 
		$result_lang = mysql_query("SELECT * FROM ".$this->database_prefix."lang WHERE enabled=1 ORDER BY id" ,$this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		$lang = array();
		while ($row_lang = mysql_fetch_array($result_lang, MYSQL_ASSOC)){
			array_push($lang,$row_lang);
		}
		return $lang;
	}
	
	
	/**
	* Word traduction
	* @return $word:string Word traduction
	*/
	public function trad_word($message) {
		$myword = $message;//."_".strtoupper($this->config["slash_language"]);
		if (defined($myword)) {
			return constant($myword); 
		}else{
			return "- NO TRANSLATE -";
		}
	}
	
	
	
	/**
	* Return GET or POST param
	* @param $name nome of param
	* @param $method POST, GET, or both if not defined
	*/
	public function sl_param($name,$method=null) {
		$value=null;
		if ($method) {
			if ($method == "POST") {
				if (array_key_exists($name, $this->post_params)) {
				$value = $this->post_params[$name];
				}	
			}
			if ($method == "GET") {
				if (array_key_exists($name, $this->get_params)) {
					$value = $this->get_params[$name];
				}
			}
		} else {
			if (array_key_exists($name, $this->get_params)) {
				$value = $this->get_params[$name];
			}
			if (array_key_exists($name, $this->post_params)) {
				$value = $this->post_params[$name];
			}
		}
		return $value;
	}
	
	
	/**
	* Return slash config value of a param
	* @return value of configuration
	*/
	public function sl_config($name) {
		return $this->config[$name];
	}
	
	
	/**
	* Return id module
	*/
	public function sl_module_id($name,$type=0) {
	
		if ($type == 0) { 
			$result_module = mysql_query("SELECT * FROM ".$this->database_prefix."modules WHERE name='".$name."'", $this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		}else{
			$result_module = mysql_query("SELECT * FROM ".$this->database_prefix."modules WHERE name='".$name."' AND type='".$type."'", $this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		}
		
		if (mysql_num_rows($result_module) > 0) {
			$row_module = mysql_fetch_array($result_module, MYSQL_ASSOC);	
			return $row_module["id"];
		}else{
			return false;
		}
	}
	
	/**
	* Modules initialise function
	* @todo Reste le chargement puis l'intégration des paramètres des modules
	*/
	public function initialise_modules() {
		
		$result = mysql_query("SELECT *, IF(initialise_order = 0, '1', '0') AS prio FROM ".$this->database_prefix."modules WHERE enabled=1 AND type='".$this->mode."' ORDER by prio ASC, initialise_order ASC",$this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {			
		
			if ($row["initialise_order"] != "0") { //Global module
				$this->load_module_language($row["url"]);
				
				if (file_exists($row["url"].$row["name"].".php")){
					$module_url = $row["url"].$row["name"].".php";
					$module_class = $row["name"];
				}elseif(file_exists($row["url"]."controller.php")){
					$module_url = $row["url"]."controller.php";
					$module_class = $row["name"]."_controller";
				}else{
					$this->show_fatal_error("UNKNOWN_MODULE_ERROR","No such module '".$row["name"]."'");
				}
				
				include ($module_url);
				$this->modules[$row["name"]] = new $module_class($this,$row["id"]);
				$this->modules[$row["id"]] = $this->modules[$row["name"]];
				//$this->modules[$row["id"]]->params = explode (",",$row["params"]);
				$this->modules[$row["name"]]->initialise();

			}else{ //Current module
				if ($this->sl_param("mod") && $this->sl_param("mod")==$row["name"] ) {
					$this->load_module_language($row["url"]);
					
					if (file_exists($row["url"].$row["name"].".php")){
						$module_url = $row["url"].$row["name"].".php";
						$module_class = $row["name"];
					}elseif(file_exists($row["url"]."controller.php")){
						$module_url = $row["url"]."controller.php";
						$module_class = $row["name"]."_controller";
					}else{
						$this->show_fatal_error("UNKNOWN_MODULE_ERROR","No such module '".$row["name"]."'");
					}
					
					include ($module_url);
					$this->modules[$row["name"]] = new $module_class($this,$row["id"]);
					$this->modules[$row["id"]] = $this->modules[$row["name"]];

				}
			}
		}
		
		if ($this->sl_param("mod")) {
			$name = $this->sl_param("mod");
			if (array_key_exists($name, $this->modules)) {
				$this->modules[$name]->load_header();	
			} else {
				$this->show_fatal_error("UNKNOWN_MODULE_ERROR","No such module '".$name."'");
			}
		}
		
	}
	
	/**
	* Module execution
	*/
	public function execute_modules() {
		
		if ($this->sl_param("mod")) {
			$name = $this->sl_param("mod");
			if (array_key_exists($name, $this->modules)) {
				$this->modules[$name]->load_footer();
			} else {
				$this->show_fatal_error("UNKNOWN_MODULE_ERROR","No such module '".$name."'");
			}
		}
		
		//Global module
		$result = mysql_query("SELECT * FROM ".$this->database_prefix."modules WHERE enabled=1 AND type='".$this->mode."' AND initialise_order > 0 ORDER by initialise_order ASC",$this->db_handle) or $this->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {	
			$this->modules[$row["name"]]->execute();
		}
	}
	
	/**
	* Include module
	* @param $name module name
	* @param $params params
	*/
	public function load_module($name,$params=null) {
		
		if (array_key_exists($name, $this->modules)) {
			if ($this->mode == "admin") {
				if (isset ($_SESSION["id_user"]) && $_SESSION["id_user"] != null || $name == "sla_secure") {
					$this->modules[$name]->params = $params;
					$this->modules[$name]->load();
				}
			} else {
				$this->modules[$name]->params = $params;
				$this->modules[$name]->load();
			}
		} else {
			$this->show_fatal_error("UNKNOWN_MODULE_ERROR","No such module '".$name."'");
		}
	}


	

}

?>