(function($) {

$.fn.selectList_input = function(options) {
	
	var defaults = {
		module_name: "",
		id_input: "",
		id_lang: "",
		prefix_module: "",
		data_products_id:"",
		data_products_text:"",
		data_products_content:"",
		data_products_id_content:""
	};
	
	var opts = $.extend(defaults, options);
	
	function click_on_select(){
		
		var value = $(this).attr('value');
		
		$(this).attr("disabled","disabled");
		
		if(opts.id_lang != "")
			$('div#'+opts.prefix_module+''+opts.id_input+'_'+opts.id_lang +'_'+value).show();
		else
			$('div#'+opts.prefix_module+''+opts.id_input+'_'+value).show();
	}
	
	function click_on_hide(){
		
		var value_parent = $(this).parent().attr('id');
		
		var value_exp = value_parent.split('_');
		var last_id = value_exp.length - 1;
		
		var value = value_exp[last_id];
		
		if(opts.id_lang != ""){
			new_id_lang = '_' + opts.id_lang;
		}
		
		$('select#'+opts.module_name+'_obj'+opts.id_input+new_id_lang+' option').each(function(){
																							   
			if($(this).attr("value") == value ){
				$(this).attr("disabled","");
			}
			
		});
		
		$('div#'+ opts.prefix_module + opts.id_input+new_id_lang +'_'+ value).hide();
		
		$('input#'+ opts.module_name +'_obj'+ opts.prefix_module + opts.id_input + new_id_lang +'_'+value).val('');
	}
	
	//Gestion du clic sur les options du select
	$(this).click(click_on_select);
	
	//Gestion de la cr�ation de la div avec ces contenants
	if(opts.data_products_id != "" && opts.data_products_text != ""){
		
		var data_products_id_tab = opts.data_products_id.split(',');
		var data_products_text_tab = opts.data_products_text.split(',');
		
		var data_products_content_tab = opts.data_products_content.split(',');
		var data_products_id_content_tab = opts.data_products_id_content.split(',');
		
		var longueur = data_products_id_tab.length-1;
		var longueur_content = data_products_id_content_tab.length-1;
		
		var content_of_data = "";
		for(i=longueur-1;i>=0;i--){
			
			content_of_data = "";
			for(j=0;j<longueur_content;j++){
				if(data_products_id_content_tab[j] == data_products_id_tab[i]){
					content_of_data = data_products_content_tab[j];
				}
			}
			
			$(this).parent().after("<div id='"+opts.prefix_module+opts.id_input+"_"+opts.id_lang+"_"+data_products_id_tab[i]+"' class='data_products_list'><span class='sl_form_title'>"+data_products_text_tab[i]+" : </span><input id='"+opts.module_name+"_obj_id"+opts.prefix_module+opts.id_input+"_"+opts.id_lang+"_"+data_products_id_tab[i]+"' name='"+opts.module_name+"_obj_id"+opts.prefix_module+opts.id_input+"_"+opts.id_lang+"_"+data_products_id_tab[i]+"' type='hidden' class='sl_form_input' size='80' value='"+data_products_id_tab[i]+"' /><input id='"+opts.module_name+"_obj"+opts.prefix_module+opts.id_input+"_"+opts.id_lang+"_"+data_products_id_tab[i]+"' name='"+opts.module_name+"_obj"+opts.prefix_module+opts.id_input+"_"+opts.id_lang+"_"+data_products_id_tab[i]+"' type='text' class='sl_form_input' size='80' value='"+content_of_data+"' /><img src='../core/plugins/jquery_plugins/selectlist_input/images/cross-circle.png' id='"+opts.module_name+"_img"+opts.id_input+"_"+opts.id_lang+"' /></div>");
			
			if(content_of_data != ""){
				$('div#'+opts.prefix_module+opts.id_input+'_'+opts.id_lang+'_'+data_products_id_tab[i]).show();
				$('select#'+opts.module_name+'_obj'+opts.id_input+'_'+opts.id_lang+' option').each(function(){
																							   
					if($(this).attr("value") == data_products_id_tab[i] ){
						$(this).attr("disabled","disabled");
					}
					
				});
			}
		}
		
	}
	
	//Gestion du clic sur la div
	$("img#"+opts.module_name+"_img"+opts.id_input+"_"+opts.id_lang).click(click_on_hide);
	
	return $(this);
	
};
})(jQuery);