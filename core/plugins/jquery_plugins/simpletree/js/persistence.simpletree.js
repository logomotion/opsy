/*
* jQuery SimpleTree Drag&Drop plugin
* Update on 22th May 2008
* Version 0.3
*
* Licensed under BSD <http://en.wikipedia.org/wiki/BSD_License>
* Copyright (c) 2008, Peter Panov <panov@elcat.kg>, IKEEN Group http://www.ikeen.com
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*     * Redistributions of source code must retain the above copyright
*       notice, this list of conditions and the following disclaimer.
*     * Redistributions in binary form must reproduce the above copyright
*       notice, this list of conditions and the following disclaimer in the
*       documentation and/or other materials provided with the distribution.
*     * Neither the name of the Peter Panov, IKEEN Group nor the
*       names of its contributors may be used to endorse or promote products
*       derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY Peter Panov, IKEEN Group ``AS IS'' AND ANY
* EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
* WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
* DISCLAIMED. IN NO EVENT SHALL Peter Panov, IKEEN Group BE LIABLE FOR ANY
* DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
* (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
* LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
* ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
* (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
* SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*/

$.fn.simpleTree = function(opt)
{
	return this.each(function()
	{
		var TREE = this;
		var ROOT = $('.root',this);
		var mousePressed = false;
		var mouseMoved = false;
		var dragMoveType = false;
		var dragNode_destination = false;
		var dragNode_source = false;
		var dragDropTimer = false;
		var ajaxCache = Array();
		var cookieTimer = null;

		/**
		* Our options
		*/
		TREE.option = 
		{
			drag:					true,
			animate:				false,
			autoclose:				false,
			speed:					'fast',
			afterAjax:				false,
			afterMove:				false,
			afterClick:				false,
			afterDblClick:			false,
			// added by Erik Dohmen (2BinBusiness.nl) to make context menu cliks available
			afterContextMenu:		false,
			docToFolderConvert:		false,
			//	Added by Jerry Ablan (jerry.ablan@gmail.com) to enable custom node templates
			nodeTemplate:			'<li><ul><li id="%_id_%"><span>%_value_%</span></li></ul></li>',
			lastLineTemplate:		'<ul><li class="line-last"></li></ul>',
			spacerLineTemplate:		'<li class="line">&nbsp;</li>',
			//	Added by Jerry Ablan (jerry.ablan@gmail.com) to do custom classes, format = [{id: 'class'}]
			classMap: { 
				//	Custom
				user_folder_id_0: 'users',		//	Exact match
				user_id_: 'user'				//	Wildcard. Equates to id of 'user_id_*'
			},
			cookieId:				'simpleTree',		//	state storage
			afterPlusMinusClick:	false
		};
		
		/**
		* Adds the options...
		*/
		TREE.option = $.extend( TREE.option, opt );

		/**
		* Adds a getSelected function to the TREE		
		*/
		$.extend( this, 
		{
			getSelected: 
				function()
				{
					return $( 'span.active', this ).parent();
				}
			}
		);
		
		/**
		* If autoClose is true, this function will close all nodes on the same level
		*/
		TREE.closeNearby = function( obj )
		{
			$(obj)
			.siblings()
			.filter( '.folder-open, .folder-open-last'.replace( 'folder', TREE.mapUserClass( $(this).attr('id'), 'folder' ) ) )
			.each( 
				function()
				{
					var childUl = $('>ul',this);
					$(this).swapClass( 'open', 'close' );
					
					if( TREE.option.animate )
						childUl.animate( { height:"toggle" }, TREE.option.speed );
					else
						childUl.hide();
				}
			);
		};
		
		/**
		* Toggles a node open/closed
		*/
		TREE.nodeToggle = function( obj, bDeserialize )
		{
			var childUl = $('>ul', obj);
			
			if ( cookieTimer ) clearTimeout( cookieTimer );
			
			if ( childUl.is( ':visible' ) )
			{
				//	Replace all 'open' with 'close'
				$(obj).swapClass( 'open', 'close' );

				if ( TREE.option.animate )
					childUl.animate( {height:"toggle"}, TREE.option.speed );
				else
					childUl.hide();
			}
			else
			{
				//	Replace all 'close' with 'open'
				$(obj).swapClass( 'close', 'open' );

				if ( TREE.option.animate )
				{
					childUl.animate( { height:"toggle" }, TREE.option.speed, 
						function()
						{
							if ( true !== bDeserialize )
							{
								if ( TREE.option.autoclose ) TREE.closeNearby( obj );
								if ( childUl.is( '.ajax' ) ) TREE.setAjaxNodes( childUl, obj.id );
							}
						}
					);
				}
				else
				{
					childUl.show();
					if ( true !== bDeserialize )
					{
						if ( TREE.option.autoclose ) TREE.closeNearby( obj );
						if ( childUl.is( '.ajax' ) ) TREE.setAjaxNodes( childUl, obj.id );
					}
				}
			}

			cookieTimer = window.setTimeout( TREE.saveCookie, 7000 );
		};
		
		TREE.setAjaxNodes = function( node, parentId, callback )
		{
			if ( $.inArray( parentId, ajaxCache ) == -1 )
			{
				var url = $.trim( $('>li', node).text() );

				ajaxCache[ ajaxCache.length ] = parentId;
				
				if ( url && url.indexOf( 'url:' ) )
				{
					url = $.trim( url.replace(/.*\{url:(.*)\}/i ,'$1' ) );
					
					$.ajax(
					{
						type: 			"GET",
						url: 			url,
						contentType:	'html',
						cache:			false,
						success: 		
							function( response )
							{
								node.removeAttr( 'class' );
								node.html( response );
								$.extend( node, { url: url } );
								TREE.setTreeNodes( node, true );

								if ( typeof TREE.option.afterAjax == 'function' ) TREE.option.afterAjax( node );
								if ( typeof callback == 'function' ) callback( node );
							}
					});
				}
			}
		};
		
		TREE.setTreeNodes = function( obj, useParent, arOpen )
		{
			obj = useParent ? obj.parent() : obj;

			$('li>span', obj ).addClass( 'text' )
			.bind( 'selectstart', 
				function() { return false; }
			)
			.click( 
				function()
				{
					$( '.active', TREE ).attr( 'class', 'text' );
					if ( $(this).className == 'text' ) $(this).className = 'active';
					if ( typeof TREE.option.afterClick == 'function' ) if ( ! TREE.option.afterClick( $(this).parent() ) ) TREE.saveCookie();
					return false;
				}
			)
			.dblclick( function()
			{
				mousePressed = false;
				TREE.nodeToggle( $(this).parent().get( 0 ) );
				if ( typeof TREE.option.afterDblClick == 'function' ) TREE.option.afterDblClick($(this).parent());
				
				return false;
			}) // added by Erik Dohmen (2BinBusiness.nl) to make context menu actions available
			.bind( "contextmenu", function()
			{
				$('.active',TREE).attr( 'class', 'text' );
				if ( this.className == 'text' ) this.className = 'active';
				if ( typeof TREE.option.afterContextMenu == 'function' ) TREE.option.afterContextMenu( $(this).parent() );
				
				return false;
			})
			.mousedown( function( event )
			{
				mousePressed = true;
				cloneNode = $(this).parent().clone();
				var LI = $(this).parent();
				
				if ( TREE.option.drag )
				{
					$('>ul', cloneNode).hide();
					$('body').append( '<div id="drag_container"><ul></ul></div>' );
					$('#drag_container').hide().css( { opacity: '0.8' } );
					$('#drag_container >ul').append( cloneNode );
					$("<img>").attr( {
						id: "tree_plus",
						src: "images/plus.gif"
					})
					.css({
						width: "7px",
						display: "block",
						position: "absolute",
						left: "5px",
						top: "5px", 
						display:'none'
					})
					.appendTo( "body" );
					
					$(document).bind( "mousemove", {LI:LI}, TREE.dragStart).bind( "mouseup", TREE.dragEnd );
				}
				
				return false;
			})
			.mouseup(function()
			{
				if ( mousePressed && mouseMoved && dragNode_source ) TREE.moveNodeToFolder( $(this).parent() );
				TREE.eventDestroy();
			});
			
			$('li', obj).each(function( i )
			{
				var className = this.className;
				var open = false;
				var cloneNode = false;
				var LI = this;
				var childNode = $( '>ul', this );
				var sFolderClass = TREE.mapUserClass( $(this).attr('id'), 'folder' );
				var sDocClass = TREE.mapUserClass( $(this).attr('id'), 'doc' );
				
				if ( ! childNode.size() )
					$(this).addClass( sDocClass + ( $(this).is( ':last-child' ) ? '-last' : '' ) );
				else
				{
					//	Is this supposed to be open?
					if ( ( arOpen != undefined && -1 != $.inArray( $(this).attr('id'), arOpen ) ) || className.indexOf( 'open' ) != -1 )
					{
						sFolderClass += '-open';
						open = true;
					}
					else
					{
						sFolderClass += '-close';
					}
					
					this.className = sFolderClass + ( $(this).is(':last-child') ? '-last' : '' );

					if ( ! open || className.indexOf('ajax') != -1 ) childNode.hide();

					TREE.setTrigger( this );
				}
            })
            .before( TREE.spacerLineTemplate )
			.filter( ':last-child' )
			.after( TREE.lastLineTemplate );
			
			TREE.setEventLine( $( '.line, .line-last', obj ) );
		};
		
		TREE.setTrigger = function(node)
		{
			$('>span',node).before('<img class="trigger" src="images/spacer.gif" border=0>');
			
			var trigger = $( '>.trigger', node );
			
			trigger.click(function( event )
			{
				TREE.nodeToggle( node );
				if ( typeof TREE.option.afterPlusMinusClick == 'function' ) TREE.option.afterPlusMinusClick( node );
			});
			
			if ( !$.browser.msie )
			{
				trigger.css( 'float', 'left' );
			}
		};
		
		TREE.dragStart = function(event)
		{
			var LI = $(event.data.LI);
			if(mousePressed)
			{
				mouseMoved = true;
				if(dragDropTimer) clearTimeout(dragDropTimer);
				if($('#drag_container:not(:visible)')){
					$('#drag_container').show();
					LI.prev('.line').hide();
					dragNode_source = LI;
				}
				$('#drag_container').css({position:'absolute', "left" : (event.pageX + 5), "top": (event.pageY + 15) });
				if(LI.is(':visible'))LI.hide();
				var temp_move = false;
				if(event.target.tagName.toLowerCase()=='span' && $.inArray(event.target.className, Array('text','active','trigger'))!= -1)
				{
					var parent = event.target.parentNode;
					var offs = $(parent).offset({scroll:false});
					var screenScroll = {x : (offs.left - 3),y : event.pageY - offs.top};
					var isrc = $("#tree_plus").attr('src');
					var ajaxChildSize = $('>ul.ajax',parent).size();
					var ajaxChild = $('>ul.ajax',parent);
					screenScroll.x += 19;
					screenScroll.y = event.pageY - screenScroll.y + 5;

					if(parent.className.indexOf('folder-close')>=0 && ajaxChildSize==0)
					{
						if(isrc.indexOf('minus')!=-1)$("#tree_plus").attr('src','images/plus.gif');
						$("#tree_plus").css({"left": screenScroll.x, "top": screenScroll.y}).show();
						dragDropTimer = setTimeout(function(){
							parent.className = parent.className.replace('close','open');
							$('>ul',parent).show();
						}, 700);
					}else if(parent.className.indexOf('folder')>=0 && ajaxChildSize==0){
						if(isrc.indexOf('minus')!=-1)$("#tree_plus").attr('src','images/plus.gif');
						$("#tree_plus").css({"left": screenScroll.x, "top": screenScroll.y}).show();
					}else if(parent.className.indexOf('folder-close')>=0 && ajaxChildSize>0)
					{
						mouseMoved = false;
						$("#tree_plus").attr('src','images/minus.gif');
						$("#tree_plus").css({"left": screenScroll.x, "top": screenScroll.y}).show();

						$('>ul',parent).show();
						/*
							Thanks for the idea of Erik Dohmen
						*/
						TREE.setAjaxNodes(ajaxChild,parent.id, function(){
							parent.className = parent.className.replace('close','open');
							mouseMoved = true;
							$("#tree_plus").attr('src','images/plus.gif');
							$("#tree_plus").css({"left": screenScroll.x, "top": screenScroll.y}).show();
						});

					}else{
						if(TREE.option.docToFolderConvert)
						{
							$("#tree_plus").css({"left": screenScroll.x, "top": screenScroll.y}).show();
						}else{
							$("#tree_plus").hide();
						}
					}
				}else{
					$("#tree_plus").hide();
				}
				return false;
			}
			return true;
		};
		
		TREE.dragEnd = function()
		{
			if ( dragDropTimer ) clearTimeout( dragDropTimer );
			if ( cookieTimer ) clearTimeout( cookieTimer );
			TREE.eventDestroy();

			//	Store new visibles
			cookieTimer = window.setTimeout( TREE.saveCookie, 7000 );
		};
		
		TREE.setEventLine = function(obj)
		{
			obj.mouseover(function(){
				if(this.className.indexOf('over')<0 && mousePressed && mouseMoved)
				{
					this.className = this.className.replace('line','line-over');
				}
			}).mouseout(function(){
				if(this.className.indexOf('over')>=0)
				{
					this.className = this.className.replace('-over','');
				}
			}).mouseup(function(){
				if(mousePressed && dragNode_source && mouseMoved)
				{
					dragNode_destination = $(this).parents('li:first');
					TREE.moveNodeToLine(this);
					TREE.eventDestroy();
				}
			});
		};
		
		TREE.checkNodeIsLast = function( node )
		{
			if ( node.className.indexOf( 'last' ) >= 0 )
			{
				var prev_source = dragNode_source.prev().prev();
				if ( prev_source.size() > 0 ) prev_source[0].className += '-last';
				node.className = node.className.replace('-last','');
			}
		};
		
		TREE.checkLineIsLast = function( line )
		{
			if ( line.className.indexOf('last') >= 0 )
			{
				var prev = $(line).prev();
				if ( prev.size() > 0 ) prev[ 0 ].className = prev[ 0 ].className.replace( '-last','' );
				dragNode_source[ 0 ].className += '-last';
			}
		};
		
		TREE.eventDestroy = function()
		{
			// added by Erik Dohmen (2BinBusiness.nl), the unbind mousemove TREE.dragStart action
			// like this other mousemove actions binded through other actions ain't removed (use it myself
			// to determine location for context menu)
			$(document).unbind('mousemove',TREE.dragStart).unbind('mouseup').unbind('mousedown');
			$('#drag_container, #tree_plus').remove();
			if(dragNode_source) $(dragNode_source).show().prev('.line').show();
			dragNode_destination = dragNode_source = mousePressed = mouseMoved = false;
			//ajaxCache = Array();
		};
		
		TREE.convertToFolder = function( node )
		{
			node[ 0 ].className = node[ 0 ].className.replace( TREE.mapUserClass( $(node[0]).attr('id'), 'doc' ), TREE.mapUserClass( $(node[0]).attr('id'), 'folder' ) + '-open' );
			node.append( TREE.option.lastLineTemplate );
			TREE.setTrigger( node[ 0 ] );
			TREE.setEventLine( $('.line, .line-last', node) );
		};
		
		TREE.convertToDoc = function( node )
		{
			$('>ul', node).remove();
			$('img', node).remove();
			node[0].className = node[0].className.replace( "/" + TREE.mapUserClass($(node[0]).attr('id'),'folder') + "-(open|close)/gi", TREE.mapUserClass( $(node[0]).attr('id'), 'doc' ) );
		};
		
		TREE.moveNodeToFolder = function(node)
		{
			_docClass = TREE.mapUserClass($(node).attr('id'),'doc')
			if ( node[ 0 ].className.indexOf( _docClass ) != -1 )
			{
				if ( ! TREE.option.docToFolderConvert ) return true;
				TREE.convertToFolder( node );
			}
			
			TREE.checkNodeIsLast( dragNode_source[ 0 ] );
			var lastLine = $('>ul >.line-last', node);
			
			if ( lastLine.size() > 0 ) TREE.moveNodeToLine( lastLine[ 0 ] );
		};
		
		TREE.moveNodeToLine = function( node )
		{
			TREE.checkNodeIsLast( dragNode_source[ 0 ] );
			TREE.checkLineIsLast( node );
			
			var parent = $(dragNode_source).parents( 'li:first' );
			var line = $(dragNode_source).prev( '.line' );
			$(node).before( dragNode_source );
			$(dragNode_source).before( line );
			node.className = node.className.replace( '-over', '' );
			var nodeSize = $('>ul >li', parent).not( '.line, .line-last' ).filter( ':visible' ).size();

			if ( nodeSize == 0 )
			{
				if ( TREE.option.docToFolderConvert ) TREE.convertToDoc( parent );
				parent[ 0 ].className = parent[ 0 ].className.replace( 'open', 'close' );
				$('>ul',parent).hide();
			}

			// added by Erik Dohmen (2BinBusiness.nl) select node
			if ( $('span:first', dragNode_source).hasClass('text') )
			{
				$('.active',TREE).addClass('text');
				$('span:first',dragNode_source).addClass('active');
			}

			if ( typeof( TREE.option.afterMove ) == 'function' )
			{
				var pos = $(dragNode_source).prevAll(':not(.line)').size();
				TREE.option.afterMove( $(node).parents('li:first'), $(dragNode_source), pos );
			}
		};

		/**
		* Adds a node to the tree
		*/
		TREE.addNode = function( id, text, callback )
		{
			template_ = TREE.option.nodeTemplate.replace(/\%_id_\%/ig, id ).replace(/\%_value_\%/ig, text );
			var temp_node = $(template_);
			TREE.setTreeNodes( temp_node );
			dragNode_destination = TREE.getSelected();
			dragNode_source = $( '.' + TREE.mapUserClass( $(temp_node).attr('id'), 'doc' ) + '-last', temp_node );
			TREE.moveNodeToFolder( dragNode_destination );
			temp_node.remove();
			
			if ( typeof( callback ) == 'function' ) callback( dragNode_destination, dragNode_source );
		};
		
		/**
		* Deletes a tree node
		*/
		TREE.delNode = function( callback )
		{
			if ( cookieTimer ) clearTimeout( cookieTimer );
			
			dragNode_source = TREE.getSelected();
			TREE.checkNodeIsLast( dragNode_source[ 0 ] );
			dragNode_source.prev().remove();
			dragNode_source.remove();
			
			if ( typeof( callback ) == 'function' ) callback( dragNode_destination );

			//	Store new visibles
			cookieTimer = window.setTimeout( TREE.saveCookie, 7000 );
		};
		
		/**
		* Any node that is visible gets its ID stuffed into the cookie
		* Requires jquery.cookie.js plugin
		*/
		TREE.saveCookie = function saveCookie()
		{
			var i = 0;
			var data = [];
			$('>ul:visible',ROOT).each( 
				function() 
				{ 
					sFolder = TREE.mapUserClass( $(this).attr('id'), 'folder' );
					$('.' + sFolder + '-open, .' + sFolder + '-open-last').each(
						function()
						{
							data[ i++ ] = $(this).attr('id');
						}
					);
				}
			);
			
			//	No error if jQ cookie plugin is missing
			if ( $.cookie ) $.cookie( TREE.option.cookieId, data.join(",") );
		};
		
		/**
		* Restore from cookie...
		*/
		TREE.restoreCookie = function restoreCookie()
		{
			return ( $.cookie ) ? $.cookie( TREE.option.cookieId ).split(',') : {};
		}
			
		/**
		* mapUserClass extension
		*/
		TREE.mapUserClass = function( id, sType )
		{
			sClass = null;
			
			//	In the array?
			$.each( TREE.option.classMap, function( sIndex, sItem ) { if ( id == sIndex || id.indexOf( sIndex ) >= 0 ) return TREE.option.classMap[ sIndex ]; } );
			
			//	Return default
			return sClass ? sClass : sType;
		};

		/**
		* Initialize the tree...			
		*/
		TREE.init = function( obj ) 
		{ 
			//	Don't restore if no cookie stuff
			TREE.setTreeNodes( obj, false, TREE.restoreCookie() ); 
		};

		TREE.init( ROOT );
	});
}

/**
* Swaps all occurrences of sSearch with sReplace in the class of the element
*/
$.fn.swapClass = function( sSearch, sReplace )
{
	//	Replace all 'sSearch' with 'sReplace'
	target_ = $(this);
	$.each( $(this).attr('class').split(' '),  function( index, item ) { if ( item.indexOf( sSearch ) != -1 ) target_.removeClass( item ).addClass( item.replace( sSearch, sReplace ) ); } );
	return $(this);
}