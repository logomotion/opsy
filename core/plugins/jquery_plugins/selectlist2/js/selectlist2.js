(function($) {

$.fn.selectList2 = function(options) {
	
	var defaults = {
		module_name: "",
		id_input: "",
		id_lang: "",
		categories_id:"",
		categories_text:"",
		categories_selected:""
	};
	
	var opts = $.extend(defaults, options);
	
	function click_on_select(){
		
		var value = $(this).attr('value');
		
		$(this).attr("disabled","disabled");
		
		if(opts.id_lang != "")
			$('div#'+opts.id_input+'_'+opts.id_lang +'_'+value).show();
		else
			$('div#'+opts.id_input+'_'+value).show();
			
		var tmp_value = $('input#'+opts.module_name+'_obj_values'+opts.id_input+'_'+opts.id_lang).attr('value');
		
		if(tmp_value == "")
			tmp_value = value;
		else
			tmp_value = tmp_value+ ','+value;
		
		$('input#'+opts.module_name+'_obj_values'+opts.id_input+'_'+opts.id_lang).attr('value',tmp_value);
	}
	
	function click_on_hide(){
		
		var value_parent = $(this).parent().attr('id');
		
		var value_exp = value_parent.split('_');
		var last_id = value_exp.length - 1;
		
		var value = value_exp[last_id];
		
		if(opts.id_lang != ""){
			new_id_lang = '_' + opts.id_lang;
		}
		
		$('select#'+opts.module_name+'_obj'+opts.id_input+new_id_lang+' option').each(function(){
																							   
			if($(this).attr("value") == value ){
				$(this).attr("disabled","");
			}
			
		});
		
		$('div#' + opts.id_input+new_id_lang + '_' + value).hide();
		
		$('input#'+ opts.module_name + '_obj' + opts.id_input + new_id_lang +'_'+value).val('');
		
		var tmp_value = $('input#'+opts.module_name+'_obj_values'+opts.id_input+'_'+opts.id_lang).attr('value');
		
		var tmp_value_tab = tmp_value.split(',');
		var tmp_out = "";
		
		for(i=0;i<tmp_value_tab.length;i++){
			if(tmp_value_tab[i] != value)
				if(tmp_out == "")
					tmp_out = tmp_value_tab[i];
				else
					tmp_out = tmp_out+','+tmp_value_tab[i];
		}
		$('input#'+opts.module_name+'_obj_values'+opts.id_input+'_'+opts.id_lang).attr('value',tmp_out);
		
	}
	
	//Gestion du clic sur les options du select
	$(this).click(click_on_select);
	
	//Gestion de la cr�ation de la div avec ces contenants
	if(opts.categories_id != "" && opts.categories_text != ""){
		
		var categories_id_tab = opts.categories_id.split(',');
		var categories_text_tab = opts.categories_text.split(',');
		var categories_selected_tab = opts.categories_selected.split(',');
		
		var longueur = categories_id_tab.length-1;
		var longueur_selected = categories_selected_tab.length;
		
		var content_of_data;

		for(i=longueur;i>=0;i--){
			
			$(this).parent().after("<div id='"+opts.id_input+"_"+opts.id_lang+"_"+categories_id_tab[i]+"' class='data_products_list'><span class='sl_form_title'>"+categories_text_tab[i]+" : </span><img src='../core/plugins/jquery_plugins/selectlist_input/images/cross-circle.png' id='"+opts.module_name+"_img"+opts.id_input+"_"+opts.id_lang+"' /></div>");
			
			content_of_data = "";
			if(longueur_selected == 0){
				if(categories_selected_tab[0] == categories_id_tab[i]){
					content_of_data = "ok";
				}
			}
			else{
				for(j=0;j<longueur_selected;j++){
					if(categories_selected_tab[j] == categories_id_tab[i]){
						content_of_data = "ok";
					}
				}
			}
			
			if(content_of_data != ""){
				$('div#'+opts.id_input+'_'+opts.id_lang+'_'+categories_id_tab[i]).show();
				$('select#'+opts.module_name+'_obj'+opts.id_input+'_'+opts.id_lang+' option').each(function(){
																					   
					if($(this).attr("value") == categories_id_tab[i] ){
						$(this).attr("disabled","disabled");
					}
					
				});
			}
		}
	}
	
	//champ pour les valeurs a recup�rer
	$(this).parent().after("<input id='"+opts.module_name+"_obj_values"+opts.id_input+"_"+opts.id_lang+"' name='"+opts.module_name+"_obj_values"+opts.id_input+"_"+opts.id_lang+"' type='hidden' value='"+opts.categories_selected+"' />");
	
	//Gestion du clic sur la div
	$('img#'+opts.module_name+'_img'+opts.id_input+'_'+opts.id_lang).click(click_on_hide);
	
	return $(this);
	
};
})(jQuery);