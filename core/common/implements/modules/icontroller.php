<?php
/**
* @package		SLASH-CMS
* @subpackage	implements
* @internal     interface module controller
* @version		icontroller.php - Version 12.3.01
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

interface iController{

	public function initialise(); //Initialise function 
	public function load_header(); //Load header function
	public function load_footer(); //Load footer function
	public function load(); //Load module function
	public function execute(); //Execute function
	
}

?>