<?php
/**
* @package		SLASH-CMS
* @subpackage	MODEL ABSTRACT CLASS
* @internal     Module Model
* @version		sla_model.php - Version 12.3.02
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

abstract class slaModel{


	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		
		$this->sla_construct();
	}
	
	public function sla_construct() {
	
	}
	
}

?>