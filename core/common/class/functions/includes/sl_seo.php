<?php
/**
* @package		SLASH-CMS
* @subpackage	SEO_FUNCTIONS
* @internal     SEO functions
* @version		sl_seo.php - Version 10.7.6
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

class sl_seo {

	/**
	 * SEO text function
	 * @param $text:string Text
	 * @return seo text
	 */
	public function seo_text($text) {
		$str = strtolower($text);
		$str = strtr(
		strtr($str,
		'������������������������������������������������������������.',
		'SZszYAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy-'),
		array('�' => 'TH', '�' => 'th', '�' => 'DH', '�' => 'dh', '�' => 'ss',
		'�' => 'OE', '�' => 'oe', '�' => 'AE', '�' => 'ae', '�' => 'u'));
		$str = preg_replace('/[^\w]/', '-', $str);
		$str = preg_replace('/[-]{2,}/', '-', $str);
		
		return strip_tags($str);
	}
	
	
	/*
	* Clear string
	*/
	
	public function clear_text($data){
		$data=strtr($data, "'" , "_");
		$data=strtr($data, "�����������������������������������������������������" , "AAAAAAaaaaaaOOOOOOooooooEEEEeeeeCcIIIIiiiiUUUUuuuuyNn");
		
		return ($data);
	}
	

	/*
	* Sanitize string
	*/
	public function str_sanitize($str) {    
		$str = strtolower($str);
		$str = strtr(
		strtr($str,
		'������������������������������������������������������������.',
		'SZszYAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy-'),
		array('�' => 'TH', '�' => 'th', '�' => 'DH', '�' => 'dh', '�' => 'ss',
		'�' => 'OE', '�' => 'oe', '�' => 'AE', '�' => 'ae', '�' => 'u'));
		$str = preg_replace('/[^\w]/', '-', $str);
		$str = preg_replace('/[-]{2,}/', '-', $str);
		return $str;    
	}
	
}

?>