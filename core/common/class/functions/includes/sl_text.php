<?php
/**
* @package		SLASH-CMS
* @subpackage	TEXT_FUNCTIONS
* @internal     Text functions
* @version		text.php - Version 9.10.21
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

class sl_text {

	/**
	 * Substring text function
	 * @param $text:string Text
	 * @param $width:int Width
	 * @param $etc:bool Add ... or not
	 * @return substr text
	 */
	public function substring($text,$width,$etc=false) {
		
		if (strlen($text)>$width) {
			$text = substr($text, 0, $width); 
		}
		if ($etc) { $text.="..."; }
		return $text;
	}

	/**
	 * Substring text function 2 (not cut complete word)
	 * @param $text:string Text
	 * @param $width:int Width
	 * @param $etc:bool Add ... or not
	 * @return substr text
	 */
	public function substring_word($text,$width,$etc=false) {
		if (strlen($text)>$width) {
			$text = substr($text, 0, $width)."..."; 
			$t = explode(" ",$text);
			$i = 0;
			$ret = "";
			while (!empty($t[$i+1])) {
				$ret .= " ".$t[$i];
				$i++;
			}
		} else {
			$ret = $text;
		}
		if ($etc) { $ret.="..."; }
		return $ret;
	}
	
	public function text2url($text){
		
		$text = utf8_decode($text);
		
		$text = strip_tags($text);
		
		$text = strtolower($text);
		
		$text = strtr(
			strtr($text,
			'������������������������������������������������������������.',
			'SZszYAAAAAACEEEEIIIINOOOOOOUUUUYaaaaaaceeeeiiiinoooooouuuuyy-'),
			array('�' => 'TH', '�' => 'th', '�' => 'DH', '�' => 'dh', '�' => 'ss',
			'�' => 'OE', '�' => 'oe', '�' => 'AE', '�' => 'ae', '�' => 'u')
		);

		//$text = preg_replace('/\%/',' pourcentage',$text); 
		//$text = preg_replace('/\@/',' arobase ',$text); 
		//$text = preg_replace('/\&/',' et ',$text); 
		$text = preg_replace('/\s[\s]+/','-',$text);
		$text = preg_replace('/[\s\W]+/','-',$text);
		$text = preg_replace('/^[\-]+/','',$text);
		$text = preg_replace('/[\-]+$/','',$text);
		
		return $text;
		
	}
	
}

?>