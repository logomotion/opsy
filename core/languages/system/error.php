<?php
/**
* @package		SLASH-CMS
* @subpackage	ERRORS MESSAGES
* @internal     errors messages
* @version		error.php - Version 9.6.10
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

define("CONNEXION_ERROR", "Database connexion error");
define("SELECT_DB_ERROR", "Database connexion error");
define("QUERY_ERROR", "SQL Query error");
define("UNKNOWN_MODULE_ERROR", "Unknown module error");
define("UNKNOWN_TEMPLATE_ERROR", "Unknown template error");

?>
