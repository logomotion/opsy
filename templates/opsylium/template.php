<?php 
	/**
	* @package		SLASH-CMS
	* @version		template.php - Version 9.6.2
	* @author		Julien Veuillet [http://www.wakdev.com]
	* @copyright	Copyright(C) 2009 - Today. All rights reserved.
	* @license		GNU/GPL
	*/

	$dbh = new PDO('mysql:host=' . $this->__get('mysql_host') . ';dbname='.$this->__get('mysql_database'),$this->__get('mysql_user'),$this->__get('mysql_password'), array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));

	function truncate($text, $chars = 25) {
	    $text = $text." ";
	    $text = substr($text,0,$chars);
	    $text = substr($text,0,strrpos($text,' '));
	    $text = $text."...";
	    return $text;
	}

	if ($this->sl_param('json')) {

		switch ($this->sl_param('zone')) {

			case 'bourgogne':
				$cp = '21';
				break;
			case 'rhones-alpes':
				$cp = '69,74';
				break;
		}
		$stmt = $dbh->prepare('SELECT ville, codepostal FROM biens WHERE LEFT(codepostal, 2) IN ('. $cp .') GROUP BY ville');
		$stmt->execute();
		$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

		header('Content-Type: application/json');
		echo json_encode($villes);
		exit;

	}

	if ($this->sl_param("mod") == "") {
		$this->get_params["mod"] = "neti_contents";
		$this->get_params["idr"] = 94;
	}

	$this->initialise_modules(); 
	$this->load_module("sl_header");

	$urlBase = $this->__get('url_base');

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">
<head>

	<!-- Base -->
	<base href="<?php echo $urlBase; ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<!-- Styles -->
	<link rel="stylesheet" type="text/css" href="specific_modules/neti_contents/views/mrpromotion/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="specific_modules/neti_menu/views/default/css/styles.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo $this->config["site_template_url"]; ?>css/styles.css" />
	<link href="<?php echo $this->config["site_template_url"]; ?>images/favicon.ico" rel="shortcut icon" type="image/x-icon" />

	<!-- Google Fonts -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic" rel="stylesheet" type="text/css" />
	<link href="http://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" type="text/css">
	<link href='http://fonts.googleapis.com/css?family=Lato' rel='stylesheet' type='text/css'>
	
	<!-- Font Awesome -->
	<link rel="stylesheet" href="templates/opsylium/font-awesome/css/font-awesome.css">

	<!-- Magnific Popup -->
	<link rel="stylesheet" href="templates/opsylium/magnific-popup/magnific-popup.css">

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="templates/opsylium/owl-carousel/owl-carousel.css">
	<link rel="stylesheet" href="templates/opsylium/owl-carousel/owl-theme.css">

	<!-- Tooltipster -->
	<link rel="stylesheet" href="templates/opsylium/tooltipster/tooltipster.css">

	<script src="core/plugins/jquery_plugins/slideShow/slideshow.js" type="text/javascript" language="javascript"></script>
	<script src="core/plugins/jquery_plugins/slider/slides.min.jquery.js" type="text/javascript" language="javascript"></script>
	<script src="<?php echo $this->config["site_template_url"]; ?>js/script.js" type="text/javascript" language="javascript"></script>	
	<script>

		// Functions

		function validateEmail(email) {

		 	if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email)) {
			    return true;
			} else {  
			    return false;
			}

		}

		function validateTelephone(telephone) {

			telephone = telephone.replace(/[^0-9]/g, '');
			
			if (telephone.length === 10) {
				return true;
			} else {
				return false;
			}

		}

	</script>
</head>
<body>

<div class="header">

	<div class="logo">
		<a href="<?php echo $urlBase; ?>"><img src="<?php echo $this->config["site_template_url"]; ?>images/logo.png" border="0" alt="logo"/></a>
	</div>

	<?php include_once('module-promo.php') ?>

	<div class="bloc-toggle">
		<a href="#">Contact<i class="fa fa-angle-down"></i></a>
	</div>
	<div class="bloc-contact">
		<div class="bloc">
			<span class="where">Rhônes-alpes</span>
			<span class="phone">04 82 29 69 69</span>
		</div>
		<div class="bloc">
			<span class="where">Bourgogne</span>
			<span class="phone">03 80 30 60 59</span>
		</div>
		<div class="bloc">
			<a href="#" class="recall">
				<span class="where"><i class="fa fa-phone"></i>Être rappelé</span>
				<span class="phone"><small>GRATUITEMENT</small></span>
			</a>
		</div>
	</div>

</div>

<div class="my-menu-toggle">
	<a href="#">Menu<i class="fa fa-angle-down"></i></a>
</div>
<div class="my-menu">
	<?php include_once('menu.urlrewrited.php'); ?>
</div>

<div class="content">

	<div class="contenu">

		<?php if ($this->sl_param('list')) { ?>

			<!-- List Regions or Villes -->
			<?php include_once('list.php'); ?>

		<?php } elseif ($this->sl_param('search')) { ?>

			<!-- Search results -->
			<?php include_once('search.php'); ?>			

		<?php } elseif ($this->sl_param('contact')) { ?>

			<!-- Contact -->
			<?php include_once('module-newsletter.php'); ?>

		<?php } elseif ($this->sl_param('news')) { ?>

			<!-- News -->
			<?php include_once('news.php'); ?>	

		<?php } elseif ($this->sl_param('bien')) { ?>

			<!-- Details -->
			<?php include_once('detail.php'); ?>

		<?php } elseif ($this->sl_param("idr") === 94) { ?>

			<!-- Homepage -->
			<div class="slider-home">
				<h2>Nos derniers programmes</h2>
				<?php include_once('slider-home.php'); ?>
			</div>

			<div class="slider-all">
				<h2>Tous nos programmes</h2>
				<?php include_once('slider-all.php'); ?>
			</div>

		<?php } else { ?>

			<!-- CMS -->
			<?php $this->load_module($this->sl_param("mod")); ?>

		<?php } ?>

	</div>

	<div class="sidebar">
		<?php

			if ($this->sl_param('bien')) {

				include_once('module-info.php');	
				include_once('module-alert-short.php');	
				include_once('module-news.php');	

			} else {

				include_once('module-search.php');	
				include_once('module-coups-de-coeur.php');					

			}

		?>
	</div>

</div>

<?php include_once('module-recall.php') ?>

<?php if ($this->sl_param("idr")!=129) { ?>

	<!-- Footer -->

	<div class="clearfix"></div>

	<div class="footer">
		<div class="foot">
	
				<div class="contact">
					<p>Nous contacter</p>
					<span><i>Bourgogne</i>03 80 30 60 59</span>
					<span><i>Rhônes-Alpes</i>04 82 29 69 69</span>
					<span>s.fellous@opsylium.com</span>
				</div>

				<div class="sociaux">
					<p>Retrouvez-nous sur</p>
					<a href="https://www.linkedin.com/company/opsylium" class="linkedin"><i class="fa fa-linkedin-square"></i></a>
					<a href="#" class="facebook"><i class="fa fa-facebook-square"></i></a>
				</div>

				<div class="menu-bottom">
						<ul>
							<li><p>Bourgogne</p></li>
							<li><a href="index.php?list=region&name=bourgogne">Tous</a></li>
							<li><a href="index.php?list=region&name=bourgogne&filter=neuf">Neufs</a></li>
							<li><a href="index.php?list=region&name=bourgogne&filter=defisc">Défiscalisation</a></li>
							<li><a href="index.php?list=region&name=bourgogne&filter=ancien">Anciens</a></li>
						</ul>
						<ul>
							<li><p>Rhônes-Alpes</p></li>
							<li><a href="index.php?list=region&name=rhone-alpes">Tous</a></li>
							<li><a href="index.php?list=region&name=rhone-alpes&filter=neuf">Neufs</a></li>
							<li><a href="index.php?list=region&name=rhone-alpes&filter=defisc">Défiscalisation</a></li>
							<li><a href="index.php?list=region&name=rhone-alpes&filter=ancien">Anciens</a></li>
						</ul>
				</div>

				<div class="newsletter">
					<form action="contact">
						<p>Newsletter</p>
						<input name="email" type="text" class="email" placeholder="Email">
						<input type="submit" class="submit" value="Valider">
					</form>
				</div>

				<hr />

				<p>Opsylium <i class="fa fa-copyright"></i> 2015 - <a href="mentions-legales-95.php">Mentions Légales</a></p>

		</div>
	</div>

<?php } ?>

<?php // $this->execute_modules(); ?>

<!-- Owl Carousel -->
<script src="templates/opsylium/owl-carousel/owl-carousel.js"></script>
<script>
	
	$('.carousel').owlCarousel({
		singleItem: true,
		navigation: true,
		pagination: false,
		navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
	});

	$('.carousel-three').owlCarousel({
		items: 3,
		navigation: true,
		pagination: false,
		navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>']
	});

</script>

<!-- Magnific Popup -->
<script src="templates/opsylium/magnific-popup/magnific-popup.js"></script>
<script>
	
	$('.popup').magnificPopup({
		type: 'image',
		gallery: {
			enabled: true
		}
	});

</script>

<!-- Tooltipster -->
<script src="templates/opsylium/tooltipster/tooltipster.js"></script>
<script>
	
	$('.tooltip').tooltipster({
		theme: 'tooltipster-opsy',
		position: 'bottom'
	});

</script>

<!-- Global JS Code -->
<script>
	
	$('.modal span.close').click(function(){

		$('.modal').addClass('hidden');

	});

	/* Responsive Menu */

	$('.my-menu-toggle a').click(function(e){

		e.preventDefault();
		$('.my-menu').toggle();		

	});


	$('.my-menu a').click(function(e){

		if ($(window).width() <= 1024) {

			if ($(this).next('ul').length !== 0) {

				e.preventDefault();
				$(this).next('ul').toggleClass('visible');

			}

		}

	});	

	/* Contact Responsive */

	$('.header .bloc-toggle a').click(function(e){

		e.preventDefault();
		$('.header .bloc-contact').toggle();		

	});

</script>

</body>
</html>