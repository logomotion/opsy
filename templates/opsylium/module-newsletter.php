<div class="module-newsletter">

	<h1>Newsletter</h1>

	<p class="fushia">Restez informé !</p>
	<p>Inscrivez-vous et recevez les nouveautés et les bons plans de tout l'immobilier neuf de Dijon et de Lyon.</p>

	<form action="index.php">

		<input name="nom" class="nom" type="text" placeholder="Nom">
		<input name="prenom" class="prenom" type="text" placeholder="Prénom">
		<input name="telephone" class="telephone" type="text" placeholder="Téléphone">
		<input name="email" class="email" type="text" <?php if ($this->sl_param('email')) { echo 'value="'. $this->sl_param('email') .'"'; } ?> placeholder="Email">

		<input type="submit" class="submit" value="Valider">

		<span>(Vous pourrez vous désinscrire en suivant le lien présent dans la newsletter)</span>

	</form>
</div>

<script>

	$('.module-newsletter form').submit(function(e){

		e.preventDefault();

		data = $(this).serialize();

		error = {
			value: false,
			text: ''
		};

		if ($('div.module-newsletter input.nom').val() === '') 			{ error.value = true; error.text = "Saisissez votre nom..."; }
		if ($('div.module-newsletter input.prenom').val() === '') 		{ error.value = true; error.text = "Saisissez votre prénom..."; }
		if ($('div.module-newsletter input.telephone').val() === '') 	{ error.value = true; error.text = "Saisissez votre téléphone..."; }
		if ($('div.module-newsletter input.email').val() === '') 		{ error.value = true; error.text = "Saisissez votre email..."; }

		if (!validateTelephone($('div.module-newsletter input.telephone').val())) 	{ error.value = true; error.text = "Saisissez un téléphone valide..."; }
		if (!validateEmail($('div.module-newsletter input.email').val())) 			{ error.value = true; error.text = "Saisissez un email valide..."; }

		if (!error.value) {

			$.ajax({
				url: 'templates/opsylium/contact.php?'+ data,
				method: 'GET',
				success: function(){

					alert('Vous êtes maintenant inscrit à la newsletter Opsylium !');
					
				},
				error: function(jqXHR, textStatus, errorThrown){

					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					alert('Une erreur est survenue lors de votre inscription à la newsletter...');

				}
			});

		} else {

			alert(error.text);

		}

	});

</script>