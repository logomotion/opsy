<div class="module-coups-de-coeur">
	<h2>Coups de coeur</h2>
	<div class="carousel">
		<?php

			$stmt = $dbh->prepare('SELECT id, mandat, ville, photos FROM biens WHERE coupdecoeur = "1"');
			$stmt->execute();
			$coeurs = $stmt->fetchAll(PDO::FETCH_OBJ);

			for ($i = 0; $i < count($coeurs); $i++) {

				$photos = json_decode($coeurs[$i]->photos);

				echo '<a href="bien/'. $coeurs[$i]->id .'">';
					echo '<div class="item" style="background-image:url('. $photos[0] .')">';
							echo '<div class="caption">';
								echo '<h2>'. $coeurs[$i]->mandat .' - '. $coeurs[$i]->ville .'</h2>';
							echo '</div>';
					echo '</div>';
				echo '</a>';

			}

		?>
	</div>
</div>