<div class="modal modal-alert hidden">

	<div class="overlay"></div>

	<form action="index.php">

		<span class="close"><i class="fa fa-times"></i></span>
		
		<h3>Alerte nouveauté</h3>

		<p>Quelques informations sur vous...</p>

		<label><input name="sexe" type="radio" value="mme">Mme</label>
		<label><input name="sexe" type="radio" value="mr">Mr</label>
		<input name="nom" class="nom" type="text" placeholder="Nom">
		<input name="prenom" class="prenom" type="text" placeholder="Prénom">
		<input name="telephone" class="telephone" type="text" placeholder="Téléphone">
		<input name="email" class="email" type="text" placeholder="Mail">

		<p>... Et sur ce que vous recherchez !</p>

		<input name="recherche_ville" type="text" class="recherche_ville" placeholder="Ville">
		<input name="recherche_piece" type="text" class="recherche_piece" placeholder="Nombre de pièces">

		<textarea name="remarques" class="remarques" placeholder="Remarques"></textarea>

		<input type="submit" class="submit" value="Valider">

	</form>
</div>

<script>

	$('.modal-alert form').submit(function(e){

		e.preventDefault();

		data = $(this).serialize();

		error = {
			value: false,
			text: ''
		};

		if ($('div.modal-alert input.sexe').val() === '') 				{ error.value = true; error.text = "Saisissez votre genre..."; }
		if ($('div.modal-alert input.nom').val() === '') 				{ error.value = true; error.text = "Saisissez votre nom..."; }
		if ($('div.modal-alert input.prenom').val() === '') 			{ error.value = true; error.text = "Saisissez votre prénom..."; }
		if ($('div.modal-alert input.telephone').val() === '') 			{ error.value = true; error.text = "Saisissez votre téléphone..."; }
		if ($('div.modal-alert input.email').val() === '') 				{ error.value = true; error.text = "Saisissez votre email..."; }
		if ($('div.modal-alert input.recherche_ville').val() === '') 	{ error.value = true; error.text = "Saisissez votre ville recherche..."; }
		if ($('div.modal-alert select.recherche_piece').val() === '') 	{ error.value = true; error.text = "Saisissez le nombre de pièces..."; }

		if (!validateTelephone($('div.modal-alert input.telephone').val())) 	{ error.value = true; error.text = "Saisissez un téléphone valide..."; }
		if (!validateEmail($('div.modal-alert input.email').val())) 			{ error.value = true; error.text = "Saisissez un email valide..."; }

		if (!error.value) {

			$.ajax({
				url: 'templates/opsylium/contact.php?'+ data,
				method: 'GET',
				success: function(){

					alert('Votre demande a bien été prise en compte !');
					
					$('.modal-alert').addClass('hidden');
					
				},
				error: function(jqXHR, textStatus, errorThrown){

					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					alert('Une erreur est survenue lors de la validation de votre demande de recherche...');

				}
			});

		} else {

			alert(error.text);

		}

	});

</script>