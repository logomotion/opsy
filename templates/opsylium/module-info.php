<div class="module-info">
	<h2>Demande d'informations</h2>

	<form action="index.php">

		<input name="id" class="id" type="hidden" value="<?php echo $bien->id; ?>">

		<input name="nom" class="nom" type="text" placeholder="Nom">
		<input name="prenom" class="prenom" type="text" placeholder="Prénom">
		<input name="telephone" class="telephone" type="text" placeholder="Téléphone">
		<input name="email" class="email" type="text" placeholder="Email">
		<select name="type" class="type">
			<option selected="selected" disabled="disabled">Bien correspondant</option>
			<?php

				for ($i = 0; $i < count($lots); $i++) {

					if ($lots[$i]->type === 'appartement') {

						echo '<option value="Appart. T'. $lots[$i]->piece .' ('. $lots[$i]->surface .' m²)">Appart. T'. $lots[$i]->piece .' ('. $lots[$i]->surface .' m²)</option>';

					} else {

						echo '<option value="'. ucwords($lots[$i]->type) .' ('. $lots[$i]->surface .' m²)">'. ucwords($lots[$i]->type) .' ('. $lots[$i]->surface .' m²)</option>';

					}

				}

			?>
		</select>
		<textarea name="commentaire" class="commentaire" placeholder="Commentaire"></textarea>
		<input type="submit" class="submit" value="Envoyer">

	</form>

</div>

<script>

	$('.module-info form').submit(function(e){

		e.preventDefault();

		data = $(this).serialize();

		error = {
			value: false,
			text: ''
		};

		if ($('div.module-info input.nom').val() === '') 		{ error.value = true; error.text = "Saisissez votre nom..."; }
		if ($('div.module-info input.prenom').val() === '') 	{ error.value = true; error.text = "Saisissez votre prénom..."; }
		if ($('div.module-info input.telephone').val() === '') 	{ error.value = true; error.text = "Saisissez votre téléphone..."; }
		if ($('div.module-info input.email').val() === '') 		{ error.value = true; error.text = "Saisissez votre email..."; }
		if ($('div.module-info select.type').val() === null) 	{ error.value = true; error.text = "Choisissez un bien..."; }

		if (!validateTelephone($('div.module-info input.telephone').val())) 	{ error.value = true; error.text = "Saisissez un téléphone valide..."; }
		if (!validateEmail($('div.module-info input.email').val())) 			{ error.value = true; error.text = "Saisissez un email valide..."; }

		if (!error.value) {

			$.ajax({
				url: 'templates/opsylium/info.php?'+ data,
				method: 'GET',
				success: function(){

					alert('Votre demande a bien été prise en compte !');

					$.ajax({
						url: 'templates/opsylium/contact.php?'+ data,
						method: 'GET',
						success: function(){

							// Nothing

						},
						error: function(jqXHR, textStatus, errorThrown){

							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
							alert('Une erreur est survenue...');

						}
					});
					
				},
				error: function(jqXHR, textStatus, errorThrown){

					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					alert('Une erreur est survenue lors de l\'envoi de votre demande d\'informations...');

				}
			});
			
		} else {

			alert(error.text);

		}


	});

</script>

</script>