<?php

	$stmt = $dbh->prepare('SELECT * FROM biens WHERE id = :id');
	$stmt->bindParam(':id', $this->sl_param('bien'));
	$stmt->execute();
	$bien = $stmt->fetch(PDO::FETCH_OBJ);

?>
<div class="detail">

	<h1>
		<?php 

			echo $bien->ville .' - '. $bien->mandat;

			if (strtolower($bien->dispositif_fiscal) === 'pinel') {
				echo '<span class="tooltip pinel"0 title="Assujetti à la Loi Pinel"></span>';
			}
			if (strtolower($bien->livraison) !== '') {
				echo '<span class="tooltip building" title="Prévu pour le '. $bien->livraison .'"></span>';
			}

		?>
	</h1>

	<div class="desc">

		<div class="carousel">
			<?php

				$photos = json_decode($bien->photos);

				for ($i = 0; $i < count($photos); $i++) {

					echo '<div class="item" style="background-image:url('. $photos[$i] .')">';
						echo '<a class="popup" href="'. $photos[$i] .'">';
							if (strtolower($bien->dispositif_fiscal) === 'pinel') {
								echo '<span class="pastilles pinel"><img src="templates/opsylium/images/pinel.png" alt="Loi Pinel"></span>';
							}
							if (strtolower($bien->livraison) !== '') {
								echo '<span class="pastilles building">'. $bien->livraison .'</span>';
							}
						echo '</a>';
					echo '</div>';

				}

			?>
		</div>

		<p><?php echo $bien->descriptif; ?></p>

		<!-- <h2>BIENS CORRESPONDANTS</h2> -->

		<div class="lots">
			<table>
				<thead>
					<tr>
						<th>Type</th>
						<th>Surface</th>
						<th>À partir de</th>
						<th>Quantité</th>
					</tr>
				</thead>
				<tbody>
					<?php

						$stmt = $dbh->prepare('SELECT * FROM lots WHERE id_bien = :id');
						$stmt->bindParam(':id', $this->sl_param('bien'));
						$stmt->execute();
						$lots = $stmt->fetchAll(PDO::FETCH_OBJ);

						function cmp($a, $b) {
							if ($a->type === 'appartement') { $str_a = 'T'. $a->piece; } else { $str_a = ucwords($a->type) .' – '. $a->surface; }
							if ($b->type === 'appartement') { $str_b = 'T'. $b->piece; } else { $str_b = ucwords($b->type) .' – '. $b->surface; }
						    return strcmp($str_a, $str_b);
						}

						usort($lots, 'cmp');

						for ($i = 0; $i < count($lots); $i++) {

							if ($lots[$i]->type === 'appartement') {

								echo '<tr><td>T'. $lots[$i]->piece .'</td><td>'. $lots[$i]->surface .' m²</td><td>'. $lots[$i]->prix .'€</td><td>'. $lots[$i]->quantite .'</td></tr>';

							} else {

								echo '<tr><td>'. ucwords($lots[$i]->type) .'</td><td>'. $lots[$i]->surface .' m²</td><td>'. $lots[$i]->prix .'€</td><td>'. $lots[$i]->quantite .'</td></tr>';

							}

						}

					?>
				</tbody>
			</table>
		</div>

	</div>

	<div class="options">

		<p class="price">À partir de <?php echo $bien->prix; ?>€</p>

		<?php
			if ($bien->promo == '1') {
				echo '<p class="promo">'. $bien->titrepromo .'</p>';
			}
		?>

		<a href="#" class="recall"><i class="fa fa-phone"></i>Être rappelé gratuitement</a>

		<div class="links">
	
			<?php

				$hr = false;
				
				if ($bien->carte !== '') {
					echo '<a target="_blank" href="'. $bien->carte .'"><i class="fa fa-map-marker"></i></i>Carte</a>';
					$hr = true;
				}
				if ($bien->fichier !== '') {
					echo '<a target="_blank" href="'. $bien->fichier .'"><i class="fa fa-file-o"></i></i>Télécharger plaquette</a>';
					$hr = true;
				}

				if ($hr) { echo '<hr>'; }

			?>

			<a href="javascript:window.print();"><i class="fa fa-print"></i>Imprimer</a>
			<a href="#" class="friend"><i class="fa fa-envelope-o"></i>Envoyer à un ami</a>
			<a href="#"><i class="fa fa-facebook-official"></i>Partager</a>
			<a href="#"><i class="fa fa-calculator"></i>Calc. financière</a>

			<?php

				$hr = false;

				switch (strtoupper($bien->ville)) {

					case 'DIJON':
						echo '<hr>';
						echo '<a href="vivre-a-dijon-140.php"><i class="fa fa-info-circle"></i></i>Vivre à Dijon</a>';
						break;
					case 'LYON':
						echo '<hr>';
						echo '<a href="vivre-a-lyon-141.php"><i class="fa fa-info-circle"></i></i>Vivre à Lyon</a>';
						break;

				}

			?>

		</div>

	</div>

	<div class="clearfix"></div>

	<?php

		$stmt = $dbh->prepare('SELECT id, mandat, ville, photos FROM biens WHERE id != "'. $bien->id .'" AND ville = "'. $bien->ville .'"');
		$stmt->execute();
		$same = $stmt->fetchAll(PDO::FETCH_OBJ);

		if (count($same) > 0) {

	?>

		<div class="slider">
			<h2>PROGRAMMES DANS LA MÊME VILLE</h2>
			<div class="carousel-three">
				<?php

					for ($i = 0; $i < count($same); $i++) {

						$photos = json_decode($same[$i]->photos);

						echo '<a href="bien/'. $same[$i]->id .'">';
							echo '<div class="item" style="background-image:url('. $photos[0] .')">';
								echo '<div class="caption">';
									echo '<h2>'. $same[$i]->mandat .' - '. $same[$i]->ville .'</h2>';
								echo '</div>';
							echo '</div>';
						echo '</a>';

					}

				?>
			</div>
		</div>

	<?php } ?>

</div>

<?php include_once('module-alert.php') ?>
<?php include_once('module-friend.php') ?>