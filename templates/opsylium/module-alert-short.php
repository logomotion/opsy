<div class="module-alert">
	<h2>Alerte nouveauté</h2>

		<!-- <p>Augmentez vos chances de trouver LE logement de vos rêves !</p> -->
		<p>Inscrivez-vous et recevez en instantané les logements qui correspondent à vos critères de recherche, vous serez les premiers informés !</p>
	
		<form action="alert.php">

			<input name="email" type="text" class="email" placeholder="Email">
			<input type="submit" class="submit" value="Valider">

		</form>

</div>

<script>
	
	$('div.module-alert form').submit(function(e){

		e.preventDefault();

		$('.modal-alert').removeClass('hidden');

		$('body').scrollTop(0);

	});

	$('div.module-alert form input.email').change(function(){

		$('div.modal-alert form input.email').val($(this).val());

	});

</script>