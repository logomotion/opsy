<?php

	$stmt = $dbh->prepare('SELECT id, mandat, ville, photos FROM biens');
	$stmt->execute();
	$home = $stmt->fetchAll(PDO::FETCH_OBJ);

?>
<div class="carousel-three">
	<?php

		for ($i = 0; $i < count($home); $i++) {

			$photos = json_decode($home[$i]->photos);

			echo '<a href="bien/'. $home[$i]->id .'">';
				echo '<div class="item" style="background-image:url('. $photos[0] .')">';
					echo '<div class="caption">';
						echo '<h2>'. $home[$i]->mandat . ' - '. $home[$i]->ville .'</h2>';
					echo '</div>';
				echo '</div>';
			echo '</a>';

		}

	?>
</div>