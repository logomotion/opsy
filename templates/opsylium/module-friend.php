<div class="modal modal-friend hidden">
	<div class="overlay"></div>
	<form action="index.php">

		<span class="close"><i class="fa fa-times"></i></span>

		<h3>Envoyer à un ami</h3>
		<p>Saisissez les informations nécessaires à l'envoi de ce bien. Vous recevrez également une copie.</p>

		<input name="link" class="link" type="hidden" value="<?php echo "http://". $_SERVER[HTTP_HOST] . $_SERVER[REQUEST_URI]; ?>">
		<input name="emailFrom" class="emailFrom" type="text" placeholder="Votre email">
		<input name="emailTo" class="emailTo" type="text" placeholder="L'email de votre ami">
		<input type="submit" class="submit" value="Valider">

	</form>
</div>

<script>
	
	$('.modal-friend form').submit(function(e){

		e.preventDefault();

		data = $(this).serialize();

		error = {
			value: false,
			text: ''
		};

		if ($('div.modal-friend input.emailFrom').val() === '') 	{ error.value = true; error.text = "Saisissez votre email..."; }
		if ($('div.modal-friend input.emailTo').val() === '') 		{ error.value = true; error.text = "Saisissez l'email de votre ami..."; }

		if (!validateEmail($('div.modal-friend input.emailFrom').val())) 	{ error.value = true; error.text = "Votre email n'est pas valide..."; }
		if (!validateEmail($('div.modal-friend input.emailTo').val())) 		{ error.value = true; error.text = "L'email de votre ami n'est pas valide..."; }

		if (!error.value) {

			$.ajax({
				url: 'templates/opsylium/friend.php?'+ data,
				method: 'GET',
				success: function(){

					alert('Votre demande a bien été prise en compte !');
					
				},
				error: function(jqXHR, textStatus, errorThrown){

					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					alert('Une erreur est survenue lors de l\'envoi de votre bien à votre ami...');

				}
			});		

		} else {

			alert(error.text);

		}	

	});

	$('a.friend').click(function(e){

		e.preventDefault();

		$('.modal-friend').removeClass('hidden');

		$('body').scrollTop(0);

	});

</script>