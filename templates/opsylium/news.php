<div class="list">

	<h1>ACTUALITÉS</h1>

	<?php

		$stmt = $dbh->prepare('SELECT sl_news.id, sl_news.title, sl_news.content, sl_attachments.filename FROM sl_news INNER JOIN sl_attachments ON sl_news.id = sl_attachments.id_element WHERE sl_attachments.id_module = 27 AND sl_news.enabled = 1');
		$stmt->execute();
		$news = $stmt->fetchAll(PDO::FETCH_OBJ);

		for ($i = 0; $i < count($news); $i++) {

			$photos = json_decode($news[$i]->photos);

			echo '<div class="item">';
				echo '<div class="preview" style="background-image:url(medias/attachments/sl_news/'. $news[$i]->id .'/'. $news[$i]->filename .')"></div>';
				echo '<div class="desc">';
					echo '<h2>'. utf8_decode($news[$i]->title) .'</h2>';
					echo '<p>'. $news[$i]->content .'</p>';
				echo '</div>';
				echo '<div class="clearfix"></div>';
			echo '</div>';

		}

		if (count($news) === 0) { echo '<p>Aucun résultat</p>'; }

	?>
</div>