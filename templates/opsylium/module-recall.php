<div class="modal modal-recall hidden">
	<div class="overlay"></div>
	<form action="index.php">

		<span class="close"><i class="fa fa-times"></i></span>

		<h3>Être rappelé gratuitement</h3>
		<p>Saisissez vos coordonnées. Nous ferons tout notre possible pour vous rappeler dans les plus brefs délais !</p>

		<input name="nom" class="nom" type="text" placeholder="Nom">
		<input name="prenom" class="prenom" type="text" placeholder="Prénom">
		<input name="email" class="email" type="text" placeholder="Email">
		<input name="telephone" class="telephone" type="text" placeholder="Téléphone">
		<input type="submit" class="submit" value="Valider">

	</form>
</div>

<script>
	
	$('.modal-recall form').submit(function(e){

		e.preventDefault();

		data = $(this).serialize();

		error = {
			value: false,
			text: ''
		};

		if ($('div.modal-recall input.nom').val() === '') 			{ error.value = true; error.text = "Saisissez votre nom..."; }
		if ($('div.modal-recall input.prenom').val() === '') 		{ error.value = true; error.text = "Saisissez votre prénom..."; }
		if ($('div.modal-recall input.email').val() === '') 		{ error.value = true; error.text = "Saisissez votre email..."; }
		if ($('div.modal-recall input.telephone').val() === '') 	{ error.value = true; error.text = "Saisissez votre téléphone..."; }

		if (!validateTelephone($('div.modal-recall input.telephone').val())) 	{ error.value = true; error.text = "Saisissez un téléphone valide..."; }
		if (!validateEmail($('div.modal-recall input.email').val())) 			{ error.value = true; error.text = "Saisissez un email valide..."; }

		if (!error.value) {

			$.ajax({
				url: 'templates/opsylium/recall.php?'+ data,
				method: 'GET',
				success: function(){

					alert('Votre demande a bien été prise en compte !');

					$.ajax({
						url: 'templates/opsylium/contact.php?'+ data,
						method: 'GET',
						success: function(){

							// Nothing

						},
						error: function(jqXHR, textStatus, errorThrown){

							console.log(jqXHR);
							console.log(textStatus);
							console.log(errorThrown);
							alert('Une erreur est survenue...');

						}
					});
					
				},
				error: function(jqXHR, textStatus, errorThrown){

					console.log(jqXHR);
					console.log(textStatus);
					console.log(errorThrown);
					alert('Une erreur est survenue lors de l\'envoi de votre demande d\'informations...');

				}
			});		

		} else {

			alert(error.text);

		}	

	});

	$('a.recall').click(function(e){

		e.preventDefault();

		$('.modal-recall').removeClass('hidden');

		$('body').scrollTop(0);

	});

</script>