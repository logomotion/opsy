<?php
 	header('Access-Control-Allow-Origin: *');  

	$to = htmlentities($_GET['emailTo']);
	$from = htmlentities($_GET['emailFrom']);

	$subject = utf8_decode('Un ami vous partage un bien Opsylium.fr');

	$message = '<html><body>';
	$message .= 'Votre ami '. htmlentities($_GET['emailFrom']) . ' souhaite vous partager ce bien sur Opsylium.fr : '. htmlentities($_GET['link']);
	$message .= '</body></html>';

	echo $message;

	$headers = "From: contact@opsylium.com\r\n";
	$headers .= "Reply-To: contact@opsylium.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

	mail($to, $subject, $message, $headers);
	mail($from, $subject, $message, $headers);

?>