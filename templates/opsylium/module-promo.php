<div class="module-promo">
	<div class="carousel">
		<?php

			$stmt = $dbh->prepare('SELECT id, titrepromo, ville, photos FROM biens WHERE promo = "1"');
			$stmt->execute();
			$promos = $stmt->fetchAll(PDO::FETCH_OBJ);

			for ($i = 0; $i < count($promos); $i++) {

				$photos = json_decode($promos[$i]->photos);

				echo '<a href="bien/'. $promos[$i]->id .'">';
					echo '<div class="item" style="background-image:url('. $photos[0] .')">';
						echo '<div class="title">'. $promos[$i]->titrepromo .'</div>';
						echo '<div class="caption">';
							echo '<h2>'. $promos[$i]->ville .'</h2>';
						echo '</div>';
					echo '</div>';
				echo '</a>';

			}

		?>
	</div>
</div>