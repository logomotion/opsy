<?php

	$sql = 'SELECT biens.id, biens.mandat, biens.ville, biens.photos, biens.descriptif, biens.dispositif_fiscal, biens.livraison FROM biens';

	$whereOrAnd = ' WHERE';

	if ($this->sl_param('type')) {

		$sql .= ' INNER JOIN lots ON biens.id = lots.id_bien WHERE lots.type = "'. $this->sl_param('type') .'"';
		$whereOrAnd = ' AND';
	}

	if ($this->sl_param('zone')) {

		switch ($this->sl_param('zone')) {

			case 'bourgogne':
				$cp = '21';
				break;
			case 'rhones-alpes':
				$cp = '69,74';
				break;
		}

		$sql .= $whereOrAnd .' LEFT(biens.codepostal, 2) IN ('. $cp . ')';
	}

	if ($this->sl_param('cp')) {  $sql .= ' AND biens.codepostal IN ('. $this->sl_param('cp') . ') '; }

	if ($this->sl_param('piece')) { $sql .= ' AND lots.piece = '. $this->sl_param('piece'); }

	if ($this->sl_param('surface_min')) { $sql .= ' AND lots.surface >= '. $this->sl_param('surface_min'); }

	if ($this->sl_param('surface_max')) { $sql .= ' AND lots.surface <= '. $this->sl_param('surface_max'); }

	if ($this->sl_param('budget_min')) { $sql .= ' AND lots.prix >= '. $this->sl_param('budget_min'); }

	if ($this->sl_param('budget_max')) { $sql .= ' AND lots.prix <= '. $this->sl_param('budget_max'); }

	$sql .= ' GROUP BY biens.id';

?>

<div class="list">

	<h1>RECHERCHE</h1>

	<?php

		// echo $sql .'<br><br>';

		$stmt = $dbh->prepare($sql);
		$stmt->execute();
		$villes = $stmt->fetchAll(PDO::FETCH_OBJ);

		for ($i = 0; $i < count($villes); $i++) {

			$photos = json_decode($villes[$i]->photos);

			echo '<div class="item">';
				echo '<a class="preview" style="background-image:url('. $photos[0] .')" href="bien/'. $villes[$i]->id .'">';
					if (strtolower($villes[$i]->dispositif_fiscal) === 'pinel') {
						echo '<span class="pastilles pinel"><img src="templates/opsylium/images/pinel.png" alt="Loi Pinel"></span>';
					}
					if (strtolower($villes[$i]->livraison) !== '') {
						echo '<span class="pastilles building">'. $villes[$i]->livraison .'</span>';
					}
				echo '</a>';
				echo '<div class="desc">';
					echo '<a href="bien/'. $villes[$i]->id .'">';
						echo '<h2>';
							echo $villes[$i]->ville .' - '. $villes[$i]->mandat;
							if (strtolower($villes[$i]->dispositif_fiscal) === 'pinel') {
								echo '<span class="tooltip pinel"0 title="Assujetti à la Loi Pinel"></span>';
							}
							if (strtolower($villes[$i]->livraison) !== '') {
								echo '<span class="tooltip building" title="Prévu pour le '. $villes[$i]->livraison .'"></span>';
							}
						echo '</h2>';
					echo '</a>';
					echo '<p>'. truncate($villes[$i]->descriptif, 300) .'</p>';
				echo '</div>';
				echo '<div class="clearfix"></div>';
			echo '</div>';

		}

		if (count($villes) === 0) { echo '<p>Aucun résultat</p>'; }

	?>
</div>