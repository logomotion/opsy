<?php

	header('Access-Control-Allow-Origin: *');

	$xml = new DOMDocument();

	// Create

	$xml_contacts = $xml->createElement('contacts');
	$xml_contact = $xml->createElement('contact');

	$xml_prenom = $xml->createElement('prenom');
	$xml_nom = $xml->createElement('nom');
	$xml_email = $xml->createElement('email');
	$xml_telephone = $xml->createElement('telephone');
	$xml_sexe = $xml->createElement('sexe');
	$xml_adresse = $xml->createElement('adresse');
	$xml_ville = $xml->createElement('ville');
	$xml_code_postal = $xml->createElement('code_postal');
	$xml_remarques = $xml->createElement('remarques');

	$xml_recherches = $xml->createElement('recherches');
	$xml_recherche = $xml->createElement('recherche');

	$xml_recherche_piece = $xml->createElement('piece');
	$xml_recherche_ville = $xml->createElement('ville');
	$xml_recherche_type = $xml->createElement('type');
	$xml_recherche_surface_min = $xml->createElement('surface_min');
	$xml_recherche_surface_max = $xml->createElement('surface_max');
	$xml_recherche_budget_min = $xml->createElement('budget_min');
	$xml_recherche_budget_max = $xml->createElement('budget_max');

	// Setting

	$xml_prenom->nodeValue = isset($_GET['prenom']) ? $_GET['prenom'] : '';
	$xml_nom->nodeValue = isset($_GET['nom']) ? $_GET['nom'] : '';
	$xml_email->nodeValue = isset($_GET['email']) ? $_GET['email'] : '';
	$xml_telephone->nodeValue = isset($_GET['telephone']) ? $_GET['telephone'] : '';
	$xml_sexe->nodeValue = isset($_GET['sexe']) ? $_GET['sexe'] : '';
	$xml_adresse->nodeValue = isset($_GET['adresse']) ? $_GET['adresse'] : '';
	$xml_ville->nodeValue = isset($_GET['ville']) ? $_GET['ville'] : '';
	$xml_code_postal->nodeValue = isset($_GET['code_postal']) ? $_GET['code_postal'] : '';
	$xml_remarques->nodeValue = isset($_GET['remarques']) ? $_GET['remarques'] : '';

	$xml_recherche_piece->nodeValue = isset($_GET['recherche_piece']) ? $_GET['recherche_piece'] : '';
	$xml_recherche_ville->nodeValue = isset($_GET['recherche_ville']) ? $_GET['recherche_ville'] : '';
	$xml_recherche_type->nodeValue = isset($_GET['recherche_type']) ? $_GET['recherche_type'] : '';
	$xml_recherche_surface_min->nodeValue = isset($_GET['recherche_surface_min']) ? $_GET['recherche_surface_min'] : '';
	$xml_recherche_surface_max->nodeValue = isset($_GET['recherche_surface_max']) ? $_GET['recherche_surface_max'] : '';
	$xml_recherche_budget_min->nodeValue = isset($_GET['recherche_budget_min']) ? $_GET['recherche_budget_min'] : '';
	$xml_recherche_budget_max->nodeValue = isset($_GET['recherche_budget_max']) ? $_GET['recherche_budget_max'] : '';

	// Build

	$xml_contact->appendChild($xml_prenom);
	$xml_contact->appendChild($xml_nom);
	$xml_contact->appendChild($xml_email);
	$xml_contact->appendChild($xml_telephone);
	$xml_contact->appendChild($xml_sexe);
	$xml_contact->appendChild($xml_adresse);
	$xml_contact->appendChild($xml_ville);
	$xml_contact->appendChild($xml_code_postal);
	$xml_contact->appendChild($xml_remarques);

	$xml_recherche->appendChild($xml_recherche_piece);
	$xml_recherche->appendChild($xml_recherche_ville);
	$xml_recherche->appendChild($xml_recherche_type);
	$xml_recherche->appendChild($xml_recherche_surface_min);
	$xml_recherche->appendChild($xml_recherche_surface_max);
	$xml_recherche->appendChild($xml_recherche_budget_min);
	$xml_recherche->appendChild($xml_recherche_budget_max);

	$xml_recherches->appendChild($xml_recherche);

	$xml_contact->appendChild($xml_recherches);

	$xml_contacts->appendChild($xml_contact);
	
	$xml->appendChild($xml_contacts);

	// Write

	// $file = __DIR__ . '/'. $_GET['email'] .'-'. date('Y-m-d-His') .'.xml';
	$file = __DIR__ . '/../../../shared/export/'. $_GET['email'] .'-'. date('Y-m-d-His') .'.xml';

	$xml->save($file);