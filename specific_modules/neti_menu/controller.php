 <?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENU
* @internal     Menu module
* @version		neti_menu.php - Version 11.4.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/



// Include models
//include ("models/neti_rub.php");

class neti_menu_controller {

	public $slash;
	public $params;
	public $module_name = "neti_menu";
	public $module_id;
	public $active_lg;
	public $lg;
	public $idr;
	
	// Views
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {

       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
       $this->lg = $this->slash->get_active_lang();

       	// Include views
		if($this->slash->sl_param("mobile") == 1 || $this->slash->mobile->isMobile() == true){
			include ("views/mobile/view.php");
		}
		else{
			include ("views/default/view.php");
		}
		include ("models/neti_menu_models.php");
       
       //Models
		//$this->neti_rub = new neti_rub($this); // On d�clare l'objet pour l'utilisation des fct de rubriquage
		
		//Views
		$this->view = new neti_menu_view($this); // Vue Principale
		$this->neti_menu = new neti_menu($this);


	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {
		
		//Enregistrement rubrique en cours
		/*if ($this->slash->sl_param("idr")) { $_SESSION["neti_idr"] = $this->slash->sl_param("idr"); }
		if (!isset($_SESSION["neti_idr"])) { $_SESSION["neti_idr"] = "root"; }
		$this->idr = $_SESSION["neti_idr"];*/
		
		if ($this->slash->sl_param("idr")) {$_SESSION["front_neti_idr"] = $this->slash->sl_param("idr");}
		if (!isset($_SESSION["front_neti_idr"])) {$_SESSION["front_neti_idr"] = "root";} 
		$this->idr = $_SESSION["front_neti_idr"];
		
		//Enregistrement langue en cours
		if ($this->slash->sl_param("lg")) { $_SESSION["front_neti_lg"] = $this->slash->sl_param("lg"); }
		if (!isset($_SESSION["front_neti_lg"])) { $_SESSION["front_neti_lg"] = $this->lg[0]['id']; }
		$this->active_lg = $_SESSION["front_neti_lg"];
		
		/*$url_language = "";
		
		if (isset($_SESSION["front_user_language"])) {
			$url_language = $_SESSION["front_user_language"];
		}
		
		if($url_language == 'en'){
			$this->active_lg = 2;
		}
		else{
			$this->active_lg = 1;
		}*/
		

		$this->view->header(); //show script header
		
			?>
		<script type='text/javascript'> 

			
 
			$(document).ready(function(){ 
				if($(window).width() < 370 && $(window).width() > 300){
					$('td.menuLink').css('font-size','13px');
				}
				else if($(window).width() < 300){
					$('td.menuLink').css('font-size','11px');
				}
				else if($(window).width() < 250){
					$('td.menuLink').css('font-size','10px');
				}
			}); 

 
		</script>
		<?php
		
		//echo $this->slash->sl_config("global_keywords");
		//echo $this->test;
	}
	
	/**
	* Load function
	*/
	public function load() {
		
		if($this->slash->sl_param("mobile") == 1 || $this->slash->mobile->isMobile() == true){


			//$this->view->start_news();

			$this->view->start_neti_menu_accordion();
			
			$parent = $this->slash->sl_param("parent");
			
			if($this->slash->sl_param("retour")){
				$parent = $this->neti_menu->get_parent_id($this->slash->sl_param("parent"));
			}
			
			if($parent){

				$this->neti_menu->load_neti_menu($parent);
			}
			else{
				$this->neti_menu->load_neti_menu(0);
			}

			$this->view->end_neti_menu_accordion();

		}
		else{
			$this->view->start_main_neti_menu();
			$this->load_neti_menu(0);
			$this->view->end_main_neti_menu();
		}
	}
	
	/**
	 * Load Header function 
	 * 
	 */
	public function load_header(){
		// Header is already sent
	}
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		
	}
	
	
	/**
	* Execute function
	*/
	public function execute() {
		if($this->slash->sl_param("mobile") == 1 || $this->slash->mobile->isMobile() == true){
			$this->view->footer(); 
		}
		else{
			$this->view->execute_neti_menu();
		}
	}

	
	/**
	* Loading neti_menu function
	* @param $parent parent id
	*/
	private function load_neti_menu($parent) {

		
		$result=mysql_query("SELECT * FROM neti_rubpos
									WHERE id_top='".$parent."' AND enabled=1 AND hidden=0 
									ORDER BY neti_rubpos.position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {
			
			if ($parent != 0 ) { $this->view->start_under_neti_menu();}
			
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$this->view->start_neti_menu($row);
				$this->load_neti_menu($row["id"]);
			}
			
			if ($parent != 0 ) { $this->view->end_under_neti_menu();}
			
			
		} else {
			$this->view->end_neti_menu();
		}

	}
	
	
	/**
	 * R�cup�ration du titre d'une rubrique
	 * @param $id rub ID
	 * @param $lg 
	 */
	public function get_title ($id,$lg){
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["title"];
		}else{
			return Null;
		}
		
	}

	/**
	 * Indique si une rubrique est parente d'autres rubriques
	 * @param $id rub ID
	 * @param $lg 
	 */
	public function isParent ($id){
		$return = false;
		$result = mysql_query("SELECT * FROM neti_rubpos ",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
			if($row["id_top"] == $id){
				$return = true;
			}
		}

		return $return;
	}


	public function isResidence ($id){
		$return = false;
		$result = mysql_query("SELECT * FROM neti_rubpos WHERE id_top = 44",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

			$result2 = mysql_query("SELECT * FROM neti_rubpos WHERE id_top = ".$row["id"],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());

			while ($row2 = mysql_fetch_array($result2, MYSQL_ASSOC)) {

				if($row2["id"] == $id){
					$return = true;
				}
			}
		}

		return $return;
	}

	public function get_neti_mobile_ssmenu ($parent){
		$this->neti_menu->load_neti_mobile_ssmenu($parent);
	}

	public function get_parent_id($id_child){
		$result = mysql_query("SELECT id_top FROM neti_rubpos WHERE id='".$id_child."' AND enabled='1'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			
			return $row["id_top"];
		}else{
			return 0;
		}
	}
	


}



?>
