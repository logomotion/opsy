<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENU
* @internal     Menu module
* @version		sl_neti_menu_view.php - Version 11.4.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class neti_menu_view {
	
	public $slash; //Core Reference
	public $controller; //Control Reference
	
	public $slash_text; //Instance of sl_text class
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		
		$this->slash_text = new sl_text;
	}

	public function header () {
		

 		echo "<link rel='stylesheet' type='text/css' href='specific_modules/neti_menu/views/mobile/css/neti_menu_mobile.css' media='screen'> \n";
	}
	
	public function start_main_neti_menu() {
		echo "<ul id='sf-neti_menu' class='sf-neti_menu'> \n";
	}
	public function end_main_neti_menu() {
		echo "</ul> \n";
	}


	public function start_actu() {
		echo "<a href='index.php?mod=sl_news'>";
	}

	
	public function start_neti_menu($row) {
		


		$title = $this->controller->neti_menu->get_title($row["id"],$this->controller->active_lg);
		if (!isset($title) || $title=="") { $title = $this->controller->neti_menu->get_title($row["id"],$this->controller->lg[0]["id"]); }
		
		$addCss = '';
		if ($row['id'] == 46) {
			$addCss = ' class="selector" ';
		}

		if ($row["url"] != ""){
			echo "<a".$addCss." href='".$row["url"]."' target='_blank'>";
			
		}elseif($row["module"] != "") {
			echo "<a".$addCss." href='".$row["module"]."'>";
		}else{
			if($this->controller->neti_menu->check_if_child($row["id"])){
				echo "<a".$addCss." href='index.php?mod=neti_menu&parent=".$row["id"]."'>";
			}
			else{
				echo "<a".$addCss." href='".$this->slash->config["site_url"].$this->slash_text->text2url($title)."-".$row["id"].".php'>";
			}
		}
		?>
		<table class="menu">
			<tbody>
				<tr>
					<td class="menuGauche"></td>
					<td class="menuLink">
						<?php 
						$ajout = '';
						if ($row["id_top"] == 44) {
							$ajout = "résidences en ";
						}
						if ($row["id"] == 51) {
							$ajout = "résidences sur la ";
						}
						echo mb_strtoupper($ajout.$title, 'UTF-8'); ?>
					</td>
					<td class="menuDroite"></td>
				</tr>
			</tbody>
		</table>
		<?php
		echo '</a>';
		if ($row["id_top"] == 44 ) {
			echo '<div>';
			$this->controller->get_neti_mobile_ssmenu($row["id"]);
			echo '</div>';
		}


	}
	
	public function end_neti_menu () {
		echo "</li> \n";
	}
	
	public function start_under_neti_menu () {
		echo "<ul> \n";
	}
	
	public function end_under_neti_menu() {
		echo "</ul></li> \n";
	}
	
	
	
	public function execute_neti_menu() {
	
		// echo "
		// <script type='text/javascript'> 
 
		// 	$(document).ready(function(){ 
		// 	$('#sf-neti_menu').superfish({ 
		//           delay:       1000,                            // one second delay on mouseout 
		//           animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
		//           speed:       300,                          // faster animation speed 
		//           autoArrows:  true,                           // disable generation of arrow mark-up 
		//           dropShadows: false                            // disable drop shadows 
		// 	}); 
		// }); 
 
		// </script>";
	
	}




	public function start_neti_mobile_ssmenu($row){
		
		$title = $this->controller->neti_menu->get_title($row["id"],$this->controller->active_lg);
		if (!isset($title) || $title=="") { $title = $this->controller->neti_menu->get_title($row["id"],$this->controller->lg[0]["id"]); }
		
		if ($row["url"] != ""){
			echo "<a class='ssmenu' href='".$row["url"]."' target='_blank'>";
			
		}elseif($row["module"] != "") {
			echo "<a class='ssmenu' href='".$row["module"]."'>";
		}else{
			if($this->controller->neti_menu->check_if_child($row["id"])){
				echo "<a class='ssmenu' href='index.php?mod=neti_menu&parent=".$row["id"]."'>";
			}
			else{
				echo "<a class='ssmenu' href='".$this->slash->config["site_url"].$this->slash_text->text2url($title)."-".$row["id"].".php'>";
			}
		}

		?>
		<table class="menu">
			<tbody>
				<tr>
					<td class="menuGauche"></td>
					<td class="menuLink">
						<?php echo mb_strtoupper($title, 'UTF-8'); ?>
					</td>
					<td class="menuDroite"></td>
				</tr>
			</tbody>
		</table>
		<?php
		
		echo '</a>';

	}




	public function footer()
	{
		?>
		<div class="footer" id="footer">

		<div class="footerItem">
		    	<a href="#" onclick="window.history.back();return false;" > 
		    		<img src="<?php echo $this->slash->config["mobile_template_url"]; ?>images/fd-footer-retour.jpg">
		    	</a>
			
		</div>
		<div class="footerItem">
			<?php echo "<a href='".$this->slash->config["site_url"].$this->slash_text->text2url($this->controller->neti_menu->get_title('46',$this->controller->active_lg))."-46.php'>"; ?>
				<img src="<?php echo $this->slash->config["mobile_template_url"]; ?>images/fd-footer-contact.jpg">
			</a>
		</div>
		<div class="footerItem">
			<a href="index.php?mod=neti_menu">
				<img src="<?php echo $this->slash->config["mobile_template_url"]; ?>images/fd-footer-menu.jpg">
			</a>
		</div>
		<div class="footerItem">
			<a href="index.php?mod=neti_menu&parent=34">
				<img src="<?php echo $this->slash->config["mobile_template_url"]; ?>images/fd-footer-societe.jpg">
			</a>
		</div>
		<div class="footerItem">
			<?php echo "<a href='".$this->slash->config["site_url"].$this->slash_text->text2url($this->controller->neti_menu->get_title('35',$this->controller->active_lg))."-35.php'>"; ?>
				<img src="<?php echo $this->slash->config["mobile_template_url"]; ?>images/fd-footer-vente.jpg">
			</a>
		</div>

		    <div class="clear"></div>
		</div>
		<?php
	}

	public function start_neti_menu_accordion(){
		echo '<div id="menuLinks">';

		if ($this->slash->sl_param("parent") != '34') {
			echo '<div id="accordion">';
		}
		
	}

	public function end_neti_menu_accordion(){
		echo '</div>';

		if ($this->slash->sl_param("parent") != '34') {
			echo '</div>';
			// echo'<a href="http://clients.logomotion.fr/mrpromotion/index.php?mod=sl_news&id=19" class="news">
			// 	<table class="menu">
			// 	<tbody>
			// 	<tr>
			// 	<td class="menuGauche"></td>
			// 	<td class="menuLink"> ACTUALITES </td>
			// 	<td class="menuDroite"></td>
			// 	</tr>
			// 	</tbody>
			// 	</table>
			// 	</a>';
		}
	}
	
}
