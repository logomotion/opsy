<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENU
* @internal     Menu module
* @version		sl_neti_menu_view.php - Version 11.4.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class neti_menu_view {
	
	public $slash; //Core Reference
	public $controller; //Control Reference
	
	public $slash_text; //Instance of sl_text class
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		
		$this->slash_text = new sl_text;
	}

	public function header () {
		
		echo "<link rel='stylesheet' type='text/css' href='specific_modules/neti_menu/views/default/css/neti_menu.css' media='screen' />";
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/superfish/js/superfish.js'></script> \n";
		echo "<script type='text/javascript' src='core/plugins/jquery_plugins/superfish/js/hoverintent.js'></script> \n";
		//echo "<link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'/> \n";
 		//echo "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.js'></script> \n";
	}
	
	
	public function start_main_neti_menu() {
		echo "<ul id='sf-neti_menu' class='sf-neti_menu'> \n";
	}
	public function end_main_neti_menu() {
		echo "</ul> \n";
	}
	
	public function start_neti_menu($row) {
		
		$title = $this->controller->get_title($row["id"],$this->controller->active_lg);
		if (!isset($title) || $title=="") { $title = $this->controller->get_title($row["id"],$this->controller->lg[0]["id"]); }
		
		if ($row["url"] != ""){
			echo "<li class='current'><a href='".$row["url"]."' target='_blank'>".$title."</a> \n";
			
		}elseif($row["module"] != "") {
			echo "<li class='current'><a href='".$row["module"]."'>".$title."</a> \n";
		}else{
			//test si parent
			if ( $this->controller->isParent($row["id"]) ) {
				$url = "#";
			}
			else{

				$addurl = "";

				if ($this->controller->isResidence($row["id"]) ) {

					$idParent = $this->controller->get_parent_id($row["id"]);

					$ville = "";

					switch ($idParent) {
						case '47':
							$ville = "dijon-";
							break;
						
						case '51':
							$ville = "cannes-";
							break;
						case '52':
							$ville = "montbeliard-";
							break;
					}

					$addurl = "appartement-".$ville."programme-immobilier-";

				}

				$url = $this->slash->config["site_url"].$addurl.$this->slash_text->text2url($title)."-".$row["id"].".php" ;
			}
			
				

			echo "<li class='current'><a href='".$url."'>".$title."</a> \n";
		}
		
		
	}
	
	public function end_neti_menu () {
		echo "</li> \n";
	}
	
	public function start_under_neti_menu () {
		echo "<ul> \n";
	}
	
	public function end_under_neti_menu() {
		echo "</ul></li> \n";
	}
	
	
	
	public function execute_neti_menu() {
	
		// echo "
		// <script type='text/javascript'> 

		// 	$(document).ready(function(){ 
		// 	$('#sf-neti_menu').superfish({ 
		//           delay:       1000,                            // one second delay on mouseout 
		//           animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
		//           speed:       300,                          // faster animation speed 
		//           autoArrows:  true,                           // disable generation of arrow mark-up 
		//           dropShadows: false                            // disable drop shadows 
		// 	}); 
		// }); 

		// </script>";
	
	}

}
