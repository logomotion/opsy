<?php 
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_core_view
* @internal     Admin netissima module
* @version		neti_core_view.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_core_view {

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}

	
	
	
	/**
	* Affiche le titre de la page
	*/
	public function create_title($title,$utitle="") {
		echo "<div class='neti_modtitle'>".$title;
		if ($utitle){ echo "<br/><span class='neti-utitle'>".$utitle."</span>"; }
		echo "</div>";
	}
	
	
	/**
	* Affiche le haut de page
	*/
	public function create_header() {
		
		/* MAIN MESSAGE */
		if (isset($this->controller->message) && $this->controller->message != ""){
			echo "<div id='message' style='
						position:fixed;
						z-index:999;
						overflow: visible; 
						opacity: 100; 
						display: block;  
						right:0; 
						width:500px; 
						top:0; 
						' >
					<table align='center'>
					<tr><td align='center' bgcolor='F7BE77' width='500' height='46'>".$this->controller->message."</td></tr></table>
				  </div>";
		}
		/* --- */
		
		echo "<div class='neti-head'>
				<span class='neti-title'>".$this->slash->trad_word("NETICONTENTS_TL-TITLE")."</span>
				<div id='neti-msg'><div id='neti-message' class='neti-message'></div></div>
			</div>";
	}
	
	/**
	* Fonctions JavaScript NETIssima
	*/
	public function fct_js(){
	
		echo "<script type='text/javascript'>
				
				function show_tab(id) {
					$('#neti-tabs a').removeClass('neti-tabs-active');";
		for($i=0;$i<count($this->controller->lg);$i++){	
			echo "$('#tab_".$this->controller->lg[$i]["id"]."').hide();";
		}	
		echo "		
					$('#tab_'+id).show();
				}
			
			</script>";
	}
	
	/**
	* Cr�ation des onglets de langue
	*/
	public function create_lang_tabs($current=0){
		
		echo "<div id='neti-tabs' class='neti-tabs'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
		
			if ($i==0) { $class = "class='neti-tabs-active'"; } else { $class = ""; }
			
			echo "<a href='javascript:void();' 
			onclick=\"
				show_tab(".$this->controller->lg[$i]["id"].");
				$(this).addClass('neti-tabs-active');
			\" 
			onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-SELECTLANG")." ".$this->controller->lg[$i]["name"]."');\"
			onmouseout=\"$('#neti-message').text('');\" ".$class.">
			<img src='templates/system/images/flags/".$this->controller->lg[$i]["shortname"].".png' width='27' border='0'/></a>";
		}
						
		echo "</div>";
	
	}
	
	/**
	* Cr�ation des onglets de langue pour le preview de la page
	*/
	public function create_lang_tabs_preview($current=0){
		
		echo "<div id='neti-tabs' class='neti-tabs'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
		
			if ($this->controller->active_lg == $this->controller->lg[$i]["id"]) { $class = "class='neti-tabs-active'"; } else { $class = ""; }
			
			echo "<a href='index.php?mod=neti_contents&idr=".$this->controller->idr."&lg=".$this->controller->lg[$i]["id"]."' 
			onmouseover=\"$('#neti-message').text('".$this->slash->trad_word("NETICONTENTS_MSG-SELECTLANG")." ".$this->controller->lg[$i]["name"]."');\"
			onmouseout=\"$('#neti-message').text('');\" ".$class.">
			<img src='templates/system/images/flags/".$this->controller->lg[$i]["shortname"].".png' width='27' border='0'/></a>";
		}
						
		echo "</div>";
	
	}
	
	

}
?>