<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima functions
* @version		netissima.php - Version 11.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_core{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		if (!isset($_COOKIE['simpleTree'])) { setcookie('simpleTree','2',(time() + 3600)); }
	}
	
	
	
	/**
	* Aper�u de la rubrique
	* @param $idr Id Rubrique
	* @param $lg Id Lang
	*/
	public function make_preview ($idr,$lg) {
		if ($this->controller->idr != "root") {
			$this->controller->neti_elm->load_page($idr,$lg);
		}else{			
			echo "<span>".$this->slash->trad_word("NETICONTENTS_TXT-RUBNEEDSEL")."</span>";
		}
	}
	
	
	
	
	
	
}


?>