<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin rub functions
* @version		rub.php - Version 11.3.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_rub{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	 * Récupération des informations d'une rubrique
	 * @param $id rub ID
	 */
	public function load($id) {
		$result = mysql_query("SELECT * FROM neti_rubpos, neti_rubtitles WHERE neti_rubpos.id='".$id."' AND neti_rubtitles.id_rub='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$vals = array();
			$vals["id"] = $id;
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				
				$vals["title-".$row["id_lg"]] = $row["title"];
				if ($i==0) {
					$vals["id_top"] = $row["id_top"];
					$vals["position"] = $row["position"];
					$vals["url"] = $row["url"];
					$vals["module"] = $row["module"];
					$vals["password"] = $row["password"];
					$vals["hidden"] = $row["hidden"];
					$vals["enabled"] = $row["enabled"];
					$vals["is_residence"] = $row["is_residence"];
				}
				$i++;
			}
			return $vals;
		}
	}
	
	
	/**
	 * Récupération des l'ensemble des rubriques
	 * @param $id rub ID
	 */
	public function load_all($lg) {
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$vals = array();
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$vals[$i] = $row;
				$i++;
			}
			return $vals;
		}
	}
	
	
	
	
	
	
	/**
	 * Récupération du titre d'une rubrique
	 * @param $id rub ID
	 * @param $lg 
	 */
	public function get_title ($id,$lg){
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["title"];
		}else{
			return Null;
		}
		
	}

	public function get_is_residence ($id){
		$result = mysql_query("SELECT is_residence FROM neti_rubpos WHERE id='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["is_residence"];
		}else{
			return Null;
		}
		
	}
	
	
}