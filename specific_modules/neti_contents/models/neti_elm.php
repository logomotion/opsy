<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin elements functions
* @version		neti_elm.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_elm{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	 * Charge le contenu d'une rubrique si il y a du contenu
	 * @param $idr Rubrique ID
	 * @param $lg Lang ID
	 */
	public function load_page($idr,$lg){
		
		//$lg=2;
		
		$rub = $this->controller->neti_rub->load($idr);
		
		//Affiche le titre de la page
		if (isset($rub["title-".$lg]) && $rub["title-".$lg]!=""){
			$this->controller->neti_elm_view->show_title($rub["title-".$lg]);
		}else{
			$this->controller->neti_elm_view->show_title($rub["title-".$this->controller->lg[0]['id']]);
		}
		
		$elm = $this->load_elements($idr,$lg);
		
		echo "<ul id='neti-page_".$lg."' class='neti-elm-ul'> \n";
		
		if (count($elm)>0){

			for ($i=0;$i<count($elm);$i++) {
				
			
				
				echo "<li id='li".$elm[$i]["id"]."' class='neti-elm-li' >";
				
				echo "<div>";
				
				
			
				switch($elm[$i]["type"]) {
					
					//Bloc Texte
					case "1": 
						$obj = array();
						$obj = $this->load_txt($elm[$i]["id"],$lg);
						if ($obj==Null){ $obj = $this->load_txt($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_txt($obj);
					break;
					
					//Bloc Image
					case 2: 
						$obj = array();
						$obj = $this->load_img($elm[$i]["id"],$lg);
						if ($obj["images"]==Null){ $obj = $this->load_img($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_img($obj);
					break;
					
					//Bloc Image + Texte
					case 3:
						$obj = array();
						$obj = $this->load_imgtxt($elm[$i]["id"],$lg);
						if ($obj["images"]==Null){ $obj = $this->load_imgtxt($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_imgtxt($obj);
					break;
					
					//Bloc Fichier
					case 4:
						$obj = array();
						$obj = $this->load_file($elm[$i]["id"],$lg);
						if ($obj["files"]==Null){ $obj = $this->load_file($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_file($obj);
					break;
					
					//Bloc Flash
					case 5:
						$obj = array();
						$obj = $this->load_flash($elm[$i]["id"],$lg);
						if ($obj["flash"]==Null){ $obj = $this->load_flash($elm[$i]["id"],$this->controller->lg[0]['id']); }
						$this->controller->neti_elm_view->show_flash($obj);
					break;
					
					//Bloc BR
					Case 6:
						$this->controller->neti_elm_view->show_br();
					break;
					
					default:
						// Nothing
				}
				
				echo "</div></li>";
				
			}
			
			echo "</ul>";
			
		}else{
			echo $this->slash->trad_word("NETICONTENTS_TXT-RUBEMPTY");
		}
	}
	
	/**
	 * Charge l'ensemble des �l�ments dans une page
	 * @param $idr Rubrique ID
	 * @param $lg Lang ID
	 */
	private function load_elements($idr,$lg){
		
		$result = mysql_query("SELECT * FROM neti_elements WHERE id_rub='".$idr."' ORDER BY position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$elm = array();
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$elm[$i] = $row;
				$i++;
			}
			return $elm;
		}else{
			return Null;
		}
	
	}
	
	/**
	 * Charge un �l�ment dans toutes les langues
	 * @param $id ELM ID
	 */
	public function load_element($id){
		$result = mysql_query("SELECT * FROM neti_elements WHERE id='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$obj_elm = array();
			
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			
			$obj_elm["id"] = $row["id"];
			$obj_elm["id_rub"] = $row["id_rub"];
			$obj_elm["type"] = $row["type"];
			$obj_elm["position"] = $row["position"];

			switch($obj_elm["type"]) {
				case "1": //Bloc Texte
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_txt($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["txt-".$this->controller->lg[$i]["id"]] = $obj["txt"];
					}
					break;
				case "2": //Bloc Image
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_img($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["img-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "3": //Bloc Image + Texte
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_imgtxt($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["imgtxt-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "4": //Bloc Fichier
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_file($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["files-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				case "5": //Bloc Flash
					for($i=0;$i<count($this->controller->lg);$i++){
						$obj = array();
						$obj = $this->load_flash($obj_elm["id"],$this->controller->lg[$i]["id"]);
						$obj_elm["flash-".$this->controller->lg[$i]["id"]] = $obj;
					}
					break;
				default:
					
					//Nothing
			}
			return $obj_elm;
		}else{
			return Null;
		}
	}
	
	
	
	
	
	/**
	 * Charge un paragraphe de type texte (1)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_txt($id,$lg){
		$result = mysql_query("SELECT * FROM neti_txt WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			return mysql_fetch_array($result, MYSQL_ASSOC);
		}else{
			return Null;	
		}		
	}
	
	/**
	 * Charge un paragraphe de type image (2)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_img($id,$lg){
		$result = mysql_query("SELECT * FROM neti_img WHERE id_element='".$id."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["images"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}				
	}
	
	/**
	 * Charge un paragraphe de type image + texte (3)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_imgtxt($id,$lg){
		$result = mysql_query("SELECT * FROM neti_imgtxt WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["images"] = $this->load_attachments($id,$lg);
			
			if($arr["images"]==Null) {
				$arr["images"] = $this->load_attachments($id,$this->controller->lg[0]['id']);
			}
				 
			return $arr;
		}else{
			return Null;	
		}	
	}
	
	/**
	 * Charge un paragraphe de type fichier (4)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_file($id,$lg){
		$result = mysql_query("SELECT * FROM neti_file WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["files"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}		
	}
	
	
	/**
	 * Charge un paragraphe de type flash (5)
	 * @param $id Elm ID
	 * @param $lg Lang ID
	 */
	private function load_flash($id,$lg){
		
		$result = mysql_query("SELECT * FROM neti_flash WHERE id_element='".$id."' AND id_lg='".$lg."' LIMIT 1",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$arr = array();
			$arr = mysql_fetch_array($result, MYSQL_ASSOC);
			$arr["flash"] = $this->load_attachments($id,$lg);	 
			return $arr;
		}else{
			return Null;	
		}		
				
	}
	
	
	
	
	/**
	 * Charge un fichier / image jointe
	 */
	private function load_attachments($id_elm,$lg) {
		
		$id_mod = $this->slash->sl_module_id("neti_contents","site");
		
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_element='".$id_elm."' AND state=1 AND id_module='".$id_mod."' AND id_field='".$lg."' ORDER BY position asc",$this->slash->db_handle) 
			or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {	
			$attachs = array();
			$i=0;
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){	
				$attachs[$i]=$row;
				$i++;
			}
			return $attachs;
		}else{
			return null;
		}
		
	}
	
	
	
	
	
}

?>