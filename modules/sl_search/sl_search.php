 <?php
/**
* @package		SLASH-CMS
* @subpackage	sl_search
* @internal     front search module
* @version		sl_search.php - Version 13.03.18
* @author		Loïc Bajard
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

include ("views/default/sl_search_view.php");
include ("models/sl_search_model.php");

class sl_search extends slController implements iController {


	/* --- Variables nécessaires --- */
	public $module_name = "sl_search"; //Nom du module
	public $model;
	public $view;
	
	/**
	* Contructeur de la classe, permet l'initialisation du module
	*/
	function sl_construct() {
       $this->model = new sl_search_model($this);
       $this->view = new sl_search_view($this);
	}
	
	public function load_header(){
		$this->view->header(); //on affiche le header (voir fonction d'affichage sl_search_view.php)
	}
	
	public function load() {
		// Traitement des données
		
		if (isset($_GET['commune'])) 
		{
			$datas = $this->model->loadSearchResultsResidence($_GET); //Appel d'une fonction personnalisée
			$this->view->show_results($datas); // Appel d'une fonction personnalisée d'affichage (voir sl_search_view.php)
		}
		else
		{		
			$this->view->show_form($this->model->getCommunes());
		}
	}
	
	public function load_footer(){
		$this->view->footer(); //on affiche le footer (voir fonction d'affichage sl_search_view.php)
	}
	
	

}

?>
