<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_search
* @internal     front search module
* @version		sl_search.php - Version 13.03.18
* @author		Loïc Bajard
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

class sl_search_model extends slModel implements iModel
{
	/**
	* Return a list of results based on a query string
	* @param $query : String Query string to be parsed for the search
	* @return Array : Search results
	*/

	public function loadSearchResultsResidence($query)
	{

		$rqt='(';
		foreach ($query as $key => $value) 
		{
			if($key<>"commune" && $key<>"mod")	$rqt.=$key."='".$value."' OR ";
		}
		$rqt = substr($rqt, 0, -4).") "; 
		$results = array();

		$sql = "SELECT * FROM neti_rubtitles t LEFT JOIN neti_rubpos p ON p.id=t.id_rub WHERE p.commune ='".$query["commune"]."' AND p.url !='#' AND ".$rqt." GROUP BY id_rub";

		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			$content="<ul class='type'>";
			for($i=1;$i<=6;$i++)
			{
				if( $res['t'.$i]>0 && array_key_exists('t'.$i,$query))	{	$content.="<li>Type".$i."</li>";}
			}
			if($res['duplex']>0){$content.="<li>Duplex</li>";	}	
			$content.="</ul>";

			$results[$res['id_rub']] = array('commune' => $res['commune'], 'title' => $res['title'] ,'content' => $content,'id_rub'=>$res['id_rub']);

		}

		return $results;

	}
	public function loadSearchResults($query){

		$query = htmlentities($query,ENT_COMPAT,"UTF-8");

		$results = array();
		// Search in titles
		$sql = "SELECT title,id_rub FROM neti_rubtitles t LEFT JOIN neti_rubpos p ON p.id=t.id_rub WHERE title LIKE '".$query."%' AND p.url !='#' GROUP BY id_rub";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			$results[$res['id_rub']] = array('score' => 5, 'title' => $res['title'] ,'content' => "content",'id_rub'=>$res['id_rub']);

		}

		$sql = "SELECT title,id_rub FROM neti_rubtitles t LEFT JOIN neti_rubpos p ON p.id=t.id_rub WHERE title LIKE '%".$query."%' AND p.url !='#' GROUP BY id_rub";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			if(!in_array($res['id_rub'], $results)) $results[$res['id_rub']] = array('score' => 3, 'title' => $res['title'],'content' => "",'id_rub'=>$res['id_rub']);
		}


		// Search in contents

				//neti_imgtxt
		$sql = "SELECT * FROM neti_imgtxt LEFT JOIN neti_elements e ON e.id = neti_imgtxt.id_element WHERE txt LIKE '%".$query."%' AND id_rub != '102'";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			$s = substr_count(strtolower($res['txt']), strtolower($query));
			$content = $this->formatContent($res['txt'],$query);
			if(in_array($res['id_rub'],$results)){
				$results[$res['id_rub']]['score'] += $s;
				$results[$res['id_rub']]['content'] .= "<p>".$content."</p>";
			}else{
				$results[$res['id_rub']] = array();
				$results[$res['id_rub']]['score'] = $s;
				$results[$res['id_rub']]['content'] = "<p>".$content."</p>";
				$results[$res['id_rub']]['id_rub'] = $res['id_rub'];
			}
		}

			
		//neti_txt
		$sql = "SELECT * FROM neti_txt LEFT JOIN neti_elements e ON e.id = neti_txt.id_element WHERE txt LIKE '%".$query."%' AND id_rub != '102'";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			$s = substr_count(strtolower($res['txt']), strtolower($query));
			$content = $this->formatContent($res['txt'],$query);
			if(isset($results[$res['id_rub']])) {
			
				$results[$res['id_rub']]['score'] += $s;
				$results[$res['id_rub']]['content'] .= "<p>".$content."</p>";
			}else{
				$results[$res['id_rub']] = array();
				$results[$res['id_rub']]['score'] = $s;
				$results[$res['id_rub']]['content'] = "<p>".$content."</p>";
				$results[$res['id_rub']]['id_rub'] = $res['id_rub'];
			}
		}

		if(empty($results)) return;
		$rtitles = array();
		// Finally, get missing titles
		foreach ($results as $k => $r){
			if(!isset($r['title'])){
				$rtitles[] = $k;
			} 
		}
		$sql = "SELECT title,id_rub FROM neti_rubtitles WHERE id_rub IN(".implode(",", $rtitles).")";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($res = mysql_fetch_assoc($req)) {
			$results[$res['id_rub']]['title'] = $res['title'];
		}



		
		// Get titles and contents

		// sort the results by score
		function sortByOrder($a,$b){
			return $b['score'] - $a['score'];

		}
		usort($results,'sortByOrder');
		return $results;
	}

	/**
	* Strip html code from content and highlight keyword
	*/
	private function formatContent($content,$keyword){

		// Strip html code
		$content = preg_replace("/<\/?\w+ ?\/?>/ism", "", $content);
		// Keep only the phrase start
		$matches = array();
		preg_match_all('/([A-Z]?[a-z\sàäâéèëêïîöôòùüû,&$£\*%:;]{1,35})('.$keyword.')((?(?=.{0,35}\.).{0,35}\.|.{1,35}\s))/ism',$content, $matches);
		$content = "";
		foreach ($matches[0] as $m) {
			$content .= preg_replace('/(.*)('.$keyword.')(.*)/ism', '$1<strong>$2</strong>$3', $m);
		}
		return $content;
	}

	public function getCommunes()
	{

		$sql = "SELECT DISTINCT commune from neti_rubpos";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		$tabCommunes = array();

		while($data = mysql_fetch_assoc($req)) 
    	{     	
    		if( $data['commune']){array_push($tabCommunes, $data['commune']);}
    	} 
    	return $tabCommunes;
    	
		
		 
		//$results = mysql_fetch_array($req);
		//print_r(mysql_fetch_array($req));
		//return (mysql_fetch_assoc($req));
	}

}
?>