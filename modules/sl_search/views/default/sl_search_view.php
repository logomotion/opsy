<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_search
* @internal     front search module
* @version		sl_search.php - Version 13.03.18
* @author		Loïc Bajard
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
* Ce fichier est un exemple pour la création d'un module front.
* Cette partie represente la partie affichage du module.
*/

class sl_search_view extends slView implements iView{

	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash_seo = new sl_seo;
	}

	/**
	* Fonction d'affichage du header (généralement les scripts et css)
	* A noter : Certains scripts son déjà inclus de base (Jquery.js), 
	* Il n'est donc pas nécessaire de les redéclarer
	*/
	public function header () {
		echo "<link rel='stylesheet' type='text/css' href='modules/sl_search/views/default/css/sl_search.css'>";
		echo "<link rel='stylesheet' type='text/css' href='templates/opsylium/css/form.css'>";		
		echo "<link rel='stylesheet' type='text/css' href='templates/opsylium/css/styles.css'>";
		echo "<link rel='stylesheet' href='specific_modules/neti_contents/views/mrpromotion/css/styles.css' type='text/css'>";
		echo "<title>OPSYLIUM - Recherche</title>";
	}


	
	
	/**
	* Fonction d'affichage du module
	*/
	public function show_results($datas) 
	{
	foreach ($datas as $key => $r) {$commune = $r['commune'];	}

		echo "</br><span class='searchResultPageTitle'>résultats de recherche</span></br>
		<span class='searchResultLocation'>Dans la commune de ".$commune."</span></br></br>";
		if($datas){
			echo "<ul class='searchResultList'>";
				foreach ($datas as $key => $r) {
					$url = "index.php?mod=neti_contents&idr=";
					$sani = utf8_decode($r['title']);
					$sani = $this->slash_seo->seo_text($sani);
					$sani = $this->slash_seo->str_sanitize($sani);

					echo "<li class='searchResult'>";
					echo "<div class='searchResultArea'>";
					echo "<a title='Acc&eacute;der &agrave; la page' href='".$url.$r['id_rub']."'>";
					
					//echo "<h2 class='searchResultTitle'><a title='Acc&eacute;der &agrave; la page' href='".$sani."-".$r['id_rub'].".html'>".$r['title']."</a></h2>";
					echo "<h2>".$r['title']."</h2>";
					echo "<p>".$r['content']."</p>";
					echo "</a></div></li>";
				}
			echo "</ul>";
		}else{
			echo "<p>Aucun r&eacute;sultat</p>";
		}
	}

	/**
	* Fonction d'affichage du footer (généralement l'execution des scripts)
	*/
	public function footer() {

		
	}

	public function show_form($datas)
	{
		echo"<link rel='stylesheet' href='templates/opsylium/css/form.css' type='text/css'>";
		echo "<form name='form_recherche' method='get' action='search.html'>";
		

		
		//Combobox listant les différentes villes
		echo "
		<ul>


	  	
	        <li class='title'>
	        	<label for='name' style='width:100%;'  name='field_474' >Rechercher un bien</label> 
	        </li>
	        </ul>

</br></br></br></br></br></br>
	        <ul>

	        	<li>
	        		<label for='name'>Commune :</label>
					<select name='commune'>	";
		foreach($datas as $value) 
		{
   			echo("<option value='".$value."'>".$value."</option>");
		}
		echo "</select></li>
	    <li>
	    <label for='name'>Types d'appartement recherch&eacute;s :</label></br></br></br>
		<label><input type='checkbox' name='t1' value='1'/>Type T1</label>
		<label><input type='checkbox' name='t2' value='1'/>Type T2</label>
		<label><input type='checkbox' name='t3' value='1'/>Type T3</label>
		<label><input type='checkbox' name='t4' value='1'/>Type T4</label>
		<label><input type='checkbox' name='t5' value='1'/>Type T5</label>
		<label><input type='checkbox' name='t6' value='1'/>Type T6</label>
		<label><input type='checkbox' name='duplex' value='1'/>Type Duplex</label>
		</br></br>
		</li>
		</br></br>
		<li>
			<button type='submit' class='action' onclick='document.form_recherche.submit();'>Rechercher</button>
		
		</li>
		</ul>

		</form>
		";
	}


}

?>