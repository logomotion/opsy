<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_HEADER
* @internal     Header module
* @version		sl_header.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


include ("views/default/sl_header_view.php");

class sl_header extends sl_header_view implements iController{

	public $slash;
	public $params;
	public $module_id;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {

		// CHECK PAGE METAS OR SHOW GLOBAL METAS !!!
		
		$this->title($this->slash->config["site_name"]);
		$this->metas($this->slash->config["global_description"],$this->slash->config["global_keywords"]);
		
		//echo $this->slash->sl_config("global_keywords");
		$this->scripts();
	
	}
	
	/**
	 * Load Header function 
	 * 
	 */
	public function load_header(){
		// Header is already sent
	}
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		// Footer is already sent
	}
	
	/**
	* Load function
	*/
	public function load() {
		

	}
	
	
	/**
	* Execute function
	*/
	public function execute() {
	
	
	
	}

}





?>