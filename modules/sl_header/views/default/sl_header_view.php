<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_HEADER
* @internal     Header module
* @version		sl_header_view.php - Version 9.12.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sl_header_view implements iView{


	
	/**
	* Show title
	*/
	protected function title ($title) {
		
		echo "<title>Opsylium - Appartements à vendre sur Dijon/Lyon</title>";
	}
	
	/**
	* Show meta data
	*/
	protected function metas ($description,$keywords) {
	
		//<meta http-equiv='Content-Type'  content='text/html; charset=utf-8' />
		echo "	<meta http-equiv='Content-Type'  content='text/html; charset=utf-8' />";
			/*
				<meta name='description' content='".$description."' />
				<meta name='keywords' content='".$keywords."' />
				<meta name='expires' content='never' />
				<meta name='language' content='FR' />";*/

	
	}
	
	
	/**
	* Show script
	*/
	protected function scripts () {
		
		// echo "<script type='text/javascript' src='./core/plugins/jquery/jquery.js'></script> \n";
		// echo "<link href='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css' rel='stylesheet' type='text/css'/> \n";
		// echo "<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.5/jquery.min.js'></script> \n";
		// echo "<script type='text/javascript' src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js'></script> \n";
		// echo "<script type='text/javascript' src='./core/plugins/jquery/jquery.dimensions.js'></script> \n";
		// echo "<script type='text/javascript' src='./core/plugins/jquery/jquery.bgiframe.js'></script> \n";
		// echo "<script type='text/javascript' src='./core/plugins/jquery/jquery.delegate.js'></script> \n";

		echo '<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>';		
		echo '<script src="http://code.jquery.com/ui/1.11.4/jquery-ui.js"></script>';		
		
	}


}





?>