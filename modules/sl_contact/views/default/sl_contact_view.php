<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_contact
* @internal     front contact module
* @version		sl_contact.php - Version 13.03.18
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

/**
* Ce fichier est un exemple pour la création d'un module front.
* Cette partie represente la partie affichage du module.
*/

class sl_contact_view extends slView implements iView{

	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash_seo = new sl_seo;
	}

	/**
	* Fonction d'affichage du header (généralement les scripts et css)
	* A noter : Certains scripts son déjà inclus de base (Jquery.js), 
	* Il n'est donc pas nécessaire de les redéclarer
	*/
	public function header () {
		//echo '<link rel='stylesheet' type='text/css' href='modules/sl_search/views/default/css/sl_search.css'>\n';
		echo "<link rel='stylesheet' type='text/css' href='templates/opsylium/css/form.css'>";
		echo "<title>Contact</title>";
	}

	public function show_form($datas,$datasResidences)
	{

	$subject = "Demande de contact";
	$onlyMessage = true;

	if(isset($_GET["alert"]) && $_GET["alert"]==1)
	{
		$subject = "Demande d'alerte";
		$onlyMessage = false;
	}
	if(isset($_GET["idRes"]) && isset($_GET["sub"]))
	{
		$onlyMessage = false;
		$resIsDisabled = "disabled";
		if($_GET["sub"]=="plan")
		{
			$subject = "Demande de plan type pour une r&eacute;sidence";
			
		}
		else
		{
			$subject = "Demande d'informations sur une r&eacute;sidence";
		}
	}

	
		
	echo"
	<link rel='stylesheet' href='templates/opsylium/css/form.css' type='text/css'>
	<link rel='stylesheet' href='specific_modules/neti_contents/views/mrpromotion/css/styles.css' type='text/css'>
	
	<link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300|Playfair+Display:400italic' rel='stylesheet' type='text/css' />

	<!--[if IE]>
	  <script src='http://html5shiv.googlecode.com/svn/trunk/html5.js'></script>
	<![endif]-->
	    <script src='http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js' type='text/javascript' charset='utf-8'></script>
	    <script src='js/jquery.uniform.min.js' type='text/javascript' charset='utf-8'></script>
	    <script type='text/javascript' charset='utf-8'>
	      $(function(){
	        $('input:checkbox, input:radio, input:file, select').uniform();
	      });
	    </script>
	</head>
		<body>
	<form name='formissima_form' method='post' action='http://formissima.logomotion.fr/forms/form_process.php?idform=63' onSubmit='return verification()' enctype='multipart/form-data'>
			
			
		  	<ul>		  	
	        <li class='title'>

	        	<label for='name' style='width:100%;'  name='field_474' >".$subject."</label> 


	        </li>

	        </br></br></br></br></br></br></br>
	        ";
	        if(!$onlyMessage){echo"	
	        <li>

	        	<label for='email'>R&eacute;sidence concern&eacute;e :</label>

	        	<select name='field_493' ".$resIsDisabled.">	";         
					foreach($datasResidences as $value) 
					{
	   					echo("<option value='".$value."'>".$value."</option>");
					}
					echo "</select>
	          
	        </li>
	        <li style='height:120px'>Types d'appartement qui vous int&eacute;ressent
	        </br></br>
	           	<label><input name='field_476' type='checkbox' value='1' />T1</label>
	        	<label><input name='field_477' type='checkbox' value='1' />T2</label>
	        	<label><input name='field_478' type='checkbox' value='1' />T3</label>
	        	<label><input name='field_479' type='checkbox' value='1' />T4</label>
	        	<label><input name='field_480' type='checkbox' value='1' />T5</label>
	        	<label><input name='field_481' type='checkbox' value='1' />T6</label>
	        	<label><input name='field_482' type='checkbox' value='1' />Duplex</label>
			</li>
			";}
			echo"



	        Vos coordonnées
	        <li>
	        	<label for='nom'>Votre nom : </label></br></br>
	        	<input type='text' name='field_484' class='input_texte' size='40' value='' onFocus='if(document.formissima_form.field_484.value == ''){document.formissima_form.field_484.value='';}' >
	        </li>
	        <li>
	        	<label for='Email'>Votre E-mail : </label></br></br>
	        	<input type='text' name='field_483' class='input_texte' size='40' value='' onFocus='if(document.formissima_form.field_483.value == ''){document.formissima_form.field_483.value='';}' >
	        </li>
	        <li>
	        	<label for='telephone'>Votre téléphone : </label></br></br>
	        	<input type='text' name='field_485' class='input_texte' size='40' value='' onFocus='if(document.formissima_form.field_485.value == ''){document.formissima_form.field_485.value='';}' >
	        </li>
	        <li>
	        	<label for='message'>Votre message : </label></br></br>
	        	<textarea name='field_492' class='input_texte' rows='50' cols='40' onFocus='if(document.formissima_form.field_492.value == ''){document.formissima_form.field_492.value='';}'></textarea>
	        </li>

			Anti-spam
	        <li>
	        	<label for='code'>Code : </label>
	        	<img src='http://formissima.logomotion.fr/forms/anti_spam.php?name=inscription&strlen=5&forbugie=code=1384421696' alt='anti-flood' />
	        	</br>
	       	 	<label for='message'>(Recopiez le code affiché) : </label>
	        	<input type='text' name='spam' class='input_texte' size='23'/>
	        </li>

		      <p>
		      	<button type='submit' class='action'>Envoyer</button>
	        	<button type='reset' class='right'>Recommencer</button>
		      </p>
		      </form>
		              
		              
		              
		  <script language='JavaScript'> function verification() { if (document.formissima_form.field_483.value.length == 0) {alert('Veuillez indiquer le champ Votre email'); return false; }if (document.formissima_form.field_484.value.length == 0) {alert('Veuillez indiquer le champ Votre nom'); return false; }if (document.formissima_form.field_485.value.length == 0) {alert('Veuillez indiquer le champ Votre téléphone'); return false; }if (document.formissima_form.field_492.value.length == 0) {alert('Veuillez indiquer le champ Votre message'); return false; }if (document.formissima_form.spam.value.length == 0) {alert('Please enter anti-spam code'); return false; }return true;} </script>
		  ";



	}


	
	

}

?>