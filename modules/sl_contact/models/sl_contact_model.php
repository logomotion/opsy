<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_contact
* @internal     front contact module
* @version		sl_contact.php - Version 13.03.18
* @author		Alexis CORNELISSENS
* @copyright	Copyright(C) 2013 - Today. All rights reserved.
* @license		GNU/GPL

This program is free software : you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.

*/

class sl_contact_model extends slModel implements iModel
{
	/**
	* Return a list of results based on a query string
	* @param $query : String Query string to be parsed for the search
	* @return Array : Search results
	*/


	public function getCommunes()
	{

		$sql = "SELECT DISTINCT commune from neti_rubpos";
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		$tabCommunes = array();

		while($data = mysql_fetch_assoc($req)) 
    	{     	
    		if( $data['commune']){array_push($tabCommunes, $data['commune']);}
    	} 
    	return $tabCommunes;
    	
		
		 
		//$results = mysql_fetch_array($req);
		//print_r(mysql_fetch_array($req));
		//return (mysql_fetch_assoc($req));
	}

	public function getResidences($idRes)
	{

		

		if (isset($idRes))
		{
			$sql = "SELECT title from neti_rubtitles WHERE id_rub=".$idRes;			
		}
		else
		{
			$sql = "SELECT title from neti_rubtitles as t, neti_rubpos as p WHERE t.id_rub=p.id AND is_residence =1";
		}
		

		/*if($idRes!="")
		{
			$sql = "SELECT title FROM neti_rubtitles WHERE id_rub = ".$idRes;	
		}
		else
		{
			
			//$sql = "SELECT title FROM neti_rubtitles WHERE id_rub =  (SELECT id from neti_rubpos where is_residence=1)";
		}*/
		
		$req = mysql_query($sql,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		$tabResidences = array();

		while($data = mysql_fetch_assoc($req)) 
    	{     	
    		if( $data['title']){array_push($tabResidences, $data['title']);}
    	} 
    	return $tabResidences;
		 
		//$results = mysql_fetch_array($req);
		//print_r(mysql_fetch_array($req));
		//return (mysql_fetch_assoc($req));
	}

}
?>