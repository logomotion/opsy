<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_PAGES
* @internal     Front page module
* @version		pages.php - Version 12.02.13
* @author		Julien veuillet
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

class pages extends slModel implements iModel{
	

	/**
	* Load page
	*/
	public function load_page($id){
	
		if ($id) {
			$result = mysql_query("SELECT * FROM sl_pages WHERE enabled=1 AND id=".intval($id),$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row;
		} else {
			return NULL;
		}
		
	}
	
	
	
	/*
	public function load_attachments($id,$id_module,$where){
		$attachments = array();
		
		$i=0;
		$result = mysql_query("select * from ".$this->slash->database_prefix."attachments where id_module = '".$id_module."' and id_element = '".$id."' and state = 1 ".$where." order by position asc ");
		while($row = mysql_fetch_array($result))
		{
			$attachments[$i]["filename"] = $row["filename"];
			$attachments[$i]["id_field"] = $row["id_field"];
			$i++;
		}
		
		return $attachments;
	}
	*/
	
}
?>