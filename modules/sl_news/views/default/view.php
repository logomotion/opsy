<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_news
* @internal     news module
* @version		sl_news_view.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sl_news_view extends slView implements iView{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	public $slash_text; //Instance of sl_text class
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		
		$this->slash_text = new sl_text;
	}


	public function header () {
		
	}
	
	
	public function start_main_news() {
		echo "<div class='container_news'> \n";
		echo "<div class='slide_container_news slide_control'> \n";
		
		
	}
	
	public function end_main_news() {
		
		echo "</div> \n";
		echo "</div> \n";
	}
	
	public function start_news($id,$title,$content,$filename) {
		
		$obj_sl_txt = new sl_text();
		
		echo "<div class='news-gene'> \n";
			echo "<div class='news-fleche-gauche'> \n";
				echo "<a href='#' class='prev' \n>";
					echo "<img src='templates/mr_promotion/images/fleche_gauche.png'  alt='MR Promotion' width='54' height='40' /> \n";
				echo "</a> \n";
			echo "</div> \n";
			echo "<div class='news-texte'> \n";
				
			
				echo "<div class='news-texte-titre'>".$title."</div> \n";
				// mettre un strip-tag ici (mais garder les <br/>)
				$content = strip_tags($content, '<br>');
				echo "<div class='news-texte-content'>".$obj_sl_txt->substring_word($content,150,true);
				$titreUrl = $this->slash_text->text2url($title);
				echo "<br /><a href='actualites-".$titreUrl."-".$id.".php'>En savoir plus</a>";
				echo "</div> \n";
			echo "</div> \n";
			echo "<div class='news-image' id='news".$id."'> \n";
				echo "<a href='#' title=\"".$title."\"> \n";
					echo "<img src='medias/attachments/sl_news/".$id."/".$filename."'  alt='MR Promotion' width='195' height='101' /> \n";
				echo "</a> \n";
			echo "</div> \n";	
			echo "<div class='news-fleche-droite'> \n";
				echo "<a href='#' class='next'> \n";
					echo "<img src='templates/mr_promotion/images/fleche_droite.png'  alt='MR Promotion' width='54' height='40' /> \n";
				echo "</a> \n";
			echo "</div> \n";

			
		echo "</div> \n";
		
			
	}

	public function one_news($id,$title,$content,$filename) {
		
		$obj_sl_txt = new sl_text();
		
		echo "<div class='news-detail'> \n";
			
			echo "<div class='news-detail-title'>";
				echo "<div class='news-detail-title-angle'></div>";
				echo "<div class='news-detail-title-fond'>".$title."</div>";
			echo "</div>";	
			
			
			echo "<div class='news-detail-image' id='news-detail".$id." > \n";
				echo "<a href='#' title=\"".$title."\" > \n";
				$url = "medias/attachments/sl_news/".$id."/".$filename;
					
				echo sl_images::show_image($url,885);
				echo "</a> \n";
			echo "</div> \n";	
			echo "<div class='news-detail-texte-content'>".$content. "</div> \n";
			echo "<div class='news-detail-foot-page'></div>";	
		echo "</div> \n";
		
		
	}

	
	public function end_news () {
		
		
	}
	
	public function start_under_news () {
		
	}
	
	public function end_under_news() {
		
	}
	
	
	
	public function execute_news() {
		
		/*echo "
		<script type='text/javascript'> 
 
			$(document).ready(function(){ 
			$('#sl_news').superfish({ 
            delay:       1000,                            // one second delay on mouseout 
            animation:   {opacity:'show',height:'show'},  // fade-in and slide-down animation 
            speed:       300,                          // faster animation speed 
            autoArrows:  true,                           // disable generation of arrow mark-up 
            dropShadows: false                            // disable drop shadows 
			}); 
		}); 
 
		</script>";*/
	
	}
	
}





?>