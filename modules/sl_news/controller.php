 <?php
/**
* @package		SLASH-CMS
* @subpackage	sl_news
* @internal     Menu module
* @version		sl_news.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

// Include views
include ("views/default/view.php");
// Include models
include ("models/news.php");

class sl_news_controller extends slController implements iController{

	public $module_name = "sl_news";
	
	public $news;
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function sl_construct() {
       
	   
	   $this->news = new news($this);
	   $this->view = new sl_news_view($this);
	   
	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {
		
		
		
		$this->view->header(); //show script header
		
		//echo $this->slash->sl_config("global_keywords");
		//echo $this->test;
	}
	
	/**
	* Load function
	*/
	public function load() {
		
		
		if ($this->params == "widget" ) {
			$this->view->start_main_news();
			$this->news->load_news();
			$this->view->end_main_news();
		}else{
			if(isset($this->slash->get_params["id"])){
				$this->news->load_news_specific($this->slash->get_params["id"]);
			}
			else{
				$this->news->load_news_all();
			}

			
		}
		
	}
	
	
	
	
	/**
	* Execute function
	*/
	public function execute() {
		$this->view->execute_news();
	}

	

}



?>
