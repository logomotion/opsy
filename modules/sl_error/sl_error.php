 <?php
/**
* @package		SLASH-CMS
* @subpackage	SL_ERROR
* @internal     Error module
* @version		sl_error.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


include ("views/default/sl_error_view.php");

class sl_error extends sl_error_view implements iController{

	public $slash;
	public $params;
	public $module_id;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {
		//no global initialisation for this module
	}
	
	
	/**
	 * Load header function
	 */
	public function load_header(){
		$this->header();
	}
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		$this->footer();
	}
	
	
	/**
	* Load function
	*/
	public function load() {
		
		$this->show_error($this->slash->sl_param("id","GET"));
		
	}
	
	
	
	
	/**
	* Execute function
	*/
	public function execute() {
		
	}


}



?>
