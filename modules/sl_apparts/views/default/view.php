<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_apparts
* @internal     apparts module
* @version		sl_apparts_view.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sl_apparts_view extends slView implements iView{

	public function header () {

	}
	
	public function start_main_apparts() {
		echo "<div class='container_apparts'> \n";
	}
	
	public function end_main_apparts() {
		echo "</div> \n";
	}
	
	public function start_appart($id,$nom,$prestations,$description,$type,$residence) {
		echo "id = $id </br> nom = $nom </br> prestations = $prestations </br> description = $description 
		</br> type = $type </br> residence = $residence ";		
	}
	
	public function end_apparts () {
		
	}
	
	public function start_under_apparts () {
		
	}
	
	public function end_under_apparts() {
		
	}
	
	public function execute_apparts() {
	
	}
}
?>