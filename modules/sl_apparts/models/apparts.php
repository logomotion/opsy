<?php
/**
* @package		SLASH-CMS
* @subpackage	sl_appartsS
* @internal     FRONT Menu module
* @version		menus.php - Version 12.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class apparts extends slModel implements iModel {

	/**
	* Loading menu function
	* @param $parent parent id
	*/
	public function load_apparts_specific($id)  {
		$moduleApparts = $this->slash->sl_module_id("sla_apparts","admin");

		$result = mysql_query("SELECT b.titre as type , c.nom as residence , a.nom as nom , a.prestations as prestations , a.description as description 
								FROM sl_apparts as a , sl_apparts_types as b , sl_residences as c 
								WHERE a.id = '".$id."' 
								AND a.enabled = '1' 
								AND a.type = b.id 
								AND a.id_res = c.id "
								,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if( mysql_num_rows($result)  > 0 ) {

			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {

				$this->controller->view->start_appart($id,$row["nom"],$row["prestations"],$row["description"],$row["type"],$row["residence"]);

			}
		}
		else {
			$this->controller->view->end_apparts();
		}
		
		$this->controller->view->end_apparts();
	}
	


	
}

?>