<?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENUS
* @internal     FRONT Menu module
* @version		menus.php - Version 12.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class menus extends slModel implements iModel {

	

	/**
	* Loading menu function
	* @param $parent parent id
	*/
	public function load_menu($parent) {
	
		$result = mysql_query("SELECT * FROM sl_menu WHERE parent=".$parent." AND pri_type = 2 AND enabled='1' ORDER by position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if( mysql_num_rows($result)  > 0 ) {
			
			if ($parent != 0 ) { $this->controller->view->start_under_menu();}
			
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$this->controller->view->start_menu($row["title"],$row["sec_type"],$row["action"]);
				$this->load_menu($row["id"]);
			}
			
			if ($parent != 0 ) { $this->controller->view->end_under_menu();}
			
		} else {
			$this->controller->view->end_menu();
		}
		
		
	}
	
}

?>