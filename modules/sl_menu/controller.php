 <?php
/**
* @package		SLASH-CMS
* @subpackage	SL_MENU
* @internal     Menu module
* @version		sl_menu.php - Version 9.6.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

// Include views
include ("views/default/view.php");
// Include models
include ("models/menus.php");

class sl_menu_controller extends slController implements iController{

	
	public $menus;
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function sl_construct() {
       
	   
	   $this->menus = new menus($this);
	   $this->view = new sl_menu_view($this);
	   
	}
	
	
	
	/**
	* Initialise function
	*/
	public function initialise() {
		
		
		
		$this->view->header(); //show script header
		
		//echo $this->slash->sl_config("global_keywords");
		//echo $this->test;
	}
	
	/**
	* Load function
	*/
	public function load() {
		
		$this->view->start_main_menu();
		$this->menus->load_menu(0);
		$this->view->end_main_menu();
		
	}
	
	
	
	
	/**
	* Execute function
	*/
	public function execute() {
		$this->view->execute_menu();
	}

	

}



?>
