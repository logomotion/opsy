<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima module languages
* @version		fr.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

// Titres
define("NETICONTENTS_TL-TITLE", "Gestion du contenu de votre site");
define("NETICONTENTS_TL-ADDRUBTITLE", "Ajouter une rubrique");
define("NETICONTENTS_TL-EDITRUBTITLE", "Modifier une rubrique");
define("NETICONTENTS_TL-MOVERUBTITLE", "D&eacute;placer une rubrique");
define("NETICONTENTS_TL-ADDELMTITLE-TXT", "Ajouter un paragraphe texte");
define("NETICONTENTS_TL-EDITELMTITLE-TXT", "Modifier un paragraphe texte");
define("NETICONTENTS_TL-ADDELMTITLE-IMG", "Ajouter une image");
define("NETICONTENTS_TL-EDITELMTITLE-IMG", "Modifier une image");
define("NETICONTENTS_TL-ADDELMTITLE-IMGTXT", "Ajouter une image et un texte");
define("NETICONTENTS_TL-EDITELMTITLE-IMGTXT", "Modifier une image et un texte");
define("NETICONTENTS_TL-ADDELMTITLE-FLASH", "Ajouter une animation Flash");
define("NETICONTENTS_TL-EDITELMTITLE-FLASH", "Modifier une animation Flash");
define("NETICONTENTS_TL-ADDELMTITLE-FILE", "Ajouter un fichier");
define("NETICONTENTS_TL-EDITELMTITLE-FILE", "Modifier un fichier");
define("NETICONTENTS_TL-ADDELMTITLE-BR", "Ajouter un saut de ligne");
define("NETICONTENTS_TL-EDITELMTITLE-BR", "Modifier un saut de ligne");

define("NETICONTENTS_TL-RUBSITETITLE", " : : Rubriques de votre site");
define("NETICONTENTS_TL-RUBTITLE", " : : Titre de la rubrique ");
define("NETICONTENTS_TL-RUBACTIONS", " : : Actions sur les rubriques");
define("NETICONTENTS_TL-RUBPASSWORD", " : : Prot&eacute;ger cette rubrique");
define("NETICONTENTS_TL-RUBHIDE", " : : Masquer cette rubrique dans le menu");
define("NETICONTENTS_TL-IS_RESIDENCE", " : : Declarer cette rubrique comme residence");
define("NETICONTENTS_TL-RUBLINK", " : : Lien externe");
define("NETICONTENTS_TL-RUBMODULE", " : : Module externe");
define("NETICONTENTS_TL-RUBPARENT", " : : Rubrique parente");

define("NETICONTENTS_TL-ELMTITLE-TXT", " : : Texte ");
define("NETICONTENTS_TL-ELMTITLE-IMG", " : : Image ");
define("NETICONTENTS_TL-ELMTITLE-IMGTXT", " : : Image + Texte ");
define("NETICONTENTS_TL-ELMTITLE-FILE", " : : Fichier ");
define("NETICONTENTS_TL-ELMTITLE-FLASH", " : : Flash ");

define("NETICONTENTS_TL-ELMTITLE-OPTIONS", " : : Options / Param&egrave;tres ");
define("NETICONTENTS_TL-ELMTITLE-DIAPO", " : : Diaporama ");

define("NETICONTENTS_TL-ELMTITLE-IMGACTION", "Action sur l'image");
define("NETICONTENTS_TL-BORDER", "Cadre");
define("NETICONTENTS_TL-POSITION", "Position");
define("NETICONTENTS_TL-FILETITLE", "Intitul&eacute; de votre fichier");
define("NETICONTENTS_TL-FLASHTITLE", "Intitul&eacute; de votre animation");
define("NETICONTENTS_TL-FLASHWIDTH", "Largeur");
define("NETICONTENTS_TL-FLASHHEIGHT", "Hauteur");


define("NETICONTENTS_TL-ADDELEMENT", " : : Ajouter un &eacute;l&eacute;ment &agrave; la rubrique");
define("NETICONTENTS_TL-PREVIEW", " : : Aper&ccedil;u de la page");

define("NETICONTENTS_TL-INRUB", "dans : ");
define("NETICONTENTS_TL-INROOT", "&agrave; la racine de votre site");
define("NETICONTENTS_TL-ROOT", "Racine de votre site");



// Boutons
define("NETICONTENTS_BT-ROOT", "Votre site");
define("NETICONTENTS_BT-ADDRUB", "Ajouter une nouvelle rubrique");
define("NETICONTENTS_BT-EDITRUB", "Modifier la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-USERRUB", "Gestion des droits sur la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-MOVERUB", "D&eacute;placer la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-UPRUB", "Monter la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-DOWNRUB", "Descendre la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-DELRUB", "Supprimer la rubrique s&eacute;lectionn&eacute;e");
define("NETICONTENTS_BT-TXT", "Texte");
define("NETICONTENTS_BT-IMG", "Image");
define("NETICONTENTS_BT-IMGTXT", "Texte + Image");
define("NETICONTENTS_BT-FILE", "Fichier");
define("NETICONTENTS_BT-FLASH", "Flash");
define("NETICONTENTS_BT-BR", "Saut de ligne");
define("NETICONTENTS_BT-ELMEDIT", "Editer cet &eacute;l&eacute;ment");
define("NETICONTENTS_BT-ELMUP", "Monter cet &eacute;l&eacute;ment");
define("NETICONTENTS_BT-ELMDOWN", "Descendre cet &eacute;l&eacute;ment");
define("NETICONTENTS_BT-ELMDEL", "Supprimer cet &eacute;l&eacute;ment");

define("NETICONTENTS_BT-ACTION-NONE", "Aucune");
define("NETICONTENTS_BT-ACTION-GOIMG", "Agrandir l'image");
define("NETICONTENTS_BT-ACTION-GOURL", "Lancer l'url suivante");
define("NETICONTENTS_BT-ACTION-GORUB", "Rediriger vers la page suivante");

// Messages
define("NETICONTENTS_MSG-SAVE", "Enregistrer les modifications");
define("NETICONTENTS_MSG-BACK", "Retourner &agrave; la page principale");
define("NETICONTENTS_MSG-SELECTLANG", "Afficher la version en");
define("NETICONTENTS_MSG-ADDTXT", "Ajouter un texte");
define("NETICONTENTS_MSG-ADDIMG", "Ajouter une image");
define("NETICONTENTS_MSG-ADDIMGTXT", "Ajouter un texte et une image");
define("NETICONTENTS_MSG-ADDFILE", "Ajouter un fichier");
define("NETICONTENTS_MSG-ADDFLASH", "Ajouter une animation Flash");
define("NETICONTENTS_MSG-ADDBR", "Ajouter un saut de ligne");
define("NETICONTENTS_MSG-RUBSELECTPARENT", "S&eacute;l&eacute;ctionnez la rubrique parente");

// Textes
define("NETICONTENTS_TXT-RUBPASSWORD", "Si un mot de passe est renseign&eacute;, il sera demand&eacute; lorsque les visiteurs acc&eacute;deront &agrave; cette rubrique.<br/>
			Laisser vide pour ne pas prot&eacute;ger la rubrique par un mot de passe");
define("NETICONTENTS_TXT-RUBHIDE", "Oui, je d&eacute;sire masquer cette rubrique pour qu'elle ne soit pas accessible depuis le menu du site");
define("NETICONTENTS_TXT-RUBFCTHIDE", "Cette fonctionnalit&eacute; peut servire &agrave; cr&eacute;er des pages qui ne feront pas partie de l'arborescence du site<br/>
		mais qui seront appel&eacute;es par l'interm&eacute;diaire de liens sur les pages du site.");
define("NETICONTENTS_TXT-RUBLINK", "Vous avez la possibilit&eacute; d'utiliser cette rubrique pour acc&eacute;der &agrave; un autre site. <br/>
					Pour cela, indiquez l'adresse du site qui s'ouvrira lorsqu'on cliquera sur cette rubrique.");	
define("NETICONTENTS_TXT-IS_RESIDENCE", "Cette Rubrique est une residence");
define("NETICONTENTS_TXT-IS_RESIDENCE_CF", "cette fonctionnalité permet de rendre la residence disponible depuis la recherche de biens;");
define("NETICONTENTS_TXT-RUBMODULE", "Vous avez la possibilit&eacute; de li&eacute; un module &agrave; cette rubrique <br/>
					Pour cela, indiquez l'adresse du module.");	
define("NETICONTENTS_TXT-RUBNEEDSEL", "S&eacute;lectionnez une rubrique.");
define("NETICONTENTS_TXT-RUBEMPTY", "Cette rubrique est vide.");
define("NETICONTENTS_TXT-RUBCONFIRMDEL", "Voulez vous vraiment supprimer cette rubrique et tout ce qu\'elle contient ?");


define("NETICONTENTS_TXT-ELMIMGPOSITION", "Comment d&eacute;sirez-vous justifier votre image ?");
define("NETICONTENTS_TXT-ELMIMGBORDER", "Quelle cadre d&eacute;sirez-vous utiliser pour cette image ?");
define("NETICONTENTS_TXT-ELMIMGACTION", "Quelle action d&eacute;sirez-vous ajouter lorsque l'on clique sur l'image ?");
define("NETICONTENTS_TXT-ELMCONFIRMDEL", "Voulez vous vraiment supprimer ce paragraphe ?");

define("NETICONTENTS_TXT-RUBADD-OK", "La rubrique a &eacute;t&eacute; enregistr&eacute;e.");
define("NETICONTENTS_TXT-RUBADD-FAIL", "Erreur lors de l'ajout de la rubrique.");
define("NETICONTENTS_TXT-RUBEDIT-OK", "La rubrique a &eacute;t&eacute; enregistr&eacute;e.");
define("NETICONTENTS_TXT-RUBEDIT-FAIL", "Erreur lors de la modification de la rubrique.");
define("NETICONTENTS_TXT-RUBMOVE-OK", "La rubrique a &eacute;t&eacute; d&eacute;plac&eacute;e.");
define("NETICONTENTS_TXT-RUBMOVE-FAIL", "Erreur lors du d&eacute;placement de la rubrique.");

define("NETICONTENTS_TXT-ELMMOVE-OK", "L'&eacute;l&eacute;ment a &eacute;t&eacute; d&eacute;plac&eacute;.");
define("NETICONTENTS_TXT-ELMMOVE-FAIL", "Erreur lors du d&eacute;placement de l'&eacute;l&eacute;ment.");
define("NETICONTENTS_TXT-ELMDEL-OK", "L'&eacute;l&eacute;ment a &eacute;t&eacute; supprim&eacute;.");
define("NETICONTENTS_TXT-ELMDEL-FAIL", "Erreur lors de la suppression de l'&eacute;l&eacute;ment.");




//ERRORS
define("NETICONTENTS_ERR-PAGEERRORS", "Il y a des erreurs sur la page.");
define("NETICONTENTS_ERR-FIELDEMPTY", "Ce champs est vide !");
define("NETICONTENTS_ERR-NEEDIMAGE", "Vous devez envoyer au moins une image.");

?>
