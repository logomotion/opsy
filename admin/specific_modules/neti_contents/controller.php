 <?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_contents
* @internal     Admin netissima module
* @version		neti_contents.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/
 
 // Include Configuration
Include ("config/neti_config.php");
 
// Include Default Views
include ("views/default/neti_view.php");
include ("views/default/neti_core_view.php");
include ("views/default/neti_rub_view.php");
include ("views/default/neti_elm_view.php");

// Include models
include ("models/neti_core.php");
include ("models/neti_rub.php");
include ("models/neti_elm.php");

class neti_contents_controller{

	public $slash; //Core Reference
	public $params;
	public $module_name = "neti_contents";
	public $module_id;
	
	public $mode;
	public $message;
	public $errors;
	public $datas;
	public $lg;
	public $idr;
	
	public $active_lg;
	
	//Config
	public $neti_config;
	
	// Models
	public $neti_core;
	public $neti_rub;
	public $neti_elm;
	
	// Views
	public $neti_view;
	public $neti_core_view;
	public $neti_rub_view;
	public $neti_elm_view;

	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
		
		
		$this->slash = $core_class_ref;
		$this->module_id = $module_id;
		$this->lg = $this->slash->get_active_lang();
		
		//Config
		$this->neti_config = new neti_config($this); // On d�clare l'objet pour l'utilisation des fct de paragraphes
		
		//Models
		$this->neti_core = new neti_core($this); // On d�clare l'objet pour l'utilisation des fct propres � NETIssima
		$this->neti_rub = new neti_rub($this); // On d�clare l'objet pour l'utilisation des fct de rubriquage
		$this->neti_elm = new neti_elm($this); // On d�clare l'objet pour l'utilisation des fct de paragraphes
		
		//Views
		$this->neti_view = new neti_view($this); // Vue Principale
		$this->neti_core_view = new neti_core_view($this); // Vue Netissima
		$this->neti_rub_view = new neti_rub_view($this); // Vue Rubriques
		$this->neti_elm_view = new neti_elm_view($this); // Vue Elements
		
		$this->load_params(); //Load params
	}

	/**
	* Initialise function 
	* Require function by slash core
	*/
	public function initialise() {
		//no global initialisation for this module
	}
	
	/**
	 * Load header function
	 */
	public function load_header(){
		$this->neti_view->header(); //Global header
		
		switch ($this->mode) {
			case "show":
				$this->neti_view->l_header(); //List header
			break;
			case "add_rub":
			case "add_elm":
			case "edit_rub":
			case "edit_elm":
				$this->neti_view->f_header(); //Form header
			break;
			
			/*case "add":
			case "edit":
				$this->f_header(); //Form header
			break;
			case "delete":
				//nothing
				*/
			break;
			default:
				//nothing
		}
	}
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		$this->neti_view->footer(); //Global footer
		
		switch ($this->mode) {
			case "show":
				$this->neti_view->l_footer(); //List footer
			break;
			case "add_rub":
			case "add_elm":
			case "edit_rub":
			case "edit_elm":	
				$this->neti_view->f_footer(); //Form footer
			break;
			
			break;
			default:
				//nothing
		}
	}
	
	/**
	* Load module function
	* Require function by slash core
	*/
	public function load() {
		switch ($this->mode) {
			
			/* ------ ACTIONS RUBRIQUE ------ */
			case "add_rub": //Forumulaire ajout rubrique
				$this->neti_rub_view->show_form();
				break;
			case "edit_rub": //Forumulaire ajout rubrique
				$this->neti_rub_view->show_form($this->datas["id"],$this->datas,$this->errors);
				break;
			case "move_rub": //Forumulaire ajout rubrique
				$this->neti_rub_view->show_move_form($this->datas["id"],$this->datas,$this->errors);
				break;
			
			
			/* ------ ACTIONS ELEMENTS ------ */
			case "add_elm": //Ajout d'un �l�ment
				$this->neti_elm_view->show_form(0,$this->datas,$this->errors);
			break;
			
			case "edit_elm": //Edition d'un �l�ment
				$this->neti_elm_view->show_form($this->datas["id"],$this->datas,$this->errors);
				break;
			
			case "show": //Page principale
				$this->neti_view->show_main($this->message);
				break;
			default: //Page principale
				$this->neti_view->show_main($this->message);
		}
	}
	
	
	/**
	* Execute function
	* Require function by slash core
	*/
	public function execute() {
		// no constant execute
	}
	
	
	/**
	 * Charge les param�tres
	 */
	private function load_params() {
		
		//Enregistrement rubrique en cours
		if ($this->slash->sl_param("idr")) { $_SESSION["neti_idr"] = $this->slash->sl_param("idr"); }
		if (!isset($_SESSION["neti_idr"])) { $_SESSION["neti_idr"] = "root"; }
		$this->idr = $_SESSION["neti_idr"];
		
		
		
		//Enregistrement langue en cours
		if ($this->slash->sl_param("lg")) { $_SESSION["neti_lg"] = $this->slash->sl_param("lg"); }
		if (!isset($_SESSION["neti_lg"])) { $_SESSION["neti_lg"] = $this->lg[0]['id']; }
		$this->active_lg = $_SESSION["neti_lg"];
		
		switch ($this->slash->sl_param($this->module_name."_act")) {
			
			
			/* ------ ACTIONS RUBRIQUE ------ */
			case "add_rub": //Ajout d'une rubrique
				$this->mode = "add_rub";
			break;

			case "edit_rub": //Edition d'une rubrique

				if ($this->idr) {
					$this->datas = $this->neti_rub->load($this->idr);
					$this->mode = "edit_rub";
				}else{
					$this->mode = "show";
					$this->message = $this->slash->trad_word("SELECTION_REQUIRE");
				}
			break;
			
			case "move_rub": //D�placement d'une rubrique

				if ($this->idr) {
					$this->datas = $this->neti_rub->load($this->idr);
					$this->mode = "move_rub";
				}else{
					$this->mode = "show";
					$this->message = $this->slash->trad_word("SELECTION_REQUIRE");
				}
			break;
			
			case "save_rub": //Enregistrement de la rubrique
			
				$this->datas = $this->neti_rub->recovery_fields();
				$this->errors = $this->neti_rub->check_fields($this->datas);
				
				if ($this->errors != null) {
					$this->message = $this->slash->trad_word("NETICONTENTS_ERR-PAGEERRORS");
					$this->mode = "edit_rub";
				}else{
					$this->message = $this->neti_rub->save($this->datas["id"],$this->datas);
					$this->mode = "show";
				}
			
			break;
			
			case "save_rub_position": //Enregistrement de la nouvelle position de la rubrique 
			
				$this->datas = $this->neti_rub->recovery_move_fields();
				$this->errors = $this->neti_rub->check_move_fields($this->datas);
				
				if ($this->errors != null) {
					$this->message = $this->slash->trad_word("NETICONTENTS_ERR-PAGEERRORS");
					$this->mode = "move_rub";
				}else{
					$this->message = $this->neti_rub->set_position($this->datas["id"],array("mode" => "set", "values" => $this->datas));
					$this->mode = "show";
				}
			
			break;
			
			case "up_rub": //Monte la rubrique en cours
				$this->message = $this->neti_rub->set_position($this->idr,array("mode" => "up"));
				$this->mode = "show";
			break;
			
			case "down_rub": //Descendre la rubrique en cours
				$this->message = $this->neti_rub->set_position($this->idr,array("mode" => "down"));
				$this->mode = "show";
			break;
			
			case "del_rub": //Suppression de la rubrique en cours + sous rubriques
				$this->message = $this->neti_rub->delete($this->idr);
				$this->mode = "show";
			break;
			
			
			
			/* ------ ACTIONS ELEMENTS ------ */
			case "add_elm_txt": //Ajout d'un element texte
				$this->datas["type"] = 1;
				$this->mode = "add_elm";
			break;
			case "add_elm_img": //Ajout d'un element image
				$this->datas["type"] = 2;
				$this->mode = "add_elm";
			break;
			case "add_elm_imgtxt": //Ajout d'un element image + texte
				$this->datas["type"] = 3;
				$this->mode = "add_elm";
			break;
			case "add_elm_file": //Ajout d'un element file
				$this->datas["type"] = 4;
				$this->mode = "add_elm";
			break;
			case "add_elm_flash": //Ajout d'un element flash
				$this->datas["type"] = 5;
				$this->mode = "add_elm";
			break;
			case "add_elm_br": //Ajout d'un element saut de ligne
				$this->datas["type"] = 6;
				$this->message = $this->neti_elm->save(0,$this->datas,$this->datas["type"]);
				$this->mode = "show";
			break;
			
			case "edit_elm": //Edition d'un �l�ment

				$ide = $this->slash->sl_param("ide","GET");
				
				if (isset($ide)) {
					$this->datas = $this->neti_elm->load_element($ide);
					$this->mode = "edit_elm";
				}else{
					$this->mode = "show";
					$this->message = $this->slash->trad_word("SELECTION_REQUIRE");
				}
				
			break;
			
			case "save_elm": //Enregistrement d'un element
				
				$type = $this->slash->sl_param("type","GET");
				
				if (isset($type)){
				
					$this->datas = $this->neti_elm->recovery_fields($type);
					$this->errors = $this->neti_elm->check_fields($this->datas,$type);
					
					if ($this->errors != null) {
						$this->message = $this->slash->trad_word("NETICONTENTS_ERR-PAGEERRORS");
						$this->datas["type"] = $type;
						$this->mode = "edit_elm";
					}else{
						$this->message = $this->neti_elm->save($this->datas["id"],$this->datas,$type);
						$this->mode = "show";
					}
			
				}
				
			break;
			
			case "up_elm": //Monte l'�lement en cours
				$this->message = $this->neti_elm->set_position($this->slash->sl_param("ide","GET"),"up");
				$this->mode = "show";
			break;
			
			case "down_elm": //Descendre l'�lement en cours
				$this->message = $this->neti_elm->set_position($this->slash->sl_param("ide","GET"),"down");
				$this->mode = "show";
			break;
			case "del_elm": //Suppression de la rubrique en cours + sous rubriques
				
				$ide = $this->slash->sl_param("ide","GET");
				
				if (isset($ide)) {
					$this->message = $this->neti_elm->delete($ide);
					//$this->datas = $this->neti_elm->delete($ide);
				}
				
				$this->mode = "show";
				
			break;
			
			
			
			
			default: // default view
				$this->mode = "show";
		}
	
	}
	


}



?>
