<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_config
* @internal     Admin netissima module
* @version		neti_config.php - Version 11.3.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/
 
//@TODO : Charger la configuration directement depuis une table neti_config

class neti_config{
	
	
	
	// configuration NETIssima.
	
	public $dragndrop = true; //Active ou non le drag and drop des �l�ments
	public $image_max_width = 923; //Longueur maximum des images
	public $image_max_height = 1024; //Largeur maximum des images
	
	//Extensions autoris�es
	public $authorized_files_extensions = "odt|doc|rtf|txt|docx|ods|xls|xlsx|odp|ppt|pptx|pps|pdf|zip|rar|mp3|mp4|avi|mpg";
	public $authorized_images_extensions = "gif|jpg|png";
	
	public $authorized_nb_rub = 999; //Maximum de rubrique autoris�es
	
	
	public $slash; //Core Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
	}
	
	
}

?>