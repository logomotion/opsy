<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_contents_view
* @internal     Admin netissima module
* @version		neti_contents_view.php - Version 11.2.18
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_view {

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}

	/**
	 * Show global HTML Header
	 */
	public function header () {
	
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/interface/js/interface.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
		
		//Fonctions JavaScript NETIssima
		$this->controller->neti_core_view->fct_js();
		
		//Styles NETIssima
		echo "<link rel='stylesheet' type='text/css' media='screen' href='specific_modules/neti_contents/views/default/css/styles.css' /> \n"; 
		echo "<link rel='stylesheet' type='text/css' media='screen' href='specific_modules/neti_contents/views/default/css/assets.css' /> \n"; 
	} 
	  
	public function l_header () {
		
		//Jquery inc.
		
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.timers.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.easing.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery/jquery.cookie.js'></script> \n";
		
		//Galleryview
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/galleryview/js/galleryview.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' media='screen' href='../core/plugins/jquery_plugins/galleryview/css/galleryview.css' /> \n";
		
		//Simple tree
		/*echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/simpletree/js/persistence.simpletree.js'></script> \n";
		//echo "<link rel='stylesheet' type='text/css' media='screen' href='../core/plugins/jquery_plugins/simpletree/css/simpletree.css'> \n";*/
		
		//Dynatree
		
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/ui/js/ui.custom.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/dynatree/js/dynatree.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' media='screen' href='../core/plugins/jquery_plugins/dynatree/skins/skin/ui.dynatree.css' /> \n";
		
		
	}
	
	public function f_header () {
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/ajaxupload/js/ajaxupload.js'></script> \n";
		echo "<link rel='stylesheet' type='text/css' href='../core/plugins/jquery_plugins/tabs/css/tabs.css' media='screen'>";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/ui/js/ui.core.js'></script> \n";
				
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/tabs/js/tabs.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/tiny_mce/jquery.tinymce.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/tiny_mce/plugins/tinybrowser/tb_tinymce.js.php'></script> \n";
		
	}
	
	/**
	 * Affiche la page principale de la gestion de contenu NETIssima
	 * @param $message message
	 */
	public function show_main($message="") {
		
		
		
		echo "<div class='neti-container'>";
		
		$this->controller->neti_core_view->create_header();
		
		echo "<div class='neti-content'>
				<div class='neti-left'>
					<div class='neti-box-top'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBSITETITLE")."</span></div>";
					$this->controller->neti_core->make_menu();
		echo "		<br/>
					<div class='neti-box-top'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBACTIONS")."</span></div>
					<div class='neti-box neti-actions'>";
					$this->controller->neti_core_view->make_actions();
		echo "		</div>";
		echo "		</div>
				<div class='neti-right'>
					<div class='neti-box-top'><span>".$this->slash->trad_word("NETICONTENTS_TL-ADDELEMENT")."</span></div>
					<div class='neti-box'>";
					$this->controller->neti_core_view->make_tools();
		echo "		</div>
					<br/>
					<div class='neti-box-top'><span>".$this->slash->trad_word("NETICONTENTS_TL-PREVIEW")."</span>";
					$this->controller->neti_core_view->create_lang_tabs_preview($this->controller->active_lg);			
		echo "
					</div>
					<div class='neti-box'>";
		
		echo "	<div id='neti-tabs-container' class='neti-page'>";
								
				for($i=0;$i<count($this->controller->lg);$i++){
					if ($this->controller->active_lg==$this->controller->lg[$i]["id"]) { 
						echo "<div id='tab_".$this->controller->lg[$i]["id"]."'>";
						$this->controller->neti_core->make_preview($this->controller->idr,$this->controller->lg[$i]["id"]);
						echo "</div>";
					}
				}
				
		echo "</div>";	
				
		echo "		</div>
				</div>
			</div>
		</div>";
	}

	
	
	
	/**
	 * HTML footer
	 */
	public function footer() {
		echo "<script type='text/javascript'>
			$(document).ready(function(){ 
				$('#message').css('opacity',1);
				$('#message').stopAll().pause(5000).fadeTo(400,0, function() { $(this).hide(); } );
				$.preloadCssImages();	

		}); 			
		</script>";
	}
	
	/**
	 * HTML footer
	 */
	public function l_footer() {
		echo "
		<script type='text/javascript'> 
		
		var simpleTreeCollection;
		
			$(document).ready(function(){ 
				/*
				simpleTreeCollection = $('.simpleTree').simpleTree({
				
					autoclose: false, 
					animate:true, 
					drag:false, 
					
					
					afterClick:function(node){
						//alert(node.attr('id'));
						$(location).attr('href','index.php?mod=neti_contents&idr='+node.attr('id'));
					},
					
					afterMove:function(destination, source, pos){
						//alert('test');
						alert(destination + ' / ' + source + ' / ' + pos);
					},
					afterAjax:function() {
						//alert('Loaded');
					}
					
					
				});
				*/
				
				
				
				//DYNATREE
				
				$('#tree').dynatree({
			      checkbox: false,
			      selectMode: 3,
			      persist: true,
				  imagePath: 'specific_modules/neti_contents/views/default/images/menu/',
				  fx: { height: 'toggle', duration: 100 },
				  onActivate: function(node) {
					if( node.data.url )
					  $(location).attr('href',node.data.url);
				  },
				  dnd: {
					  autoExpandMS: 1000,
					  preventVoidMoves: true, 
					  onDragStart: function(node) {
        
					  },
					  onDragStop: function(node) {
					   
					  },
					  onDragEnter: function(node, sourceNode) {
						
					  },
					  onDragOver: function(node, sourceNode, hitMode) {
						
					  },
					  onDrop: function(node, sourceNode, hitMode, ui, draggable) {
						alert('drop');
					  },
					  onDragLeave: function(node, sourceNode) {
						
					  }
					}
				});
				
				
				
				
			";
		
		if ($this->controller->neti_config->dragndrop){
			for($i=0;$i<count($this->controller->lg);$i++){
				if ($this->controller->active_lg == $this->controller->lg[$i]["id"]){
				
					echo "
						$('#neti-page_".$this->controller->lg[$i]["id"]."').Sortable( {
		            		accept : 'neti-elm-li',
							axis: 'vertically',
							opacity: 0.6,
							onchange : function (sorted) {
							serial = $.SortSerialize('neti-page_".$this->controller->lg[$i]["id"]."'); 
							// requ�te Ajax pour l'enregistrement des positions
							$.ajax({
								url: 'specific_modules/neti_contents/neti_ajax.php?action=set_elm_position&lg=".$this->controller->lg[$i]["id"]."',
								type: 'post',
								data: serial.hash,
				
								//complete: function(data){alert(data);},
								//success: function(feedback){ alert(feedback);},
								//error: function(){alert('Erreur lors du deplacement');}
				
								});
							}
		        		});	
		        		
			        ";
				}
			}
		}
		
		echo "
		
		});
		</script>";

	}
	
	/**
	 * HTML footer
	 */
	public function f_footer() {
		//Nothing
	}
	

	/**
	 * 
	 * NETI Tiny Mce.
	 * @param $module_name
	 * @param $field_id
	 * @param $options
	 */
	public function tinymce($module_name,$field_id,$options=null) {
	
		if (!isset($options["value"])) {$options["value"]= "";} 
		if (!isset($options["style"])) {$options["style"]= "";} 
		if (isset($options["css"])) { $css = $options["css"]; } else { $css = ""; }
		if (isset($options["script_url"])) { $script_url = $options["script_url"]; } else { $script_url = "../core/plugins/tiny_mce/tiny_mce.js"; }
		if (isset($options["theme"])) { $theme = $options["theme"]; } else { $theme = "advanced"; }
		
		echo "<textarea id='".$module_name."_obj".$field_id."' name='".$module_name."_obj".$field_id."' style='".$options["style"]."'>".$options["value"]."</textarea>";
				

		echo '<script type="text/javascript">
					$(document).ready(function(){ 
						$("#'.$module_name.'_obj'.$field_id.'").tinymce({
							// Location of TinyMCE script
							script_url : "'.$script_url.'",
							
							// General options
							mode : "textareas",
							theme : "'.$theme.'",
							language : "fr",
							//forced_root_block : false,
							//force_br_newlines : true,
							//force_p_newlines : false,
							relative_urls : true,
							//file_browser_callback : "tinyBrowser",
							skin : "o2k7",
							plugins : "safari,pagebreak,style,layer,table,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups",

							// Theme options
							theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,styleselect,formatselect,fontselect,fontsizeselect",
							theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,|,insertdate,inserttime,preview,|,forecolor,backcolor",
							theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,emotions,iespell,media,advhr,|,print,|,ltr,rtl,|,fullscreen",
							
							theme_advanced_toolbar_location : "top",
							theme_advanced_toolbar_align : "left",
							theme_advanced_statusbar_location : "bottom",
							theme_advanced_resizing : true,

							// Example word content CSS (should be your site CSS) this one removes paragraph margins
							content_css : "'.$css.'",

							// Drop lists for link/image/media/template dialogs
							//template_external_list_url : "lists/template_list.js",
							//external_link_list_url : "lists/link_list.js",
							//external_image_list_url : "lists/image_list.js",
							//media_external_list_url : "lists/media_list.js",

							// Replace values for the template plugin
							//template_replace_values : {
							//	username : "Some User",
							//	staffid : "991234"
							//}			
						});
						
						
					});
				</script>';

	}
	
	
}





?>