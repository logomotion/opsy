<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @subpackage	neti_rub_view
* @internal     Admin netissima module
* @version		neti_rub_view.php - Version 11.3.17
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

class neti_rub_view {

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	 * Formulaire d'ajout / édition d'une rubrique
	 */
	public function show_form ($id=0, $values=null, $error=null) {
		
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_rub");
		
		//Gestion du titre
		$title = ""; $utitle="";
		if ($id != 0) { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-EDITRUBTITLE");
		} else { 
			$title = $this->slash->trad_word("NETICONTENTS_TL-ADDRUBTITLE");
			if ($this->controller->idr=='root') {
				$utitle = $this->slash->trad_word("NETICONTENTS_TL-INROOT");
			}else{
				$utitle = $this->slash->trad_word("NETICONTENTS_TL-INRUB").$this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
			}
		}
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBTITLE")."</span>";
		$this->controller->neti_core_view->create_lang_tabs();
		echo "</div>
			<div class='neti-box neti-box-dim600'>";
		echo "	<div id='neti-tabs-container'>";
		
		for($i=0;$i<count($this->controller->lg);$i++){
			
			if($this->controller->lg[$i]["id"] == $this->controller->active_lg){ $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			//if ($i==0) { $style = "style='display:block;'"; } else { $style = "style='display:none;'"; }
			if (!isset($values["title-".$this->controller->lg[$i]["id"]])) { $values["title-".$this->controller->lg[$i]["id"]]=""; }
			echo "<div id='tab_".$this->controller->lg[$i]["id"]."' ".$style.">";
			if ($error["title-".$this->controller->lg[$i]["id"]]["message"]) { sl_form::error($error["title-".$this->controller->lg[$i]["id"]]["message"]); echo "<br/>";   }
			sl_form::title($this->slash->trad_word("TITLE")." ".$this->controller->lg[$i]["name"]." : ");
			sl_form::br(1);
			sl_form::input($mn,"title-".$this->controller->lg[$i]["id"],array("value" => $values["title-".$this->controller->lg[$i]["id"]]));
			
			echo "</div>";
		}
		
		echo "	</div>
			</div>";

					//La rubrique est une residence
		echo "<br/>
			<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-IS_RESIDENCE")."</span></div>
			<div class='neti-box neti-box-dim600'>";
		
		if (!isset($values["is_residence"])) { $values["is_residence"]=0; }
		sl_form::checkbox($mn,6,array("value" => $values["is_residence"]));			
		sl_form::title($this->slash->trad_word("NETICONTENTS_TXT-IS_RESIDENCE"));
		if ($error[6]["message"]) { sl_form::error($error[6]["message"]); }					
		echo "</br></br>Types disponibles :</br>";
		if (!isset($values["t1"])) { $values["t1"]=0; }
		sl_form::checkbox($mn,7,array("value" => $values["t1"]));			
		sl_form::title("T1");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["t2"])) { $values["t2"]=0; }
		sl_form::checkbox($mn,8,array("value" => $values["t2"]));			
		sl_form::title("T2");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["t3"])) { $values["t3"]=0; }
		sl_form::checkbox($mn,9,array("value" => $values["t3"]));			
		sl_form::title("T3");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["t4"])) { $values["t4"]=0; }
		sl_form::checkbox($mn,10,array("value" => $values["t4"]));			
		sl_form::title("T4");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["t5"])) { $values["t5"]=0; }
		sl_form::checkbox($mn,11,array("value" => $values["t5"]));			
		sl_form::title("T5");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["t6"])) { $values["t6"]=0; }
		sl_form::checkbox($mn,12,array("value" => $values["t6"]));			
		sl_form::title("T6");
		echo "&nbsp;&nbsp;&nbsp;";
		if (!isset($values["duplex"])) { $values["duplex"]=0; }
		sl_form::checkbox($mn,13,array("value" => $values["duplex"]));			
		sl_form::title("Duplex");
		echo "</br></br>Commune :</br>";
		if (!isset($values["commune"])) { $values["commune"]=""; }
		sl_form::input($mn,14,array("value" => $values["commune"],"size" => "20"));
		


		echo "<br/><br/>".$this->slash->trad_word("NETICONTENTS_TXT-IS_RESIDENCE_CF")."
		</div>";
		
		//Mot de passe de la rubrique 
		echo "<br/>
			<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBPASSWORD")."</span></div>
			<div class='neti-box neti-box-dim600'>";			
		
		if (!isset($values["password"])) { $values["password"]=""; }
		sl_form::title($this->slash->trad_word("PASSWORD")." : ");
		sl_form::input($mn,2,array("value" => $values["password"],"size" => "20"));
		if ($error[2]["message"]) { sl_form::error($error[2]["message"]); }					
		echo "<br/><br/>".$this->slash->trad_word("NETICONTENTS_TXT-RUBPASSWORD")."
			</div>";
		
		//Masquer la rubrique
		echo "<br/>
			<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBHIDE")."</span></div>
			<div class='neti-box neti-box-dim600'>";
		
		if (!isset($values["hidden"])) { $values["hidden"]=0; }
		sl_form::checkbox($mn,3,array("value" => $values["hidden"]));			
		sl_form::title($this->slash->trad_word("NETICONTENTS_TXT-RUBHIDE"));
		if ($error[3]["message"]) { sl_form::error($error[3]["message"]); }					
		echo "<br/><br/>".$this->slash->trad_word("NETICONTENTS_TXT-RUBFCTHIDE")."
		</div>";

	
		//Liens
		echo "<br/>
			<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBLINK")."</span></div>
			<div class='neti-box neti-box-dim600'>".$this->slash->trad_word("NETICONTENTS_TXT-RUBLINK")."<br/><br/>";
		
		if (!isset($values["url"])) { $values["url"]=""; }
		sl_form::title($this->slash->trad_word("URL")." : ");
		sl_form::input($mn,4,array("value" => $values["url"]));
		if ($error[4]["message"]) { sl_form::error($error[4]["message"]); }					
		echo "</div>";
		
		//Module externe
		echo "<br/>
			<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBMODULE")."</span></div>
			<div class='neti-box neti-box-dim600'>".$this->slash->trad_word("NETICONTENTS_TXT-RUBMODULE")."<br/><br/>";
		
		if (!isset($values["module"])) { $values["module"]=""; }
		sl_form::title($this->slash->trad_word("URL")." : ");
		sl_form::input($mn,5,array("value" => $values["module"]));
		if ($error[5]["message"]) { sl_form::error($error[5]["message"]); }					
		echo "</div>";
	
		echo "</div>";
		
		sl_form::end();
	}
	
	
	
	
	/**
	 * Formulaire de déplacement rubrique
	 */
	public function show_move_form ($id=0, $values=null, $error=null) {
		
		
		$mn = $this->controller->module_name;
		sl_form::start($mn,$id,"save_rub_position");
		
		//Gestion du titre
		
		$title = $this->slash->trad_word("NETICONTENTS_TL-MOVERUBTITLE");
		$utitle = $this->controller->neti_rub->get_title($this->controller->idr,$this->controller->lg[0]['id']);
		
		
		//Haut de la page
		echo "<div class='neti-container' >";
		$this->controller->neti_core_view->create_header();
		echo "	<div class='neti-content' >";
		
		//Zone de controle
		echo "		<div class='neti-form-top' >";
		$this->controller->neti_core_view->create_title($title,$utitle);
		$this->controller->neti_core_view->create_buttons(array("save","back"));
		echo "		</div>";
		echo "<br/>";
		
		//Onglet Langue + Titres rubriques
		echo "<div class='neti-box-top neti-top-dim600'><span>".$this->slash->trad_word("NETICONTENTS_TL-RUBPARENT")."</span>";
		
		echo "</div>
			<div class='neti-box neti-box-dim600'>";
		echo "	<div id='neti-tabs-container'>";
		
		$rub = $this->controller->neti_rub->load_all($this->controller->lg[0]["id"]);
		$rub_id = array();
		$rub_text = array();
		
		$rub_id[0] = 0;
		$rub_text[0] = " - ".$this->slash->trad_word("NETICONTENTS_TL-ROOT")." - "; 
		
		for ($i=0; $i<count($rub); $i++) {
				if ($rub[$i]["id_rub"] != $this->controller->idr){
					$rub_id[] =  $rub[$i]["id_rub"];
					$rub_text[] = $rub[$i]["title"];
				}
		}
		
				
		sl_form::title($this->slash->trad_word("NETICONTENTS_MSG-RUBSELECTPARENT")." : ");
		sl_form::select($mn,1,array("value" => $values["id_top"], "values" => $rub_id, "texts" => $rub_text ));
		sl_form::br(2);
		
		echo "	</div>
			</div>";
		
		
	
		echo "</div>";
		
		sl_form::end();
	}

}

?>