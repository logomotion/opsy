<?php
/**
* @package		SLASH-CMS
* @subpackage	AJAX NETISSIMA
* @internal     fct ajax
* @version		neti-ajax.php - Version 11.3.21
* * @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

/**************************************************/
/******** 		CONFIGURATION		********/
/**************************************************/
session_start();
include ("../../../core/config/configuration.php");
$config = new SLConfig();
$host = $config->mysql_host;
$login = $config->mysql_user; 
$password = $config->mysql_password; 
$database_name = $config->mysql_database; 
$database = mysql_connect($host, $login, $password) or die ("CONNEXION ERROR");	
mysql_select_db($database_name, $database) or die ("DATABASE CONNEXION ERROR");


/* FONCTIONS */

/**
 * Déplacement d'un paragraphe
 */
function set_elm_position() {
	if (isset($_GET['lg'])) {
		
		$sortlist = $_POST['neti-page_'.$_GET['lg']];
		
		//echo 'neti-page_'.$_GET['lg']." : ".count($sortlist);
		
		for ($i = 0; $i < count($sortlist); $i++) {
			
			
			$query = ("UPDATE neti_elements SET position='".($i + 1)."' WHERE id=$sortlist[$i]");
			$res_ajout = mysql_query($query) or die(mysql_error());
		}
	
	}
}



/* ACTIONS */
$action = $_GET["action"];
if ($action) {
	switch($action){
		
		case "set_elm_position":
			set_elm_position();
		break;
		
		default:
		
	}
	
}

?>