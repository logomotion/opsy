<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin rub functions
* @version		rub.php - Version 11.3.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_rub{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
	}
	
	/**
	 * Récupération des informations d'une rubrique
	 * @param $id rub ID
	 */
	public function load($id) {
		$result = mysql_query("SELECT * FROM neti_rubpos, neti_rubtitles WHERE neti_rubpos.id='".$id."' AND neti_rubtitles.id_rub='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$vals = array();
			$vals["id"] = $id;
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				
				$vals["title-".$row["id_lg"]] = $row["title"];
				if ($i==0) {
					$vals["id_top"] = $row["id_top"];
					$vals["position"] = $row["position"];
					$vals["url"] = $row["url"];
					$vals["module"] = $row["module"];
					$vals["password"] = $row["password"];
					$vals["hidden"] = $row["hidden"];
					$vals["enabled"] = $row["enabled"];
					$vals["is_residence"] = $row["is_residence"];
					$vals["t1"] = $row["t1"];
					$vals["t2"] = $row["t2"];
					$vals["t3"] = $row["t3"];
					$vals["t4"] = $row["t4"];
					$vals["t5"] = $row["t5"];
					$vals["t6"] = $row["t6"];
					$vals["duplex"] = $row["duplex"];
					$vals["commune"] = $row["commune"];
				}
				$i++;
			}
			return $vals;
		}
	}
	
	
	/**
	 * Récupération des l'ensemble des rubriques
	 * @param $id rub ID
	 */
	public function load_all($lg) {
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$vals = array();
			$i = 0;	
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$vals[$i] = $row;
				$i++;
			}
			return $vals;
		}
	}
	
	
	/**
	 * Enregistrement / modification rubrique
	 */
	public function save($id,$values){
		
		if ($id != 0) { //Modification d'une rubrique
			mysql_query("UPDATE neti_rubpos set 
					url='".$values["url"]."',
					module='".$values["module"]."',
					password='".$values["password"]."',
					hidden='".$values["hidden"]."',
					enabled='".$values["enabled"]."', 
					is_residence='".$values["is_residence"]."',
					t1='".$values["t1"]."',
					t2='".$values["t2"]."',
					t3='".$values["t3"]."',
					t4='".$values["t4"]."',
					t5='".$values["t5"]."',
					t6='".$values["t6"]."',
					duplex='".$values["duplex"]."',
					commune='".$values["commune"]."'
					WHERE id='".$values["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			for($i=0;$i<count($this->controller->lg);$i++){
				
				$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				if (mysql_num_rows($result) > 0) {
					mysql_query("UPDATE neti_rubtitles set 
						title='".mysql_real_escape_string($values["title-".$this->controller->lg[$i]["id"]])."' 
						WHERE id_rub='".$values["id"]."' AND id_lg='".$this->controller->lg[$i]["id"]."'",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}else{
					if ($values["title-".$this->controller->lg[$i]["id"]]) {
						mysql_query("INSERT INTO neti_rubtitles 
							(id,id_rub,id_lg,title) value
							('','".$values["id"]."','".$this->controller->lg[$i]["id"]."','".$values["title-".$this->controller->lg[$i]["id"]]."')",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					}
				}
				
				
			}
			
			return $this->slash->trad_word("NETICONTENTS_TXT-RUBEDIT-OK");	
			
		} else { //Ajout d'une rubrique
			
			if ($this->controller->idr){
				
				$c_idr = $this->controller->idr;
				if ($this->controller->idr=="root"){ $c_idr = 0; } 
				
				$result = mysql_query("SELECT * FROM neti_rubpos WHERE id_top='".$c_idr."' ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$nb = mysql_num_rows($result);
				
				mysql_query("INSERT INTO neti_rubpos
					(id,id_top,position,url,module,password,hidden,enabled,is_residence,t1,t2,t3,t4,t5,t6,duplex,commune) value
					('','".$c_idr."','".($nb+1)."','".$values["url"]."','".$values["module"]."','".$values["password"]."','".$values["hidden"]."','".$values["enabled"]."','".$values["is_residence"]."','".$values["t1"]."','".$values["t2"]."','".$values["t3"]."','".$values["t4"]."','".$values["t5"]."','".$values["t6"]."','".$values["duplex"]."','".$values["commune"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
				$insert_id = mysql_insert_id();
					
				for($i=0;$i<count($this->controller->lg);$i++){
					if ($values["title-".$this->controller->lg[$i]["id"]]) {
						mysql_query("INSERT INTO neti_rubtitles
						(id,id_rub,id_lg,title) value
						('','".$insert_id."','".$this->controller->lg[$i]["id"]."','".$values["title-".$this->controller->lg[$i]["id"]]."')",$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
					}
				}
				
				return $this->slash->trad_word("NETICONTENTS_TXT-RUBADD-OK");	
			
			
			}else{
				return $this->slash->trad_word("NETICONTENTS_TXT-RUBADD-FAIL");	
			}
	
		}
		
	}
	
	
	
	/**
	 * Récupération du titre d'une rubrique
	 * @param $id rub ID
	 * @param $lg 
	 */
	public function get_title ($id,$lg){
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["title"];
		}else{
			return Null;
		}
		
	}
	
	/**
	 * Suppression d'une rubrique
	 * @param $id rub ID
	 */
	public function delete($id){
		
		$result = mysql_query("SELECT * FROM neti_rubpos WHERE id='".$id."'",$this->slash->db_handle) 
								or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			
			//Suppression rubrique et sous rubrique
			$this->delete_child($id);
			$this->single_delete($id);
			
			//Reconstruction des positions 
			$result_pos = mysql_query("SELECT id, position FROM neti_rubpos WHERE id_top='".$row["id_top"]."' ORDER BY position ASC",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			$i=1;
			while ($row_pos = mysql_fetch_array($result_pos, MYSQL_ASSOC)){
				mysql_query("UPDATE neti_rubpos set 
					position='".$i."'
					WHERE id='".$row_pos["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
		
		}
	
	}
	
	/**
	 * Suppression individuel rubrique
	 * @param $id rub ID
	 */
	public function single_delete($id){
		
		echo "DEL Rubique : ".$id;
		echo "<br/>";
		
		$result = mysql_query("SELECT * FROM neti_elements WHERE id_rub='".$id."' ORDER BY position",$this->slash->db_handle) 
								or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$this->controller->neti_elm->delete($row["id"]);
				//echo "DELETE ELEMENT : ".$row["id"]." TYPE : ".$row["type"]."<br/>";
			}
		
		}
		
		mysql_query("DELETE FROM neti_rubpos WHERE id='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		mysql_query("DELETE FROM neti_rubtitles WHERE id_rub='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
	}
	
	/**
	 * Suppression des enfants d'une rubrique
	 * @param $id rub ID
	 */
	public function delete_child($id){
		$result = mysql_query("SELECT * FROM neti_rubpos WHERE id_top='".$id."' ORDER BY position",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$this->delete_child($row["id"]);
				$this->single_delete($row["id"]);
			}	
		}
	}
	
	/**
	 * Retourne le nombre de rubique (non vide)
	 * @return $n:uint Nombre total de rubrique (non vide)
	 */
	public function get_nb_rub () {
		$result = mysql_query("SELECT * FROM neti_rubpos",$this->slash->db_handle)
		or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		return mysql_num_rows($result);
	}
	
	
	/**
	 * Récupération de la position
	 * @param $id rub ID
	 * @param $option:string 
	 * @return $pos
	 */
	public function get_position($id,$option){
		
		
		switch($option){
			
			case "next":
				
			break;
			
			
			
			default:
				return Null;
			
		}
		/*
		$result = mysql_query("SELECT * FROM neti_rubtitles WHERE id_rub='".$id."' AND id_lg='".$lg."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result) > 0) {
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row["title"];
		}else{
			return Null;
		}*/
		
	}
	
	/**
	 * Déplacement d'une rubrique
	 * @param $id Rub ID
	 * @param $option 
	 * 	up : Monter la rubrique
	 *  down : Descendre la rubrique
	 */
	public function set_position($id,$option) {

		$result1 = mysql_query("SELECT id, id_top, position FROM neti_rubpos WHERE id='".$id."'",$this->slash->db_handle) 
								or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$rub1 = mysql_fetch_array($result1, MYSQL_ASSOC);
		
		//Monter ou descendre la rubrique
		if ($option["mode"] == "up" || $option["mode"] == "down") {
			
			$order = "ASC";
			if ($option["mode"] == "up") { $order = "ASC"; }
			if ($option["mode"] == "down") { $order = "DESC"; }
						
			$result_nb = mysql_query("SELECT id, position FROM neti_rubpos WHERE id_top='".$rub1['id_top']."' ORDER BY position ".$order,$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$rub_nb = mysql_fetch_array($result_nb, MYSQL_ASSOC);
			
			if($id != $rub_nb["id"]){
				
				$current_pos = $rub1["position"];
				
				
				$next_pos = $current_pos - 1;
				if ($option["mode"] == "up") { $next_pos = $current_pos - 1; }
				if ($option["mode"] == "down") { $next_pos = $current_pos + 1; }
				
				$result2 = mysql_query("SELECT id, id_top, position FROM neti_rubpos WHERE id_top='".$rub1['id_top']."' AND position='".$next_pos."'",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$rub2 = mysql_fetch_array($result2, MYSQL_ASSOC);
				
				mysql_query("UPDATE neti_rubpos set 
					position='".$next_pos."'
					WHERE id='".$id."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
				mysql_query("UPDATE neti_rubpos set 
					position='".$current_pos."'
					WHERE id='".$rub2["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
					
				return $this->slash->trad_word("NETICONTENTS_TXT-RUBMOVE-OK");
					
			}else{
				return $this->slash->trad_word("NETICONTENTS_TXT-RUBMOVE-FAIL");
			}
		
		}elseif($option["mode"]== "set"){	
			
			if (isset($id) && isset($option["values"]["id_top"])) {
			
							
				$result_nb = mysql_query("SELECT id, position FROM neti_rubpos WHERE id_top='".$option["values"]["id_top"]."' ORDER BY position DESC LIMIT 1",$this->slash->db_handle) 
							or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$rub_nb = mysql_fetch_array($result_nb, MYSQL_ASSOC);
				
				$new_rubpos = $rub_nb["position"] + 1;
				
								
				mysql_query("UPDATE neti_rubpos set 
					id_top='".$option["values"]["id_top"]."', 
					position='".$new_rubpos."'
					WHERE id='".$id."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
			}
		}else{
			//Autre action (déplacement)
			return Null;
		}
				
			
	}
	
	
	
	/**
	* Récupération des valeurs du formulaire rubrique
	*/
	public function recovery_fields() {
		
			
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		
		for($i=0;$i<count($this->controller->lg);$i++){
			$obj["title-".$this->controller->lg[$i]["id"]] = $this->slash->sl_param($this->controller->module_name."_objtitle-".$this->controller->lg[$i]["id"],"POST");
		}
		
		$obj["password"] = $this->slash->sl_param($this->controller->module_name."_obj2","POST");
		if ($this->slash->sl_param($this->controller->module_name."_obj3","POST")){
			$obj["hidden"] = $this->slash->sl_param($this->controller->module_name."_obj3","POST");
		}else{
			$obj["hidden"] = 0;
		}

		
		$obj["url"] = $this->slash->sl_param($this->controller->module_name."_obj4","POST");
		$obj["module"] = $this->slash->sl_param($this->controller->module_name."_obj5","POST");
		$obj["enabled"] = 1;

		if ($this->slash->sl_param($this->controller->module_name."_obj6","POST")){
			$obj["is_residence"] = $this->slash->sl_param($this->controller->module_name."_obj6","POST");
		}else{
			$obj["is_residence"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj7","POST")){
			$obj["t1"] = $this->slash->sl_param($this->controller->module_name."_obj7","POST");
		}else{
			$obj["t1"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj8","POST")){
			$obj["t2"] = $this->slash->sl_param($this->controller->module_name."_obj8","POST");
		}else{
			$obj["t2"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj9","POST")){
			$obj["t3"] = $this->slash->sl_param($this->controller->module_name."_obj9","POST");
		}else{
			$obj["t3"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj10","POST")){
			$obj["t4"] = $this->slash->sl_param($this->controller->module_name."_obj10","POST");
		}else{
			$obj["t4"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj11","POST")){
			$obj["t5"] = $this->slash->sl_param($this->controller->module_name."_obj11","POST");
		}else{
			$obj["t5"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj12","POST")){
			$obj["t6"] = $this->slash->sl_param($this->controller->module_name."_obj12","POST");
		}else{
			$obj["t6"] = 0;
		}
				if ($this->slash->sl_param($this->controller->module_name."_obj13","POST")){
			$obj["duplex"] = $this->slash->sl_param($this->controller->module_name."_obj13","POST");
		}else{
			$obj["duplex"] = 0;
		}

		$obj["commune"] = $this->slash->sl_param($this->controller->module_name."_obj14","POST");
		return $obj;
		
	}
	
	
	/**
	* Vérification des valeurs du formulaire rubrique
	* @param $values:Array Object Values
	*/
	public function check_fields($values) {
		
		$mess = array();
		
		if ($values["title-".$this->controller->lg[0]["id"]]==""){
			$mess["title-".$this->controller->lg[0]["id"]]["message"] =  $this->slash->trad_word("NETICONTENTS_ERR-FIELDEMPTY");
		}
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  $this->slash->trad_word("ERROR_TITLE_FIELD_EMPTY");
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	
	
	
	
	
	
	
	
	
	/**
	* Récupération des valeurs du formulaire déplacement rubrique
	*/
	public function recovery_move_fields() {
		
			
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		
		
		$obj["id_top"] = $this->slash->sl_param($this->controller->module_name."_obj1","POST");
		
		return $obj;
		
	}
	
	
	/**
	* Vérification des valeurs du formulaire déplacement rubrique
	* @param $values:Array Object Values
	*/
	public function check_move_fields($values) {
		
		$mess = array();
		
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  $this->slash->trad_word("ERROR_TITLE_FIELD_EMPTY");
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	
	
	
	
	
	
	
	
	
}