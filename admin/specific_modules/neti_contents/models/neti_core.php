<?php
/**
* @package		SLASH-CMS / NETISSIMA
* @internal     Admin netissima functions
* @version		netissima.php - Version 11.2.14
* @author		Julien Veuillet [http://www.wakdev.com]
* @author		Logomotion [http://www.logomotion.fr]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


class neti_core{

	public $slash; //Core Reference
	public $controller; //Control Reference
	
	/**
	* Contructeur
	*/
	function __construct(&$controller_class_ref) {
		$this->slash = &$GLOBALS["slash"];
		$this->controller = $controller_class_ref;
		//if (!isset($_COOKIE['simpleTree'])) { setcookie('simpleTree','2',(time() + 3600)); }
	}
	
	
	/**
	* Menu des rubriques
	*/
	public function make_menu (){
	
		//echo "<div id='draggableSample' style='width:20px; height:20px; background-color:#FF0000;'></div>";
	
		echo "<div id='tree'>";
		
		echo "<ul class='simpleTree neti-menu' style='padding-top:10px; padding-bottom:10px;'>";
				
		$class_span ="";
		if ($this->controller->idr == "root") {
			$class_span = "class='neti-rub-active'";
		}
				
		echo "<li class='root active focused expanded' data=\"url: 'index.php?mod=neti_contents&idr=root', icon: 'folder_home.png'\" ><span ".$class_span.">".$this->slash->trad_word("NETICONTENTS_BT-ROOT")."</span>";
			
		$this->neti_menu(0);
		
		echo "</li></ul>";
		
		echo "</div>"; 
	}
	
	
	
	
	
	/**
	* Aper�u de la rubrique
	* @param $idr Id Rubrique
	* @param $lg Id Lang
	*/
	public function make_preview ($idr,$lg) {
		if ($this->controller->idr != "root") {
			$this->controller->neti_elm->load_page($idr,$lg);
		}else{			
			echo "<span>".$this->slash->trad_word("NETICONTENTS_TXT-RUBNEEDSEL")."</span>";
		}
	}
	
	
	/**
	* Menu des rubriques
	*/
	
	/*private function neti_menu2($parent) {
	
		$requete_child=mysql_query("SELECT * FROM neti_rubpos, neti_rubtitles 
									WHERE id_top='".$parent."' 
									AND neti_rubtitles.id_lg = '1' AND neti_rubtitles.id_rub = neti_rubpos.id 
									GROUP BY neti_rubtitles.id_rub ORDER BY neti_rubpos.position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if (mysql_num_rows($requete_child) != 0)
		{
			echo "<ul>";
			
			while($tab_child = mysql_fetch_array($requete_child))
			{
				$requete_type=mysql_query("SELECT * FROM neti_rubpos WHERE id_top='".$tab_child["id_rub"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				$style_li = "";
				$style_span = "";
				$options="";
				
				if (mysql_num_rows($requete_type) == 0) {
					$style_li = "style='margin-left:-16px; background: url(specific_modules/neti_contents/views/default/images/folder.gif) 0 -2px no-repeat #fff;'";

				}
				
				if ($tab_child["password"]) {
						$options .= "<img src='specific_modules/neti_contents/views/default/images/lock.png' valign='text-top'/>";	
				}
				
				if ($tab_child["hidden"]==1) {
						$options .= "<img src='specific_modules/neti_contents/views/default/images/hidden.png' valign='text-top'/>";	
				}
				
				if ($tab_child["url"]) {
						$options .= "<img src='specific_modules/neti_contents/views/default/images/link.png' valign='text-top'/>";	
				}
				
				if ($tab_child["module"]) {
						$options .= "<img src='specific_modules/neti_contents/views/default/images/module.png' valign='text-top'/>";	
				}
				
				if ($tab_child["id_rub"] == $this->controller->idr) {
					$style_span = "class='neti-rub-active'";
				}
				
				
				echo "<li class='folder' id='".$tab_child["id_rub"]."' ".$style_li."><span ".$style_span.">".$options." <a href='index.php?mod=neti_contents&idr=".$tab_child["id_rub"]."'>".$tab_child["title"]."</a></span>";
				
				
				$this->neti_menu($tab_child["id_rub"]);
			}
			echo "</li></ul>"; 
			
		}
	
	}*/
	
	
	
	/**
	* Menu des rubriques via dynatree
	*/
	private function neti_menu($parent) {
	
		$requete_child=mysql_query("SELECT * FROM neti_rubpos, neti_rubtitles 
									WHERE id_top='".$parent."' 
									AND neti_rubtitles.id_lg = '1' AND neti_rubtitles.id_rub = neti_rubpos.id 
									GROUP BY neti_rubtitles.id_rub ORDER BY neti_rubpos.position ASC",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		if (mysql_num_rows($requete_child) != 0)
		{
			echo "<ul>";
			
			while($tab_child = mysql_fetch_array($requete_child))
			{
				$requete_type=mysql_query("SELECT * FROM neti_rubpos WHERE id_top='".$tab_child["id_rub"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				
				$style_span = "";
				
				
				$n1 = "1";
				$n2 = "0";
				$n3 = "0";
				$n4 = "0";
				
				if (mysql_num_rows($requete_type) == 0) {
					// a faire
				}
				
				if ($tab_child["password"]) {
						$n2 = "1";
				}
				
				if ($tab_child["hidden"]==1) {
						$n1 = "0";
				}
				
				if ($tab_child["url"]) {
						$n3 = "1";
				}
				
				if ($tab_child["module"]) {
						$n4 = "1";
				}
				
				if ($tab_child["id_rub"] == $this->controller->idr) {
					$style_span = "class='neti-rub-active'";
					// Icone pour rubrique en cours ?
				}
				
				$options = "icon: '".$n1.$n2.$n3.$n4.".png'";
			
				echo "<li class='folder' data=\"url: 'index.php?mod=neti_contents&idr=".$tab_child["id_rub"]."', ".$options."\" id='".$tab_child["id_rub"]."' >".$tab_child["title"];
				
				
				
				$this->neti_menu($tab_child["id_rub"]);
			}
			echo "</li></ul>"; 
			
		}
		
	}
	
	
	
}


?>