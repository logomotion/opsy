<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_DEFAUT_TEMPLATE
* @internal     Defaut Admin Template
* @version		template.php - Version 11.5.26
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

/**
 * @todo		Plugins initialisation
 */

//slash->initialise_plugins();
$this->initialise_modules(); // Modules initialisation
$this->load_module("sla_header"); // Loading header module
?>
<!-- Global Styles -->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config["admin_template_url"]; ?>css/styles.css" />
<!--<link rel="stylesheet" type="text/css" href="<?php //echo $this->config["admin_template_url"]; ?>css/modules.css" />-->
<link rel="stylesheet" type="text/css" href="<?php echo $this->config["admin_template_url"]; ?>css/assets.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config["admin_template_url"]; ?>css/panel.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config["admin_template_url"]; ?>css/interface.css" />
<link rel="stylesheet" type="text/css" href="<?php echo $this->config["admin_template_url"]; ?>css/form.css" />
<link href="<?php echo $this->config["admin_template_url"]; ?>images/favicon.ico" rel="shortcut icon" type="image/x-icon" />
</head>
<body>
<div class="sl-container">
	<div class="sl-header">
		<img src="<?php echo $this->config["admin_template_url"]; ?>images/logo.jpg" border="0"/>
	</div>
	<div class="sl-menu"><?php $this->load_module("sla_admmenu"); ?></div>
	<div class="sl-content">
		<?php 
		if ($this->sl_param("mod")) {
			$this->load_module($this->sl_param("mod")); 
		} else {
			$this->load_module("sla_panel"); 
		}
		?>
	</div>
	<br /><br />
	<div class="sl-footer">Powered by <a href="http://www.slash-cms.com" target="_blank" class="footer_credit">Slash CMS</a></div>
</div>
<?php $this->execute_modules(); ?>
</body>
</html>