<?php
/**
* @package		SLASH-CMS
* @subpackage	FR CATEGORIES MODULE LANGUAGES
* @internal     French categories module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_CATEGORIES
define("CATEGORIES_TITLE", "Gestion des cat&eacute;gories");
define("CATEGORIES_DELETE_CONFIRM", "Supprimer cette cat&eacute;gorie ?");
define("CATEGORIES_ERROR_EXIST", "Cette cat&eacute;gorie exite d&eacute;j&agrave; !");
?>
