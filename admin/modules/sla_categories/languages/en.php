<?php
/**
* @package		SLASH-CMS
* @subpackage	EN CATEGORIES MODULE LANGUAGES
* @internal     English categories module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_CATEGORIES
define("CATEGORIES_TITLE", "Categories config");
define("CATEGORIES_DELETE_CONFIRM", "Delete this categorie ?");
define("CATEGORIES_ERROR_EXIST", "This categorie is already exist !");
?>
