<?php
/**
* @package		SLASH-CMS
* @subpackage	FR SLIDER MODULE LANGUAGES
* @internal     French SLIDER module translate
* @version		fr.php - Version 10.2.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_SLIDER
define("SLIDER_TITLE", "Gestion des SLIDER");
define("SLIDER_DELETE_CONFIRM", "Supprimer la / les SLIDER ?");
define("SLIDER_IN_PROGRESS", "En cours");
define("SLIDER_PAST", "Expir&eacute;es");
define("SLIDER_PUBLISH_DATE", "Date de publication");
define("SLIDER_PUBLISH_TIME", "Heure de publication");
define("SLIDER_UNPUBLISH_DATE", "Date de d&eacute;publication");
define("SLIDER_UNPUBLISH_TIME", "Heure de d&eacute;publication");
define("SLIDER_PERMANENT", "SLIDER permanente");
?>
