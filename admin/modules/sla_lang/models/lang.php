<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_LANG
* @internal     Admin lang module
* @version		lang.php - Version 11.3.25
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class lang extends slaModel implements iModel{



	public function load_items() {
		
		$search = "";
			
		if ($_SESSION[$this->controller->module_name."_search"] != "#") {
			$search = "WHERE title LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' OR shortname LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' ";
		}
			
		$result = mysql_query("SELECT id,name,shortname,enabled FROM ".$this->slash->database_prefix."lang WHERE enabled='1' ".$search."ORDER BY ".$_SESSION[$this->controller->module_name."_orderby"]." ".$_SESSION[$this->controller->module_name."_sort"],
		$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		
		$objects = array();
		
		$obj_ids = array("id","name","shortname","flag");
		$obj_titles = array("ID",$this->slash->trad_word("NAME"),$this->slash->trad_word("LANG_SHORTNAME"),$this->slash->trad_word("LANG_FLAGS"));
		$obj_sorts = array(false,true,true,false);
		$obj_sizes = array(5,40,25,10);
		$obj_actions = array(false,false,false,false);

		$obj_controls = array("single_delete");
		

		while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
			
			/* CONTENT */
			$row[3] = "<img src='templates/system/images/flags/".$row[2].".png' height='20' width='20' />";
			
			array_push($objects,$row);
		}
		
		sl_interface::create_listing($this->controller->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,false);
		
	}
	
	/**
	 * Get the language shortname
	 * @param none
	 * @return shortname languages available array
	 */
	public function load_disabled_items() {
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."lang WHERE enabled='0'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			$return_array = array();
			$id = 0;
			
			while($row = mysql_fetch_array($result, MYSQL_ASSOC)){
				$return_array[$id]["id"] = $row["id"];
				$return_array[$id]["shortname"] = $row["shortname"];
				$return_array[$id]["name"] = $row["name"];
				$id++;
			}
			
			return $return_array;
	}
	
	/**
	 * Load categorie
	 * @param $id Categorie ID
	 */
	public function load_item($id) {
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."lang WHERE id='".$id."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row;
	}
	
	
	/**
	 * Delete categorie
	 * @param $id Categorie ID
	 */
	public function delete_items($id_array) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."lang set 
					enabled='0' 
					WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	 * Save categorie
	 */
	public function save_item($id,$values){
		
		/*if ($id != 0) {*/
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."lang set  
					enabled='".$values["enabled"]."' 
					WHERE id='".$values["id"]."'
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		/*} else {
			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."lang
					(id,name,shortname,enabled) value
					('','".$values["name"]."','".$values["shortname"]."','".$values["enabled"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		}*/
					
	}
	
	/**
	 * Set is enabled
	 * @param $id article ID
	 *//*
	private function set_items_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."lang set enabled='".$enabled."' WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}*/
	
	
	
	/**
	* Recovery fields value
	*/
	public function recovery_fields() {
	
		$obj = array();
		
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_obj0","POST");
		$obj["name"] = $this->slash->sl_param($this->controller->module_name."_obj0","POST");
		$obj["shortname"] = $this->slash->sl_param($this->controller->module_name."_obj0","POST");
		$obj["enabled"] = $this->slash->sl_param($this->controller->module_name."_obj2","POST");
		
		return $obj;
		
	}
	
	
	/**
	* Check add/edit values
	* @param $values:Array Object Values
	*/
	public function check_fields($values) {
		/*
		$mess = array();
		echo "test";
		//languages verification
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."lang WHERE name='".$values["name"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		
		if ($row["shortname"] == 1) {
			$mess[0]["message"] = $this->slash->trad_word("LANG_ERROR_EXIST");
		}
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	*/
	}
	

}



?>