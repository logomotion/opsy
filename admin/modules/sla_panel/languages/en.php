<?php
/**
* @package		SLASH-CMS
* @subpackage	EN PANEL MODULE LANGUAGES
* @internal     English panel module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_PANEL
define("PANEL_TITLE", "Main panel");
define("PANEL_WELCOME", "Welcome to your admin");

define("PANEL_SLA_ARTICLE", "Articles");
define("PANEL_SLA_CATEGORIES", "Cat&eacute;gories");
define("PANEL_SLA_MENU", "Menus");
define("PANEL_SLA_NEWS", "News");
define("PANEL_SLA_PAGES", "Pages");
define("PANEL_SLA_USERS", "Users");
?>
