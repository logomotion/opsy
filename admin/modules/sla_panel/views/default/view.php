<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_PANEL
* @internal     Admin Panel module
* @version		view.php - Version 9.10.20
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class sla_panel_view extends slaView implements iView {

	

	public function header () {
		
		//<link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
		//echo "<link rel='stylesheet' type='text/css' href='templates/system/css/sla_menu.css' media='screen'>";
		//echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/superfish/js/hoverintent.js'></script> \n";
		//echo "<script type='text/javascript' src='../core/plugins/fckeditor/pick.js'></script> \n";
		//echo "<link rel='stylesheet' type='text/css' href='modules/sla_panel/view/default/css/sla_panel.css' media='screen'>";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
	
	}
	
	
	public function show_panel() {
		
		
		echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='sl_mod_title' align='left' width='50%'><img src='modules/".$this->controller->module_name."/views/default/images/".$this->controller->module_name.".png' align='absmiddle' /> ".$this->slash->trad_word("PANEL_TITLE")."</td>		
					</tr>
					</table>
					
					
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='mod_panel'><br />".$this->slash->trad_word("PANEL_WELCOME")." : <u>".$this->controller->get_admin_username()."</u> <br /> <br />";
						
						echo "<table width='400' cellspacing='20' cellpadding='0' border='0'><tr>";
						
							/* ------- PANEL ITEMS -------  */
							
							/* ARTICLES MODULE */
							echo "<td align='center'><a href='index.php?mod=neti_contents' class='panel_sla_articles'></a>Gestion du contenu</td>";	
					
							/* NEWS MODULE */
							echo "<td align='center'><a href='index.php?mod=sla_news' class='panel_sla_news'></a>Gestion des actualit&eacute;s</td>";
							
							/* SLIDER MODULE 
							echo "<td align='center'><a href='index.php?mod=sla_slider' class='panel_sla_slider'></a>Gestion du slider</td>";
						/*
							/* USERS MODULE */
							echo "<td align='center'><a href='index.php?mod=sla_users' class='panel_sla_users'></a>".$this->slash->trad_word("PANEL_SLA_USERS")."</td>";								
							
						
							/* --------------------------  */
							
							echo "</tr></table>";	

						
			echo "			</td>
					</tr>
					</table>	
								
			</td>
			</tr></table>
			
					";
					

		
	}
	
	
	
	
	/**
	 * HTML footer
	 */
	public function footer() {
	
		
		echo "
		<script type='text/javascript'> 
 
			$(document).ready(function(){ 
			
			
				$.preloadCssImages();
				
		}); 

 		 			
		</script>";
	
	}
	
	
	
}





?>