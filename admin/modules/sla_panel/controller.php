 <?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_PANEL
* @internal     Admin Panel module
* @version		controller.php - Version 9.12.16
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

/**
 * @todo		Load panel system with core/configuration.php
 */

include ("views/default/view.php");

class sla_panel_controller extends slaController implements iController{

	
	public $module_name = "sla_panel";
	
	
	public $view;
	
	/**
	* Contructor
	
	*/
	function sla_construct() {
       
	    $this->view = new sla_panel_view($this);
	}
	

	
	
	/**
	 * Load header function # require by slash-cms #
	 */
	public function load_header(){
		$this->view->header();
	}
	
	/**
	 * Load footer function # require by slash-cms #
	 */
	public function load_footer(){
		$this->view->footer();
	}
	
	/**
	 * User admin name
	 */
	public function get_admin_username (){
		$row_user = $this->slash->get_admin_infos();
		return $row_user["name"];
	}
	
	/**
	* Load function # require by slash-cms #
	*/
	public function load() {
		$this->view->show_panel();
	}

	


}



?>
