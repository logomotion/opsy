<?php
/**
* @package		SLASH-CMS
* @subpackage	FR SECURE MODULE LANGUAGES
* @internal     French secure module translate
* @version		fr.php - Version 10.7.5
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_SECURE
define("SECURE_CONNEXION_LOGIN", "Utilisateur");
define("SECURE_CONNEXION_PASSWORD", "Mot de passe");
define("SECURE_CONNEXION_SUBMIT", "Connexion");
define("SECURE_CONNEXION_ERROR", "Erreur lors de la connexion");
define("SECURE_CONNEXION_NO_USER", "Login ou mot de passe incorrect");
define("SECURE_CONNEXION_INACTIVE_USER", "Utilisateur inactif");
define("SECURE_CONNEXION_TEXT", "Connexion &agrave; l'administration");
define("SECURE_DECONNEXION_TEXT", "D&eacute;connexion");
define("SECURE_CONNEXION_USERNAME", "Connect&eacute; en tant que");
?>
