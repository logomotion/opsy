<?php
/**
* @package		SLASH-CMS
* @subpackage	EN SECURE MODULE LANGUAGES
* @internal     English secure module translate
* @version		en.php - Version 10.7.5
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_SECURE
define("SECURE_CONNEXION_LOGIN", "User");
define("SECURE_CONNEXION_PASSWORD", "Password");
define("SECURE_CONNEXION_SUBMIT", "Connexion");
define("SECURE_CONNEXION_ERROR", "Connexion error");
define("SECURE_CONNEXION_NO_USER", "No valid user");
define("SECURE_CONNEXION_INACTIVE_USER", "Inactive user");
define("SECURE_CONNEXION_TEXT", "Administration connexion");
define("SECURE_DECONNEXION_TEXT", "Disconnect");
define("SECURE_CONNEXION_USERNAME", "Connected on");
?>
