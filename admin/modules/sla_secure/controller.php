<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_SECURE
* @internal     Admin Login module
* @version		controller.php - Version 9.12.16
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


/**
* @todo		Use Cookies
* @todo		Log connexion function
*/


// Include Module View
include ("views/default/view.php");

class sla_secure_controller extends slaController implements iController{

	
	
	public $view;
	
	/**
	* Contructor
	*/
	function sla_construct() {
       
	   $this->view = new sla_secure_view($this);
	}
	
	
	/**
	* Initialise function # Require by slash-cms #
	*/
	public function initialise() {
		
		$this->error = "no_error";
		
		if (isset($this->slash->get_params["mod"]) 
		&& $this->slash->get_params["mod"] == "sla_secure" ) {
			
			// Login script
			if ($this->slash->get_params["act"] == "login") {
				
				$this->error = "no_user";
				$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."users",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
					
					if ($row["login"] == $this->slash->post_params["sla_secure_login"]  && $row["password"] == sha1($this->slash->post_params["sla_secure_password"]) ) {
						if ($row["enabled"]!= 0) {
							$_SESSION["id_user"] = $row["id"];
							$_SESSION["user_language"] = $row["language"];
							$this->slash->get_params["mod"] = "sla_panel";
						}else{
							$this->error = "inactive_user";
						}
					}
				}
			
			}
			
			
			// Logout script
			if ($this->slash->get_params["act"] == "logout") {
				
				$_SESSION = array();
				/*
				if (isset($_COOKIE[session_name()])) {
				    setcookie(session_name(), '', time()-42000, '/');
				}*/
		
				session_destroy();

				$this->slash->get_params["mod"] = "sla_secure";
				
			}
			
		} 
		
		if (isset($_SESSION["id_user"]) && $_SESSION["id_user"] != null) {
		
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."users WHERE id=".$_SESSION["id_user"],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			if( mysql_num_rows($result)  == 0 ) {
				$this->slash->get_params["mod"] = "sla_secure";
			}else{
				$row = mysql_fetch_array($result, MYSQL_ASSOC);
				$_SESSION["user_language"] = $row["language"];
			}
			
			
		
		} else {
			$this->slash->get_params["mod"] = "sla_secure";			
		}
		
	
	}
	
	/**
	 * Load Header function # Require by slash-cms #
	 */
	public function load_header(){
		$this->view->header();
	}
	
	/**
	* Load function # Require by slash-cms #
	*/
	public function load() {
	
		
		switch ($this->error) {
			
			case "no_error" :
				$this->view->login_form();
			break;
			
			case "no_user" :
				$this->view->show_error("SECURE_CONNEXION_NO_USER");
				$this->view->login_form();
			break;
			
			case "inactive_user" :
				$this->view->show_error("SECURE_CONNEXION_INACTIVE_USER");
				$this->view->login_form();
			break;
			
			default:
				$this->view->show_error("SECURE_CONNEXION_ERROR");
				$this->view->login_form();
			
		}
		
	}
	
	/**
	 * Load footer function # Require by slash-cms #
	 */
	public function load_footer(){
		$this->view->footer();
	}
	
	

}

?>