<?php
/**
* @package		SLASH-CMS
* @subpackage	EN MENU MODULE LANGUAGES
* @internal     English menu module translate
* @version		en.php - Version 10.4.8
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_MENU
define("SLA_MENU_TITLE", "Menu config");
define("SLA_MENU_DELETE_CONFIRM", "Delete this menu ?");
define("SLA_MENU_ACTION", "Link action");
define("SLA_MENU_SELECT_MENU", "Menu");
define("SLA_MENU_LEVEL", "Level");
define("SLA_MENU_TOPLEVEL", "Top");
?>
