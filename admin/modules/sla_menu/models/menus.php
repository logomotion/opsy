<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_MENUS
* @internal     Admin menu module
* @version		menus.php - Version 11.6.1
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class menus extends slaModel implements iModel{

	
	
	/**
	 * Load links from database
	 */
	public function load_items() {
						
		$filter = "";
		if ($_SESSION[$this->controller->module_name."_categorie1"] != -1) {
			$filter_menu = $_SESSION[$this->controller->module_name."_categorie1"];
		}

		$objects = array();
		$obj_ids = array("id","title","action","enabled");
		$obj_titles = array("ID",
							$this->slash->trad_word("TITLE"),
							$this->slash->trad_word("SLA_MENU_ACTION"),
							$this->slash->trad_word("ACTIVE"));
		$obj_sorts = array(false,false,false,false);
		$obj_sizes = array(5,40,40,5);
		$obj_actions = array(false,"single_edit","single_edit","set_state");
		$obj_controls = array("single_up","single_down","single_edit","single_delete");
		
		$this->load_child_links($objects,$filter_menu,0,0);

		sl_interface::create_listing($this->controller->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,false);

	}
	
	/**
	 * Set position
	 * @param $id item ID
	 */
	public function set_position($id,$order) {
			
			$link = $this->load_item($id);
			
			if ($order=="up") { $next_position = $link["position"]-1; } 
			if ($order=="down") { $next_position = $link["position"]+1; } 
			
			if ($order=="up" || $order=="down") {
				$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type='2' AND parent='".$link["parent"]."' AND position='".$next_position."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$num_rows = mysql_num_rows($result);
				
				if ($num_rows != 0) {
					$row = mysql_fetch_array($result, MYSQL_ASSOC);
					$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set position='".($link["position"])."' WHERE parent='".$link["parent"]."' AND position='".$next_position."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set position='".$next_position."' WHERE id='".$link["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}
			}
	}
	
	
	/**
	 * Load item
	 * @param $id item ID
	 */
	public function load_item($id) {
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		return $row;
	}
	
	
	/**
	 * Delete categorie
	 * @param $id Categorie ID
	 */
	public function delete_items($id_array) {
		foreach ($id_array as $value) {
			
			/*if (file_exists("../medias/attachments/sl_articles/".$value)) {
				$this->delete_attachment("../medias/attachments/sl_articles",$value);
			}*/
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."menu WHERE id=".$value,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		}
	}
	
	/**
	 * Load menus from database
	 */
	public function load_menus() {
		
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=1 AND enabled=1 ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$menus = array();
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			array_push($menus,$row); 
		}
		return $menus;
		
	}
	
	/**
	 * Load links
	 * @param $id item ID
	 */
	public function load_links($menu_id) {
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=2 AND menu_id='".$menu_id."' ORDER BY parent,position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		$links = array();
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			array_push($links,$row); 
		}
		return $links;
	}
	
	
	/**
	 * Load child links
	 */
	public function load_child_links(&$objects,$menu_id,$parent,$level) {
	
		$count=0;
		$result_link = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=2 AND menu_id=".$menu_id." AND parent=".$parent." ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($row_link = mysql_fetch_array($result_link, MYSQL_BOTH)) {
		    
		    $row = array();
		    $row[0] = $row_link["id"];
			$row["id"] = $row_link["id"];
		    $row[1] = " ".$row_link["title"];
			
			 
		    for ($i=0; $i<$level; $i++){
		    $row[1] = "--".$row[1];
		    }
		   
		    $row[2] = $row_link["action"];
			$row[3] = $row_link["enabled"];
			
		    array_push($objects,$row); 

		    $count++;
		    
		    
		    $result_child = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=2 AND menu_id=".$menu_id." AND parent=".$row_link["id"],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		    
		    $child_count = mysql_num_rows($result_child);
		    
		    if ($child_count > 0 ) {
			    $this->load_child_links($objects,$menu_id,$row_link["id"],$level+1);
		    }
					
		}
		
	
	}
	
	
	
	
	/**
	 * Load link menu
	 * @param $id link menu ID
	 */
	 /*
	protected function load_link($id) {
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image' AND id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row;
	}
	*/
	
	/**
	 * Save categorie
	 */
	public function save_item($id,$values){
	
		print_r($values);
		
		if ($id != 0) {
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set 
					menu_id='".$values["menu_id"]."',
					parent='".$values["parent"]."',
					title='".$values["title"]."',
					action='".$values["action"]."',
					enabled='".$values["enabled"]."' 
					WHERE id='".$values["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else {
		
			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."menu
					(id,menu_id,pri_type,sec_type,parent,title,action,enabled) value
					('','".$values["menu_id"]."','2','url_self','".$values["parent"]."','".$values["title"]."','".$values["action"]."','".$values["enabled"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			$insert_id = mysql_insert_id();
			/*		
			$ret = $this->save_attachment("../medias/attachments/sl_articles",$insert_id);
			
			if (!$ret){
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}else{
				return $this->slash->trad_word("SAVE_SUCCESS");
			}	
			*/
			return $this->slash->trad_word("SAVE_SUCCESS");
		}
		
	}
	
	/**
	 * Save attachment 
	 */
	public function save_attachment($destination,$id_element){
		
		$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
		or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
		
		if (mysql_num_rows($result_files) > 0) {
		
			$sl_files_sv = new sl_files();
			
			if (!$sl_files_sv->make_dir($destination."/".$id_element)){
				return false;
			}
			
			
			
			while ($row = mysql_fetch_array($result_files, MYSQL_BOTH)) {
				if (!$sl_files_sv->move_files("../tmp/".$row["filename"],$destination."/".$id_element."/".$row["filename"])){
					return false;
				}
			}
			
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."attachments set 
					id_element='".$id_element."',
					state='1'
					WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0'
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
			return true;
		}else{
			return true;
		}
	}
	
	/**
	* Delete attachment
	*/
	public function delete_attachment($destination,$id_element){
	
		$sl_files_dl = new sl_files();
		
		if ($sl_files_dl->remove_dir($destination."/".$id_element)) {
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."attachments WHERE id_module=".$this->controller->module_id." AND id_element='".$id_element."' and state=1",$this->slash->db_handle) 
									or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			return true;
		}else{
			return false;
		}
				
	}
	
	/**
	 * Set is enabled
	 * @param $id article ID
	 */
	public function set_items_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set enabled=".$enabled." WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	 * Load categorie from database
	 */
	public function load_categories() {
		
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories ORDER BY title",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$categories = array();
		$i = 0;
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$categories[$i] = $row;
			$i++;
			
		}
		return $categories;
		
	}
	
	
	/**
	* Recovery fields value
	*/
	public function recovery_fields() {
	
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		$obj["title"] = $this->slash->sl_param($this->controller->module_name."_obj1","POST");
		$obj["menu_id"] = $this->slash->sl_param($this->controller->module_name."_obj2","POST");
		//$obj["parent"] = $this->slash->sl_param($this->controller->module_name."_obj3","POST");
		$obj["action"] = $this->slash->sl_param($this->controller->module_name."_obj4","POST");
		$obj["enabled"] = $this->slash->sl_param($this->controller->module_name."_obj5","POST");
		
		$obj["parent"] = $this->slash->sl_param($this->controller->module_name."_obj6","POST");
		
		return $obj;
		
	}
	
	
	/**
	* Check add/edit values
	* @param $values:Array Object Values
	*/
	public function check_fields($values) {
		
		$mess = array();
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  $this->slash->trad_word("ERROR_TITLE_FIELD_EMPTY");
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	
	
}

?>