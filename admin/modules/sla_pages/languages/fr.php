<?php
/**
* @package		SLASH-CMS
* @subpackage	FR SLA_PAGES MODULE LANGUAGES
* @internal     French pages module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_PAGES
define("SLA_PAGES_TITLE", "Gestion des pages");
define("SLA_PAGES_DELETE_CONFIRM", "Supprimer les pages ci-dessous ?");
?>
