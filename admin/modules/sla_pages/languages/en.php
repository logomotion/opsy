<?php
/**
* @package		SLASH-CMS
* @subpackage	EN PAGES MODULE LANGUAGES
* @internal     English pages module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_PAGES
define("SLA_PAGES_TITLE", "pages config");
define("SLA_PAGES_DELETE_CONFIRM", "Delete this page ?");
?>
