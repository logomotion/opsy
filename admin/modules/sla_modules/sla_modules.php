 <?php
/**
* @package		SLASH-CMS
* @subpackage	sla_modules
* @internal     	Admin categories module
* @version		sla_modules.php - Version 10.1.5
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/




// Include Default Module View
include ("views/default/sla_modules_view.php");

class sla_modules extends sla_modules_view implements iController{

	public $slash; //Core Reference
	public $params;
	public $module_name = "sla_modules";
	public $module_id;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
	}
	
	
	/**
	* Initialise function 
	* Require function by slash core
	*/
	public function initialise() {
		//no global initialisation for this module
	}
	
	/**
	 * Load header function
	 */
	public function load_header(){
		//SESSION
		sl_interface::listing_sessions($this->module_name,array('name','type','url'));
		$this->header(); //show script header
	
	}
	
	
	/**
	 * Load footer function
	 */
	public function load_footer(){
		$this->footer();
	}
	
	/**
	* Load module function
	* Require function by slash core
	*/
	public function load() {
		
		switch ($this->slash->sl_param($this->module_name."_act","POST")) {
			
			case "add":
				$this->show_form();
			break;
			
			case "edit":
				$values = $this->slash->sl_param($this->module_name."_checked","POST");
				if (isset ($values) && count($values) > 0) {
					reset($values);
					$obj_values = $this->load_item(current($values));
					$this->show_form($obj_values["id"],$obj_values);
				}else{
					$this->show_items($this->slash->trad_word("SELECTION_REQUIRE"));
				}
			break;
			
			case "save":
			
				$obj_values = $this->recovery_fields();
				$obj_errors = $this->check_fields($obj_values);
			
				if ($obj_errors != null) {
					$this->show_form($obj_values["id"],$obj_values,$obj_errors);
				}else{
					$this->save_item($obj_values["id"],$obj_values);
					
					if ($obj_values["id"] != 0) {
						$this->show_items($this->slash->trad_word("EDIT_SUCCESS"));
					}else{
						$this->show_items($this->slash->trad_word("SAVE_SUCCESS"));
					}
				}

			break;
			case "set_enabled": //Set enabled
				$values = $this->slash->sl_param($this->module_name."_checked","POST");
				if (isset ($values) && count($values) > 0) {
					$this->set_item_enabled($this->slash->sl_param($this->module_name."_checked","POST"),1);
					$this->show_items($this->slash->trad_word("ITEM_ENABLE_SUCCESS"));
				}else{
					$this->show_items($this->slash->trad_word("SELECTION_REQUIRE"));
				}
			break;
			case "set_disabled": //Set disabled
				$values = $this->slash->sl_param($this->module_name."_checked","POST");
				if (isset ($values) && count($values) > 0) {
					$this->set_item_enabled($this->slash->sl_param($this->module_name."_checked","POST"),0);
					$this->show_items($this->slash->trad_word("ITEM_DISABLE_SUCCESS"));
				}else{
					$this->show_items($this->slash->trad_word("SELECTION_REQUIRE"));
				}
			break;			
			case "delete":
				if ($this->slash->sl_param($this->module_name."_valid","POST")) {
					$this->delete_items($this->slash->sl_param($this->module_name."_checked","POST"));
					$this->show_modules($this->slash->trad_word("DELETE_SUCCESS"));
				}else {
					$values = $this->slash->sl_param($this->module_name."_checked","POST");
					
					if (isset ($values) && count($values) > 0) {
						$this->show_delete($this->slash->sl_param($this->module_name."_checked","POST"));
					}else{
						$this->show_modules($this->slash->trad_word("SELECTION_REQUIRE"));
					}
					
				}
					
			break;
			
			default:
				
				$this->show_modules();
		}
		
		
	}
	
	
	/**
	* Execute function
	* Require function by slash core
	*/
	public function execute() {
		
	}
	
	
	
	/* ---------------- */
	/* MODULE FUNCTIONS */
	/* ---------------- */
	
	protected function load_items() {
		
		
		/* Order */
		$filter = "";
		if ($_SESSION[$this->module_name."_categorie1"] != -1) {
			$filter = "WHERE type='".$_SESSION[$this->module_name."_categorie1"]."' ";
		}	
		if ($_SESSION[$this->module_name."_search"] != "#") {
			if ($filter == ""){
				$filter = "WHERE name LIKE '%".$_SESSION[$this->module_name."_search"]."%' ";
			}else{
				$filter .= "AND name LIKE '%".$_SESSION[$this->module_name."_search"]."%' ";
			}
		}

		$result = mysql_query("SELECT id,type,name,url,enabled, initialise_order 
								FROM ".$this->slash->database_prefix."modules ".$filter."ORDER BY ".$_SESSION[$this->module_name."_orderby"]." ".$_SESSION[$this->module_name."_sort"],
		$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		
		$objects = array();
		
		$obj_ids = array("id","type","name","url","enabled");
		$obj_titles = array("ID",$this->slash->trad_word("TYPE"),$this->slash->trad_word("NAME"),$this->slash->trad_word("ADDRESS"),$this->slash->trad_word("ACTIVE"),$this->slash->trad_word("GLOBAL"));
		$obj_sorts = array(false,true,true,true,false,false);
		$obj_sizes = array(5,10,30,30,5,5);
		$obj_actions = array(false,"single_edit","single_edit","single_edit","set_state",false);

		$obj_controls = array("single_edit","single_delete");
		

		while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
			
			/* CONTENT */
			/*$sl_txt = new sl_text();
			$h2t_content =& new html2text($row[2]);
			$row[2] = $sl_txt->substring_word($h2t_content->get_text(),80,true);
			*/
			
			
			if ($row[5] > 0) {
				$pos = $row[5];
				$row[5] = $this->slash->trad_word("YES")." (".$pos.")";
			}else{
				$row[5] = $this->slash->trad_word("NO")." (0)";
			}
			
			array_push($objects,$row);
		}
		
		sl_interface::create_listing($this->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,true);
		
	}
	
	
	
	
	
	/**
	 * Load categorie
	 * @param $id Categorie ID
	 */
	protected function load_item($id) {
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."modules WHERE id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row;
	}
	
	
	/**
	 * Delete categorie
	 * @param $id Categorie ID
	 */
	protected function delete_items($id_array) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."modules WHERE id=".$value,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	 * Save categorie
	 */
	protected function save_module($id,$values){
		
			
		if ($id != 0) {
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."modules set 
					type='".$values["type"]."',
					name='".$values["name"]."',
					url='".$values["url"]."', 
					initialise_order='".$values["initialise_order"]."', 
					enabled='".$values["enabled"]."' 
					WHERE id='".$values["id"]."'
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		} else {
			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."modules
					(id,type,name,url,initialise_order,enabled) value
					('','".$values["type"]."','".$values["name"]."','".$values["url"]."','".$values["initialise_order"]."','".$values["enabled"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		}
					
		
		
	}
	
	/**
	 * Set is enabled
	 * @param $id article ID
	 */
	private function set_items_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."modules set enabled=".$enabled." WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	* Recovery fields value
	*/
	private function recovery_fields() {
	
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->module_name."_id_obj","POST");
		$obj["type"] = $this->slash->sl_param($this->module_name."_obj0","POST");
		$obj["name"] = $this->slash->sl_param($this->module_name."_obj1","POST");
		$obj["url"] = $this->slash->sl_param($this->module_name."_obj2","POST");
		$obj["initialise_order"] = $this->slash->sl_param($this->module_name."_obj3","POST");
		$obj["enabled"] = $this->slash->sl_param($this->module_name."_obj4","POST");
		
		return $obj;
		
	}
	
	
	/**
	* Check add/edit values
	* @param $values:Array Object Values
	*/
	private function check_fields($values) {
		
		$mess = array();
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}*/
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	

}



?>
