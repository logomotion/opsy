<?php
/**
* @package		SLASH-CMS
* @subpackage	EN MODULE LANGUAGES
* @internal     English  module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_MODULES
define("MODULES_TITLE", "Modules config");
define("MODULES_TYPE", "Module type");
define("MODULES_DELETE_CONFIRM", "Delete this module ?");
?>
