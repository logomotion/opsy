<?php
/**
* @package		SLASH-CMS
* @subpackage	FR Module LANGUAGES
* @internal     French  module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_MODULES
define("MODULES_TITLE", "Gestion des modules");
define("MODULES_TYPE", "Type de module");
define("MODULES_DELETE_CONFIRM", "Supprimer ce(s) module(s) ?");
?>
