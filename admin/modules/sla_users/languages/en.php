<?php
/**
* @package		SLASH-CMS
* @subpackage	EN USERS MODULE LANGUAGES
* @internal     English users module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_USERS
define("USERS_TITLE", "Users config");
define("CATEGORIES_DELETE_CONFIRM", "Delete this users ?");
define("USERS_GR_ADMINISTRATOR", "Administrator");
define("USERS_GR_MANAGEMENT", "Management");
define("USERS_GR_REDACTION", "Redaction");
define("USERS_NO_GROUP", "No group");
define("USERS_ERROR_LOGIN_EXIST", "This login is already exist !");
define("USERS_ERROR_PWD_NOT_SIMIL", "Password is not identic !");
define("USERS_ERROR_NO_PERMIT", "Not allowed ! ");
define("USERS_ERROR_DEL_CURRENT_ACCOUNT", "You can't disable or delete your own account ! ");
define("USERS_LANGUAGE", "Language");
define("USERS_ENGLISH", "English");
define("USERS_FRENCH", "French");
define("USERS_ERROR_ONLY_YOUR_SETTING", "Your access rights allow you to only set up your account setting");
?>
