<?php
/**
* @package		SLASH-CMS
* @subpackage	FR NEWS MODULE LANGUAGES
* @internal     French news module translate
* @version		fr.php - Version 10.2.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_NEWS
define("NEWS_TITLE", "Gestion des news");
define("NEWS_DELETE_CONFIRM", "Supprimer la / les news ?");
define("NEWS_IN_PROGRESS", "En cours");
define("NEWS_PAST", "Expir&eacute;es");
define("NEWS_PUBLISH_DATE", "Date de publication");
define("NEWS_PUBLISH_TIME", "Heure de publication");
define("NEWS_UNPUBLISH_DATE", "Date de d&eacute;publication");
define("NEWS_UNPUBLISH_TIME", "Heure de d&eacute;publication");
define("NEWS_PERMANENT", "News permanente");
?>
