<?php
/**
* @package		SLASH-CMS
* @subpackage	EN NEWS MODULE LANGUAGES
* @internal     English news module translate
* @version		en.php - Version 10.2.2
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_NEWS
define("NEWS_TITLE", "News config");
define("NEWS_DELETE_CONFIRM", "Delete this news ?");
define("NEWS_IN_PROGRESS", "In progress");
define("NEWS_PAST", "Expire");
define("NEWS_PUBLISH_DATE", "Publish date");
define("NEWS_PUBLISH_TIME", "Publish time");
define("NEWS_UNPUBLISH_DATE", "Unpublish date");
define("NEWS_UNPUBLISH_TIME", "Unpublish time");
define("NEWS_PERMANENT", "News permanent");
?>
