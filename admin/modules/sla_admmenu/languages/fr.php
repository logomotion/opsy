<?php
/**
* @package		SLASH-CMS
* @subpackage	FR ADMMENU MODULE LANGUAGES
* @internal     French secure module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_ADMMENU
define("ADMMENU_DECONNEXION_TEXT", "D&eacute;connexion");
define("ADMMENU_CONNEXION_USERNAME", "Connect&eacute; en tant que");
?>
