<?php
/**
* @package		SLASH-CMS
* @subpackage	EN ADMMENU MODULE LANGUAGES
* @internal     English admmenu module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_ADMMENU
define("ADMMENU_DECONNEXION_TEXT", "Disconnect");
define("ADMMENU_CONNEXION_USERNAME", "Connected on");
?>
