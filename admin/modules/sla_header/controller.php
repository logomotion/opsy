<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_HEADER
* @internal     Admin header module
* @version		sla_header.php - Version 11.5.31
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


include ("views/default/view.php");

class sla_header_controller extends slaController implements iController{

	
	public $view;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function sla_construct() {
      
	   $this->view = new sla_header_view($this);
	}
	
	
	
	/**
	* Initialise function # require by slash-cms #
	*/
	public function initialise() {
		
		//echo $this->slash->sl_config("global_keywords");
		$this->view->scripts();
	
	}
	

	
	/**
	* Load function # require by slash-cms #
	*/
	public function load() {
		$this->view->title();
		$this->view->metas();
	}
	
	

}





?>