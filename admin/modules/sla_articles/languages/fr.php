<?php
/**
* @package		SLASH-CMS
* @subpackage	FR ARTICLES MODULE LANGUAGES
* @internal     French articles module translate
* @version		fr.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/

//Module SLA_ARTICLES
define("ARTICLES_TITLE", "Gestion des articles");
define("ARTICLES_DELETE_CONFIRM", "Supprimer cet article ?");
?>
