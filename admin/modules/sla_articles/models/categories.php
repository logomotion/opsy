<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_ARTICLES
* @internal     Admin articles module
* @version		categories.php - Version 11.3.25
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class categories extends slaModel implements iModel{

	
	/**
	 * Load categorie from database
	 */
	public function load_categories() {
		
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories ORDER BY title",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$categories = array();
		$i = 0;
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$categories[$i] = $row;
			$i++;
			
		}
		return $categories;
		
	}
	
}
?>