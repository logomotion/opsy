<?php
/**
* @package		SLASH-CMS
* @subpackage	SLA_ARTICLES
* @internal     Admin articles module
* @version		articles.php - Version 11.3.25
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


class articles extends slaModel implements iModel{

	
	/**
	* Load items
	*/
	public function load_items() {
		
		/* Order */
		$filter = "";
		if ($_SESSION[$this->controller->module_name."_categorie1"] != -1) {
			$filter = "WHERE id_categorie=".$_SESSION[$this->controller->module_name."_categorie1"]." ";
		}		
		if ($_SESSION[$this->controller->module_name."_search"] != "#") {
			if ($filter == ""){
				$filter = "WHERE title LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' OR content LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' ";
			}else{
				$filter .= "AND title LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' OR content LIKE '%".$_SESSION[$this->controller->module_name."_search"]."%' ";
			}
		}
			
		$result = mysql_query("SELECT id,title,content,id_categorie,id_user,date,enabled 
								FROM ".$this->slash->database_prefix."articles ".$filter."
								ORDER BY ".$_SESSION[$this->controller->module_name."_orderby"]." ".$_SESSION[$this->controller->module_name."_sort"],
		$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
		
		$objects = array();
		$obj_ids = array("id","title","content","id_categorie","id_user","date","enabled");
		$obj_titles = array("ID",
						$this->slash->trad_word("TITLE"),
						$this->slash->trad_word("CONTENT"),
						$this->slash->trad_word("CATEGORIE"),
						$this->slash->trad_word("AUTHOR"),
						$this->slash->trad_word("CREATION_DATE"),
						$this->slash->trad_word("ACTIVE"));
		$obj_sorts = array(false,true,true,false,false,true,false);
		$obj_sizes = array(5,20,20,20,10,15,5);
		$obj_actions = array(false,"single_edit","single_edit","single_edit","single_edit","single_edit","set_state");
		$obj_controls = array("single_edit","single_delete");

		while ($row = mysql_fetch_array($result, MYSQL_BOTH)) {
			/* CONTENT */
			$sl_txt = new sl_text();
			$h2t_content = new html2text($row[2]);
			$row[2] = $sl_txt->substring_word($h2t_content->get_text(),20,true);
			
			/* USER */
			$result_user = mysql_query("SELECT id,name FROM ".$this->slash->database_prefix."users WHERE id='".$row[4]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row_user = mysql_fetch_array($result_user, MYSQL_ASSOC);
			$row[4] = $row_user["name"];
			
			/* CATEGORIE */
			if ($row[3]) {
				$result_cat = mysql_query("SELECT id,title FROM ".$this->slash->database_prefix."categories WHERE id='".$row[3]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$row_cat = mysql_fetch_array($result_cat, MYSQL_ASSOC);
				$row[3] = $row_cat["title"];
			}else{
				$row[3] = $this->slash->trad_word("NONE");
			} 
			array_push($objects,$row);
		}
		
		//Load listing
		sl_interface::create_listing($this->controller->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,true);
		
	}
	
	

	/**
	 * Load categorie
	 * @param $id Categorie ID
	 */
	public function load_item($id) {
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."articles WHERE id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		return $row;
	}
	
	
	/**
	 * Delete categorie
	 * @param $id Categorie ID
	 */
	public function delete_items($id_array) {
		foreach ($id_array as $value) {
			if (file_exists("../medias/attachments/sl_articles/".$value)) {
				$this->delete_attachment("../medias/attachments/sl_articles",$value);
			}
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."articles WHERE id=".$value,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		}
	}
	
	
	/**
	 * Save categorie
	 */
	public function save_item($id,$values){
		
		if ($id != 0) {
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."articles set 
					id_user='".$_SESSION["id_user"]."',
					id_categorie='".$values["categorie"]."',
					title='".$values["title"]."',
					content='".$values["content"]."',
					enabled='".$values["enabled"]."' 
					WHERE id='".$values["id"]."'",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			return $this->slash->trad_word("EDIT_SUCCESS");	
			
		} else {
		
			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."articles
					(id,id_user,id_categorie,title,content,date,enabled) value
					('','".$_SESSION["id_user"]."','".$values["categorie"]."','".$values["title"]."','".$values["content"]."','".date ("Y-m-d H:i:s", time())."','".$values["enabled"]."')",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			$insert_id = mysql_insert_id();
					
			$ret = $this->save_attachment("../medias/attachments/sl_articles",$insert_id);
			
			if (!$ret){
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}else{
				return $this->slash->trad_word("SAVE_SUCCESS");
			}					
			
		}
		
	}
	
	/**
	 * Save attachment 
	 */
	public function save_attachment($destination,$id_element){
		
		$result_files = mysql_query("SELECT * FROM ".$this->slash->database_prefix."attachments WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0' ORDER BY position",$this->slash->db_handle) 
		or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());	
		
		if (mysql_num_rows($result_files) > 0) {
		
			$sl_files_sv = new sl_files();
			
			if (!$sl_files_sv->make_dir($destination."/".$id_element)){
				return false;
			}
			
			
			
			while ($row = mysql_fetch_array($result_files, MYSQL_BOTH)) {
				if (!$sl_files_sv->move_files("../tmp/".$row["filename"],$destination."/".$id_element."/".$row["filename"])){
					return false;
				}
			}
			
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."attachments set 
					id_element='".$id_element."',
					state='1'
					WHERE id_user='".$_SESSION["id_user"]."' and id_module='".$this->controller->module_id."' and state='0'
					",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		
			return true;
		}else{
			return true;
		}
	}
	
	/**
	* Delete attachment
	*/
	public function delete_attachment($destination,$id_element){
	
		$sl_files_dl = new sl_files();
		
		if ($sl_files_dl->remove_dir($destination."/".$id_element)) {
			$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."attachments WHERE id_module=".$this->controller->module_id." AND id_element='".$id_element."' and state=1",$this->slash->db_handle) 
									or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			return true;
		}else{
			return false;
		}
				
	}
	
	/**
	 * Set is enabled
	 * @param $id article ID
	 */
	public function set_items_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."articles set enabled=".$enabled." WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	
	/**
	* Recovery fields value
	*/
	public function recovery_fields() {
	
		$obj = array();
		$obj["id"] = $this->slash->sl_param($this->controller->module_name."_id_obj","POST");
		$obj["title"] = $this->slash->sl_param($this->controller->module_name."_obj1","POST");
		$obj["categorie"] = $this->slash->sl_param($this->controller->module_name."_obj2","POST");
		$obj["content"] = $this->slash->sl_param($this->controller->module_name."_obj3","POST");
		$obj["enabled"] = $this->slash->sl_param($this->controller->module_name."_obj5","POST");
		
		return $obj;
		
	}
	
	
	/**
	* Check add/edit values
	* @param $values:Array Object Values
	*/
	public function check_fields($values) {
		
		$mess = array();
		
		/*
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."categories WHERE title='".$values["title"]."' AND id !='".$values["id"]."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		if (mysql_num_rows($result)>0) {
			$mess[0]["message"] = $this->slash->trad_word("CATEGORIES_ERROR_EXIST");
		}
		*/
		//$mess[1]["message"] =  $this->slash->trad_word("ERROR_TITLE_FIELD_EMPTY");
		
		if (count($mess) > 0){ return $mess; } else { return null; }
	
	}
	
}

?>