<?php
/**
* @package		SLASH-CMS
* @subpackage	sla_menu
* @internal     Admin menu config module
* @version		sla_menu_view.php - Version 9.12.15
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/


/*
TODO Liste des menus
*/

class sla_menu_view implements iView{


	/**
	 * Show HTML Header
	 */
	protected function header () {
		
		//<link rel="stylesheet" type="text/css" href="css/superfish.css" media="screen">
		//echo "<link rel='stylesheet' type='text/css' href='templates/system/css/sla_menu.css' media='screen'>";
		//echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/superfish/js/hoverintent.js'></script> \n";
		/*
		echo "<link rel='stylesheet' type='text/css' href='modules/sla_menu/views/default/css/sla_menu.css' media='screen'>";*/
		echo "<link rel='stylesheet' type='text/css' href='../core/plugins/jquery_plugins/tabs/css/tabs.css' media='screen'>";
		echo "<link rel='stylesheet' type='text/css' href='../core/plugins/jquery_plugins/lightbox/css/lightbox.css' media='screen'>";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/ui/js/ui.core.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/tabs/js/tabs.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/interface/js/interface.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/pager/js/pager.js'></script> \n";
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/lightbox/js/lightbox.js'></script> \n";
		
		echo "<script type='text/javascript' src='../core/plugins/jquery_plugins/preload/js/preloadCssImages.js'></script> \n";
		
		echo "<script type='text/javascript' src='../core/functions/sl_javascript.js'></script> \n";
		
		
		
	}
	
	
	/**
	 * Show menu_config module
	 * @param $message message
	 */
	protected function show_menu_config($message="") {
		
		echo "<form name='".$this->module_name."_nav_form' method=post action='index.php?mod=".$this->module_name."'>
			  <input type='hidden' id='".$this->module_name."_act' name='".$this->module_name."_act' value=''>
			  <input type='hidden' id='".$this->module_name."_valid' name='".$this->module_name."_valid' value=''>
			  <input type='hidden' id='".$this->module_name."_tab' name='".$this->module_name."_tab' value='".$_SESSION[$this->module_name."_tab"]."'>
			  <input type='hidden' id='".$this->module_name."_menu' name='".$this->module_name."_menu' value='".$_SESSION[$this->module_name."_menu"]."'>";
			 
			sl_interface::listing_hidden_fields($this->module_name);	
		
		echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='sl_mod_title' align='left' width='50%'>".$this->slash->trad_word("MENU_CONFIG_TITLE")."
						";
						
						if ($message != "") { 
							echo "<div id='message' style='overflow: visible; opacity: 100; display: block; position:fixed; width:100%; top: 100px; margin-left:auto; margin-right:auto; ' >
									<table align='center'>
									<tr><td class='sl_mod_message' align='center'>".$message."</td></tr></table>
								  </div>";
						}
						
				 echo "</td>
				 		
				 		
							
					</tr>
					</table>
					
					
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='mod_menu_config'>		
							<div id='tab_container'>
					            <ul>
					                <li><a href='#tab_1'><span>".$this->slash->trad_word("MENU_CONFIG_MENUS")."</span></a></li>
					                <li><a href='#tab_2'><span>".$this->slash->trad_word("MENU_CONFIG_LINKS")."</span></a></li>
					                <!--<li><a href='#tab_3'><span>".$this->slash->trad_word("MENU_CONFIG_CONFIG")."</span></a></li>-->
					            </ul>
					          <div id='tab_1'>";     
					    	 	$this->show_tab(1);
						echo "</div>
							  <div id='tab_2'>";
					            $this->show_tab(2); 		
						echo "</div>
					          <!--<div id='tab_3'>-->";
								//$this->show_tab(3);
						echo "<!--</div>-->
					       
					       </div>

							</td>
					</tr>
					</table>	
								
			</td>
			</tr></table>
			</form>	
					";
					
	}
		
		
	/**
	 * Show menus list
	 * @param $menus menus array
	 */
	protected function show_tab_menus($menus) {
		echo "<span style='color:#FF0000;'>".$this->slash->trad_word("CONTROL_IS_LOCK")." !</span><br /><br />";
		echo $this->slash->trad_word("MENU_CONFIG_LIST_MENUS")." : <br />";
		
		for ($i=0; $i<count($menus); $i++ ) {
			
			echo "- <b> ".$menus[$i]["title"]."</b><br />";
			
		}
	
	}
	
	/**
	 * Show links
	 */
	protected function show_tab_links() {
		
		echo "<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
	    		<td class='mod_menu_config_control'>
					<table align='right'>
						<tr>
							<td align='center' width='20%'><a href='javascript:void(0);' class='add_button'
								onClick=\"javascript:submitForm('sla_menu','add');\"></a>".$this->slash->trad_word("ADD")."</td>
							
							<td align='center' width='20%'><a href='javascript:void(0);' class='edit_button'
								onClick=\"javascript:submitForm('sla_menu','edit');\"></a>".$this->slash->trad_word("EDIT")."</td>														
					
							<td align='center' width='20%'><a href='javascript:void(0);' class='publish_button'
								onClick=\"javascript:submitForm('sla_menu','set_enabled');\"></a>".$this->slash->trad_word("ENABLED")."</td>		
							
							<td align='center' width='20%'><a href='javascript:void(0);' class='unpublish_button'
								onClick=\"javascript:submitForm('sla_menu','set_disabled');\"></a>".$this->slash->trad_word("DISABLED")."</td>
							
							<td align='center' width='20%'><a href='javascript:void(0);' class='delete_button'
								onClick=\"javascript:submitForm('sla_menu','delete');\"></a>".$this->slash->trad_word("DELETE")."</td>
				   			
						 </tr>
					</table>
													
										
										
				</td></tr></table>
				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='sl_mod_list'>";
								
			$this->load_links();
							
			
			echo "</td>
					</tr>
					</table>";
              		 	         
         
	}
	
	
	/**
	 * Show config
	 */
	protected function show_tab_config() {
		echo "<span style='color:#FF0000;'>".$this->slash->trad_word("CONTROL_IS_LOCK")." !<br /></span><br />";
		
	}
	
	
	/**
	 * HTML footer
	 */
	protected function footer_menu_config() {
	
		
		echo "
		<script type='text/javascript'> 
 
 			 	
 	
			$(document).ready(function(){ 

			
			$('.lightbox').lightbox();		
			
			$('#message').css('opacity',1);
			$('#message').stopAll().pause(5000).fadeTo(400,0, function() { $(this).hide(); } );
		
			$.preloadCssImages();
		

			tabs = $('#tab_container > ul').tabs({ selected: ".$_SESSION[$this->module_name."_tab"]." });

				
				
		}); 
 	
 		
		</script>";
	
	}
	
	
	
	/**
	 * Show menu_config form
	 */
	protected function show_form ($id=0,$message="") {
		
		if ($_SESSION["sla_menu_image_temp"] == 1 && $id!=0) {
				$id = $_SESSION["sla_menu_image_id_temp"];
		
		}
		
		if ($id != 0) {
			$image = $this->load_image($id);
			$title = $this->slash->trad_word("CATEGORIES_TITLE")." >>> <span class='mod_undertitle'>".$this->slash->trad_word("EDIT")."</span>";
			$image_title = $this->slash->trad_word("MENU_CONFIG_IMAGE_FILE_EDIT");
		} else {
			$image = null;
			$title = $this->slash->trad_word("CATEGORIES_TITLE")." >>> <span class='mod_undertitle'>".$this->slash->trad_word("ADD")."</span>";
			$image_title = $this->slash->trad_word("MENU_CONFIG_IMAGE_FILE_ADD");
			
			
		}
		
		if ($_SESSION["sla_menu_image_temp"] == 1 && $id==0) {
				$id = $_SESSION["sla_menu_image_id_temp"];
				$image = array();
				$image["description"] = $_SESSION["sla_menu_image_description_temp"];		
		}
		
		
		if (file_exists("../modules/sl_menu_config/images/thumbs/".$id.".jpg")) {
			
			$thumbnail = "<a href='../modules/sl_menu_config/images/full/".$id.".jpg' class='lightbox' title='".substr(htmlentities($image["description"]), 0, 25)."...'>
            	<img src='../modules/sl_menu_config/images/thumbs/".$id.".jpg' class='mod_menu_config_thumb' /></a>";
		}else {
			$thumbnail = "<img src='../modules/sl_menu_config/images/empty.jpg' class='mod_menu_config_thumb' />";
		}
		
		
		
		echo "
				<form name='sla_menu_add_form' method=post action='index.php?mod=sla_menu' enctype='multipart/form-data'>
				<input type='hidden' id='sla_menu_act' name='sla_menu_act' value='save'>
				<input type='hidden' id='sla_menu_id_image' name='sla_menu_id_image' value='".$id."'>";
				
				if ($message != "") { 
					echo "<div id='message' style='overflow: visible; opacity: 100; display: block; position:fixed; width:100%; top: 100px; margin-left:auto; margin-right:auto; ' >
							<table align='center'>
							<tr><td class='mod_message' align='center'>$message</td></tr></table>
						  </div>";
				}
						
		echo "	<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='mod_title'>".$title."</td>
					
						<td class='mod_control'>
							<table align='right' width='200'>
							<tr>
								<td align='center' width='50%'>	
									<a href='javascript:void(0);' class='apply_button' onClick=\"";
	
							if ($id != 0) { 
								echo "submitForm('sla_menu','add_apply');"; 
							}else {
								echo "if (document.sla_menu_add_form.sla_menu_image_file.value != '') {submitForm('sla_menu','add_apply');		
										}else{alert('".$this->slash->trad_word("MENU_CONFIG_IMAGE_NEED_MESSAGE")."');}";
							}
								
								echo "	\"></a>
													".$this->slash->trad_word("SAVE")."</td>
									
								<td align='center' width='50%'>			
									<a href='index.php?mod=sla_menu' class='undo_button'></a>
									".$this->slash->trad_word("BACK")."			
								</td>
							</tr>
							</table>	
						</td>
					</tr>
					</table>
					
					<br />
					<table width='600' cellspacing='0' cellpadding='10' border='0' align='center' style='border:1px solid #333333;'>	
					<tr>
						<td align='center'>		
							<table width='600' cellspacing='0' cellpadding='10' border='0' >
							<tr>
								<td align='left' class='mod_field_title'>".$this->slash->trad_word("CATEGORIE")." : 
										<select id='sla_menu_image_categorie' name='sla_menu_image_categorie'>";
					
								$row_categories = $this->load_categories();	
					
								for ($i=0; $i<count($row_categories); $i++ ) {
									if ($image["parent"] == $row_categories[$i]["id"] && $id!=0) {
										echo "<OPTION VALUE='".$row_categories[$i]["id"]."' selected>".$row_categories[$i]["title"]."</OPTION>"; 
									}elseif ($_SESSION["sla_menu_categorie"]==$row_categories[$i]["id"] && $id==0) { 
										echo "<OPTION VALUE='".$row_categories[$i]["id"]."' selected>".$row_categories[$i]["title"]."</OPTION>"; 
									}else { 
										echo "<OPTION VALUE='".$row_categories[$i]["id"]."'>".$row_categories[$i]["title"]."</OPTION>";
									}
								}			
								echo "</select>";
										
										
						echo "	</td>
							</tr>
							</table>
					</tr>
					<tr>
						<td align='center'>		
							<table width='600' cellspacing='0' cellpadding='10' border='0' >
							<tr>
								<td align='left' class='mod_field_title'>".$this->slash->trad_word("DESCRIPTION")." : </td>
							</tr>
									
							<tr>
								<td align='left'><input size='80' maxlength='50' id='sla_menu_image_description' name='sla_menu_image_description' value='".$image["description"]."' 
										onKeyDown='document.getElementById(\"caract_count\").innerHTML  = 50 - document.sla_menu_add_form.sla_menu_image_description.value.length;' 
										onkeyup='document.getElementById(\"caract_count\").innerHTML  = 50 - document.sla_menu_add_form.sla_menu_image_description.value.length;' 
									 	onfocus='document.getElementById(\"caract_count\").innerHTML  = 50 - document.sla_menu_add_form.sla_menu_image_description.value.length;'/>
									 	<div id='caract_count' class='mod_menu_config_maxlength'></div></td>
							</tr>
							</table>
					</tr>
					<tr>
						<td align='center'>		
							<table width='600' cellspacing='0' cellpadding='10' border='0' >
							<tr>
								<td align='left' class='mod_field_title'>".$image_title." : <input type='file' name='sla_menu_image_file' size='46' /><br /></td>
							</tr>";
							
							if ($id != 0) {	
								
								echo"	
									<tr>
										<td align='left' class='mod_field_title'>".$this->slash->trad_word("THUMBNAIL")." :</td>
									</tr>
									<tr>
										<td align='left' class='mod_field_title'>".$thumbnail."</td>
									</tr>";
									
							}
							
						echo "</table>
					</tr>

					</table>	
								
			</td>
			</tr></table></form>

				";
			
			
		
	}
	
	
	/**
	 * Show categorie form
	 */
	protected function show_delete ($id_array) {
		
		echo "	<form name='sla_menu_del_form' method=post action='index.php?mod=sla_menu'>
				<input type='hidden' id='sla_menu_act' name='sla_menu_act' value='delete'>
				<input type='hidden' id='sla_menu_valid' name='sla_menu_valid' value='1'>
				
				<table width='100%' cellspacing='0' cellpadding='0' border='0'>
				<tr>
				<td>
					<table width='100%' cellspacing='0' cellpadding='0' border='0'>
					<tr>
						<td class='mod_title'>".$this->slash->trad_word("CATEGORIES_TITLE")."</td>
					
						<td class='mod_control'>
							<table align='right' width='200'>
							<tr>
								<td align='center' width='50%'>
										
									<a href='javascript:void(0);' class='del_button' onClick=\"javascript:submitForm('sla_menu','del_apply');\"></a>
									".$this->slash->trad_word("DELETE")."</td>
									
								<td align='center' width='50%'>			
									<a href='index.php?mod=sla_menu' class='undo_button'></a>
									".$this->slash->trad_word("BACK")."				
								</td>

							</tr>
							</table>	
						</td>
					</tr>
					</table>
					
					<br />
					<table width='600' cellspacing='0' cellpadding='10' border='0' align='center' style='border:1px solid #333333;'>
					<tr>
						<td align='left'>		
							<table width='600' cellspacing='0' cellpadding='0' border='0' >
							<tr>
								<td align='left' class='mod_field_title' width='50%'>".$this->slash->trad_word("MENU_CONFIG_DELETE_CONFIRM")." 
								</td>
							</tr>";
					
					
					
					
							
					if (count ($id_array) != 0) {
						$count=0;		
						foreach ($id_array as $value) {
								
								$image = $this->load_image($value);
								$h2t_description =& new html2text($image["description"]);
								
								echo "<tr><td align='left' class='mod_delete_text' width='50%'>
										<input type='checkbox' id='sla_menu_checked[".$count."]' name='sla_menu_checked[".$count."]' value='".$image["id"]."' checked style='display:none;'  />
										".sl_substring($h2t_description->get_text(),30,true)."
										</td>
									</tr>";
								$count++;
						}
						
					}
						echo"	</table>
						</td>
					</tr>
					
					</table>	
								
			</td>
			</tr></table></form>

				";
			
			
		
	}
	
	
	
}





?>