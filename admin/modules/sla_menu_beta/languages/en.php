<?php
/**
* @package		SLASH-CMS
* @subpackage	EN MENU_CONFIG  MODULE LANGUAGES
* @internal     English MENU_CONFIG module translate
* @version		en.php - Version 10.1.19
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		GNU/GPL
*/


//Module SLA_MENU_CONFIG
define("MENU_CONFIG_TITLE", "MENU_CONFIG configuration");
define("MENU_CONFIG_MENUS", "Menus");
define("MENU_CONFIG_LINKS", "Links");
define("MENU_CONFIG_CONFIG", "Config");
define("MENU_CONFIG_CATEGORIES_SELECT", "Categorie");
define("MENU_CONFIG_IMAGE_FILE_ADD", "Add image");
define("MENU_CONFIG_IMAGE_FILE_EDIT", "Replace image");
define("MENU_CONFIG_IMAGE_NEED_MESSAGE", "You must be upload a image");
define("MENU_CONFIG_EXTENSION_ACCEPT", "Extensions autorised");
define("MENU_CONFIG_DELETE_CONFIRM","Delete images ?");
define("MENU_CONFIG_OVER_PICTURE_QUOTA","Too many image");
define("MENU_CONFIG_PICTURE_SIZE_MIN","Images min size");
define("MENU_CONFIG_PICTURE_SIZE_MAX","Images max size");
define("MENU_CONFIG_PICTURE_WEIGHT_MAX","Images max weight");
define("MENU_CONFIG_PICTURE_QUOTA_MAX","Images allowed quota");
define("MENU_CONFIG_PICTURE_CURRENT_QUOTA","Current quota");
define("MENU_CONFIG_SIZE_OPTIMAL","Optimal image size is 410/308 pixels");
define("MENU_CONFIG_LIST_MENUS","Menus list");
?>
