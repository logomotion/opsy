 <?php
/**
* @package		SLASH-CMS
* @subpackage	sla_menu
* @internal     Admin menu config module
* @version		sla_menu.php - Version 9.12.16
* @author		Julien Veuillet [http://www.wakdev.com]
* @copyright	Copyright(C) 2009 - Today. All rights reserved.
* @license		CLUF
*/

/*
TODO Liste des menus
*/


// Include Module default View
include ("views/default/sla_menu_view.php");

class sla_menu extends sla_menu_view implements iController{

	public $slash; //Core Reference
	public $params;
	public $module_name = "sla_menu";
	public $module_id;
	
	/**
	* Contructor
	* @param core_class_ref Core class reference
	*/
	function __construct(&$core_class_ref,$module_id) {
       $this->slash = $core_class_ref;
       $this->module_id = $module_id;
	}
	
	
	/**
	* Initialise function # require by slash-cms #
	*/
	public function initialise() {
		//no global initialisation for this module		
	}
	
	/**
	 * Load Header function # require by slash-cms #
	 */
	public function load_header(){
		
		 //SESSION
		
		if (isset($_SESSION[$this->module_name."_tab"]) == false) { $_SESSION[$this->module_name."_tab"] = 0;}
		if (isset($_SESSION[$this->module_name."_menu"]) == false) { $_SESSION[$this->module_name."_menu"] = -1;}
		if (isset($_POST[$this->module_name."_tab"])) { $_SESSION[$this->module_name."_tab"] = $_POST[$this->module_name."_tab"];}
		if (isset($_POST[$this->module_name."_menu"])) { $_SESSION[$this->module_name."_menu"] = $_POST[$this->module_name."_menu"];}
		
		sl_interface::listing_sessions($this->module_name,array('position'));
		
		$this->header(); //show script header
		
	}
	
	/**
	 * Load footer function # require by slash-cms #
	 */
	public function load_footer(){
		$this->footer_menu_config();
	}
	
	/**
	* Load module function # require by slash-cms #
	*/
	public function load() {
		
		
		
		if ($_SESSION[$this->module_name."_tab"] == 0) { // Menus Tab Action
			// Switch Action In development
			$this->show_menu_config();
			
		}
		
		
		if ($_SESSION[$this->module_name."_tab"] == 1) { // Images Tab Action
			
		
			switch ($this->slash->sl_param($this->module_name."_act","POST")) {
				
				case "add":
				
					
					$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					$num_rows = mysql_num_rows($result);
					
					if (intval($this->load_config("max_pictures")) > $num_rows || $this->load_config("max_pictures") == 0 ) {
						$this->show_form();
					}else{
						$this->show_menu_config($this->slash->trad_word("MENU_CONFIG_OVER_PICTURE_QUOTA")." : ".$this->slash->trad_word("MAX")." ".$this->load_config("max_pictures"));
					}
					
					
				break;
				
				case "edit":
					$values = $this->slash->sl_param($this->module_name."_checked","POST");
					if (isset ($values) && count($values) > 0) {
						reset($values);
						$this->show_form(current($values));
					}else{
						$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
					}
				break;
				
				case "set_enabled":
					
						$values = $this->slash->sl_param($this->module_name."_checked","POST");
						
						if (isset ($values) && count($values) > 0) {
							$this->set_link_enabled($this->slash->sl_param($this->module_name."_checked","POST"),1);
							$this->show_menu_config($this->slash->trad_word("ITEM_ENABLE_SUCCESS"));
						}else{
							$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
						}
				break;
				
				case "set_disabled":
					
						$values = $this->slash->sl_param($this->module_name."_checked","POST");
						
						if (isset ($values) && count($values) > 0) {
							$this->set_link_enabled($this->slash->sl_param($this->module_name."_checked","POST"),0);
							$this->show_menu_config($this->slash->trad_word("ITEM_DISABLE_SUCCESS"));
						}else{
							$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
						}
				break;
				
				
				case "set_up":
					
					$values = $this->slash->sl_param($this->module_name."_checked","POST");
					if (isset ($values) && count($values) > 0) {
						reset($values);
						$this->set_link_position(current($values),"up");
						$this->show_menu_config($this->slash->trad_word("ITEM_UP_SUCCESS"));
					}else{
						$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
					}
				break;
				
				case "set_down":
					
						$values = $this->slash->sl_param($this->module_name."_checked","POST");
					if (isset ($values) && count($values) > 0) {
						reset($values);
						$this->set_link_position(current($values),"down");
						$this->show_menu_config($this->slash->trad_word("ITEM_DOWN_SUCCESS"));
					}else{
						$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
					}
				break;
				
				
				case "save":
					
					$result = $this->save_link();
					
					if ($result == 1) { 
									
						if ($this->slash->sl_param($this->module_name."_id_cat","POST") != 0) {
							$this->show_menu_config($this->slash->trad_word("EDIT_SUCCESS"));
						}else{
							$this->show_menu_config($this->slash->trad_word("SAVE_SUCCESS"));
						}
					}else{	
						
						$_SESSION[$this->module_name."_link_temp"] = 1;
						$_SESSION[$this->module_name."_link_description_temp"] = $this->slash->sl_param($this->module_name."_link_description","POST");
						$_SESSION[$this->module_name."_link_id_temp"] = $this->slash->sl_param($this->module_name."_id_link","POST");
						
						if ($_SESSION[$this->module_name."_link_id_temp"] != 0) {	
							$this->show_form($_SESSION[$this->module_name."_link_id_temp"],$result);
						}else{
							$this->show_form(0,$result);
						}
	
						
					}
					
				break;
				
							
				case "delete":
					if ($this->slash->sl_param($this->module_name."_valid","POST")) {
						
						$this->delete_links($this->slash->sl_param($this->module_name."_checked","POST"));
						$this->show_menu_config($this->slash->trad_word("DELETE_SUCCESS"));
						
					}else {
						
						$values = $this->slash->sl_param($this->module_name."_checked","POST");
						
						if (isset ($values) && count($values) > 0) {
							$this->show_delete($this->slash->sl_param($this->module_name."_checked","POST"));
						}else{
							$this->show_menu_config($this->slash->trad_word("SELECTION_REQUIRE"));
						}
						
					}
						
				break;
				
				default:
					
					$this->show_menu_config();
			}
		
		}
		
		/*
		if ($_SESSION["sla_menu_tab"] == 2) { // Config Tab Action
			// Switch Action In development
			
			
			$this->show_menu_config();
		}
		*/
		
		
		
		//destroy temp session
		unset($_SESSION[$this->module_name."_link_temp"]);
		unset($_SESSION[$this->module_name."_link_description_temp"]);
		
	}
	
	
	
	
	/**
	 * Show tab
	 * @param $id id of tab 
	 */
	protected function show_tab($id) {
		
		switch ($id) {
			
			case 1:
				$row_menus = $this->load_menus();
				$this->show_tab_menus($row_menus);
				break;
				
			case 2:
				 $this->show_tab_links();
				break;
			/*
			case 3 :
				
				$result = mysql_query("SELECT * FROM sl_menu WHERE type='image'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$num_rows = mysql_num_rows($result);
				
				$this->show_tab_config($num_rows);
				break;
			*/
		}
	}
	
	/**
	* Execute function # require by slash-cms #
	*/
	public function execute() {
		//Nothing
	}
	
	
	
	
	/* ---------------- */
	/* MODULE FUNCTIONS */
	/* ---------------- */
	
	
	
	/**
	 * Load menus from database
	 */
	protected function load_menus() {
		
		$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=1 AND enabled=1 ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$menus = array();
		$i = 0;
		while ($row = mysql_fetch_array($result, MYSQL_ASSOC)){
			$menus[$i] = $row;
			$i++;
			
		}
		return $menus;
		
	}
	
	
	
	
	/**
	 * Load child links
	 */
	protected function load_child_links(&$objects,$menu_id,$parent,$level) {
	
		$count=0;
		$result_link = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=2 AND menu_id=".$menu_id." AND parent=".$parent." ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		while ($row_link = mysql_fetch_array($result_link, MYSQL_BOTH)) {
		    
		    $row = array();
		    $row[0] = $row_link["id"];
			$row["id"] = $row_link["id"];
		    $row[1] = " ".$row_link["title"];
			
			 
		    for ($i=0; $i<$level; $i++){
		    $row[1] = "--".$row[1];
		    }
		   
		    $row[2] = $row_link["action"];
			$row[3] = $row_link["enabled"];
			
		    array_push($objects,$row); 

		    $count++;
		    
		    
		    $result_child = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE pri_type=2 AND menu_id=".$menu_id." AND parent=".$row_link["id"],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		    
		    $child_count = mysql_num_rows($result_child);
		    
		    if ($child_count > 0 ) {
			    $this->load_child_links($objects,$menu_id,$row_link["id"],$level+1);
		    }
					
		}
		
	
	}
	
	
	/**
	 * Load links from database
	 */
	protected function load_links() {
		

		if ($_SESSION[$this->module_name."_menu"] != -1) {
			$filter_menu = $_SESSION[$this->module_name."_menu"];
		} else {
			// TODO CHECK FIRST MENU \\
			$filter_menu= 1;
		}

		$objects = array();
		$obj_ids = array("id","title","action","enabled");
		$obj_titles = array("ID",
							$this->slash->trad_word("TITLE"),
							$this->slash->trad_word("DESCRIPTION"),
							$this->slash->trad_word("ACTIVE"));
		$obj_sorts = array(false,false,false,false);
		$obj_sizes = array(5,40,40,5);
		$obj_actions = array(false,"single_edit","single_edit","set_state");
		$obj_controls = array("single_up","single_down","single_edit","single_delete");
		
		$this->load_child_links($objects,$filter_menu,0,0);
		

		sl_interface::create_listing($this->module_name,$obj_ids,$obj_titles,$obj_sorts,$obj_sizes,$obj_actions,$objects,$obj_controls,true,true,true,false);
		

		
		
	}
	
	/**
	 * Load link menu
	 * @param $id link menu ID
	 */
	protected function load_link($id) {
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image' AND id=".$id,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$row = mysql_fetch_array($result, MYSQL_ASSOC);
			return $row;
	}
	
	
	/**
	 * Set is enabled
	 * @param $id image ID
	 */
	private function set_link_enabled($id_array,$enabled) {
			
			foreach ($id_array as $value) {
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set enabled=".$enabled." WHERE id='".$value."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			}
	}
	
	
	/**
	 * Set position
	 * @param $id image ID
	 */
	private function set_link_position($id,$order) {
			
			$link = $this->load_link($id);
			
			if ($order=="up") { $next_position = $link["position"]-1; } 
			if ($order=="down") { $next_position = $link["position"]+1; } 
			
			if ($order=="up" || $order=="down") {
				$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image' AND parent=".$link["parent"]." AND position=".$next_position,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$num_rows = mysql_num_rows($result);
				
				if ($num_rows != 0) {
					$row = mysql_fetch_array($result, MYSQL_ASSOC);
					$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set position=".($image["position"])." WHERE parent=".$link["parent"]." AND position=".$next_position,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set position=".$next_position." WHERE id=".$link["id"],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				}
			}
	}
	
	
	/**
	 * Save link
	 */
	private function save_link(){
		
		$link_id = $this->slash->sl_param("sla_menu_id_link","POST");
		$link_description = $this->slash->sl_param("sla_menu_link_description","POST");
		$link_menu = $this->slash->sl_param("sla_menu_link_menu","POST");
		$link_file = $this->slash->sl_param("sla_menu_image_file","POST");
		
		if ($link_id != 0) { // EDIT
			
			$my_link = $this->load_link($link_id);
			
			$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set 
					id_user=".$_SESSION["id_user"].",
					parent=".$link_menu.",
					description='".$link_description."'
					WHERE id=".$link_id,$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			
			// Categorie change = new position
			if ($my_link["parent"] != $link_menu) {
				
				$result = mysql_query("UPDATE ".$this->slash->database_prefix."menu set position=99999
						WHERE id=".$link_id,$this->slash->db_handle) 
						or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				$this->reloadLinkPosition();
			}
			
			/*
			if (isset($_FILES['sla_menu_image_file']['tmp_name']) && $_FILES['sla_menu_image_file']['tmp_name'] != "") {
				
				$id_array = array();
				$id_array[0] = $link_id;
	
				$temp = '../modules/sl_menu/images/temp/'; // work directory
				$full = '../modules/sl_menu/images/full/'; // full size directory
				$small = '../modules/sl_menu/images/small/'; // small size directory
				$thumbs = '../modules/sl_menu/images/thumbs/'; // thumbs size directory
				
				if (file_exists($temp."image.temp")) {unlink($temp."image.temp"); }
				
			    $tmp_file = $_FILES['sla_menu_image_file']['tmp_name'];
			
				// Check is uploaded
			   	if( !is_uploaded_file($tmp_file) ) { 
			   		return $this->slash->trad_word("NO_FILE_FOUND"); 
			   	}
			
			    // Check extension
			    $ext = array ();
			    $ext = split(",",$this->load_config("upload_mime"));
				$type_file = $_FILES['sla_menu_image_file']['type'];
				if (!sl_check_files_extension($type_file,$ext)) { 
					return $this->slash->trad_word("INVALID_IMAGE_FILE"); 
				}
				
				
				// Check max and min size image
				$ret = sl_check_image_spec ($tmp_file,
											$this->load_config("max_weight"),
											$this->load_config("min_width"),
											$this->load_config("min_height"),
											$this->load_config("max_width"),
											$this->load_config("max_height")
											);
				if ($ret <= 0 ) {
					
					switch ($ret) {
					
						case 0:
							return $this->slash->trad_word("FILE_TRANFERT_FAIL"); 
						break;
						
						case -1:
							return $this->slash->trad_word("INVALID_FILE_SIZE")." : ".$this->slash->trad_word("MAX")." ".$this->load_config("max_weight")." Ko"; 
						break;
						
						case -2:
						case -3:
							return $this->slash->trad_word("INVALID_IMAGE_WIDTH")." : ".
							$this->slash->trad_word("MIN")." ".$this->load_config("min_width")."px - ".
							$this->slash->trad_word("MAX")." ".$this->load_config("max_width")."px ";
						break;
						
						case -4:
						case -5:
							return $this->slash->trad_word("INVALID_IMAGE_HEIGHT")." : ".
							$this->slash->trad_word("MIN")." ".$this->load_config("min_height")."px - ".
							$this->slash->trad_word("MAX")." ".$this->load_config("max_height")."px ";
						break;
						
						default:
						return $this->slash->trad_word("INVALID_IMAGE_FILE"); 
					}
				}
				

			    // Move image
			    $name_file = "image.temp";
			    if(!move_uploaded_file($tmp_file,$temp.$name_file)){ 
			    	return $this->slash->trad_word("FILE_TRANFERT_FAIL"); 
			    }
			
				if (file_exists($full.$image_id.".jpg")) {unlink($full.$image_id.".jpg"); }
				if (file_exists($small.$image_id.".jpg")) {unlink($small.$image_id.".jpg"); }
				if (file_exists($thumbs.$image_id.".jpg")) {unlink($thumbs.$image_id.".jpg"); }
				
				
			  	// Format conversion
				if (!sl_create_image($temp.$name_file,$thumbs.$image_id.".jpg",80,60,2))  {
					return $this->slash->trad_word("FILE_TRANFERT_FAIL");
				}
				if (!sl_create_image($temp.$name_file,$full.$image_id.".jpg","auto","auto",2))  {
					return $this->slash->trad_word("FILE_TRANFERT_FAIL");
				}
				if (!sl_create_image($temp.$name_file,$small.$image_id.".jpg",410,308,2))  {
					return $this->slash->trad_word("FILE_TRANFERT_FAIL");
				}
			
			}*/
			
			return 1;
					
		} else { //ADD
			
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image' AND parent=".$image_categorie,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
			$num_rows = mysql_num_rows($result);

			$result = mysql_query("INSERT INTO ".$this->slash->database_prefix."menu
					(id,type,parent,position,description,id_user,enabled) value
					('','image',".$image_categorie.",".($num_rows+1).",'".$image_description."',".$_SESSION["id_user"].",1)",$this->slash->db_handle) 
					or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
					
			$add_id = mysql_insert_id();
			
			$id_array = array();
			$id_array[0] = $add_id;
			
			/*
			$temp = '../modules/sl_menu/images/temp/'; // work directory
			$full = '../modules/sl_menu/images/full/'; // full size directory
			$small = '../modules/sl_menu/images/small/'; // small size directory
			$thumbs = '../modules/sl_menu/images/thumbs/'; // thumbs size directory
			
			if (file_exists($temp."image.temp")) {unlink($temp."image.temp"); }
			
		    $tmp_file = $_FILES['sla_menu_image_file']['tmp_name'];
		
			// Check is uploaded
		   	if( !is_uploaded_file($tmp_file) ) { 
		   		$this->delete_images($id_array);
		   		return $this->slash->trad_word("NO_FILE_FOUND"); 
		   	}
		
		    // Check extension
		    $ext = array ();
		    $ext = split(",",$this->load_config("upload_mime"));
			$type_file = $_FILES['sla_menu_image_file']['type'];
			if (!sl_check_files_extension($type_file,$ext)) { 
				$this->delete_images($id_array);
				return $this->slash->trad_word("INVALID_IMAGE_FILE"); 
			}
			
			// Check max and min size image
			$ret = sl_check_image_spec ($tmp_file,
										$this->load_config("max_weight"),
										$this->load_config("min_width"),
										$this->load_config("min_height"),
										$this->load_config("max_width"),
										$this->load_config("max_height")
										);
			if ($ret <= 0 ) {
				$this->delete_images($id_array);
				switch ($ret) {
					
					case 0:
						return $this->slash->trad_word("FILE_TRANFERT_FAIL"); 
					break;
					
					case -1:
						return $this->slash->trad_word("INVALID_FILE_SIZE")." : ".$this->slash->trad_word("MAX")." ".$this->load_config("max_weight")." Ko"; 
					break;
					
					case -2:
					case -3:
						return $this->slash->trad_word("INVALID_IMAGE_WIDTH")." : ".
						$this->slash->trad_word("MIN")." ".$this->load_config("min_width")."px - ".
						$this->slash->trad_word("MAX")." ".$this->load_config("max_width")."px ";
					break;
					
					case -4:
					case -5:
						return $this->slash->trad_word("INVALID_IMAGE_HEIGHT")." : ".
						$this->slash->trad_word("MIN")." ".$this->load_config("min_height")."px - ".
						$this->slash->trad_word("MAX")." ".$this->load_config("max_height")."px ";
					break;
					
					default:
					return $this->slash->trad_word("INVALID_IMAGE_FILE"); 
				}
			}
			
			
		    // Move image
		    $name_file = "image.temp";
		    if(!move_uploaded_file($tmp_file,$temp.$name_file)){ 
		    	$this->delete_images($id_array);
		    	return $this->slash->trad_word("FILE_TRANFERT_FAIL"); 
		    }
		
		  	// Format conversion	
			if (!sl_create_image($temp.$name_file,$thumbs.$add_id.".jpg",80,60,2))  {
				$this->delete_images($id_array);
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}
			if (!sl_create_image($temp.$name_file,$full.$add_id.".jpg","auto","auto",2))  {
				$this->delete_images($id_array);
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}
			if (!sl_create_image($temp.$name_file,$small.$add_id.".jpg",410,308,2))  {
				$this->delete_images($id_array);
				return $this->slash->trad_word("FILE_TRANFERT_FAIL");
			}
				
			*/

			return 1;
		}
					
		
		
	}
	
	
	/**
	 * Delete links
	 * @param $id Links ID
	 */
	private function delete_links($id_array) {
			/*
			$temp = '../modules/sl_menu/images/temp/'; // work directory
			$full = '../modules/sl_menu/images/full/'; // full size directory
			$small = '../modules/sl_menu/images/small/'; // small size directory
			$thumbs = '../modules/sl_menu/images/thumbs/'; // thumbs size directory
			
			if (file_exists($temp."image.temp")) {unlink($temp."image.temp"); }
			*/
			
			foreach ($id_array as $value) {
				
				$result = mysql_query("DELETE FROM ".$this->slash->database_prefix."menu WHERE id=".$value,$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				
				/*
				if (file_exists($full.$value.".jpg")) {unlink($full.$value.".jpg"); }
				if (file_exists($small.$value.".jpg")) {unlink($small.$value.".jpg"); }
				if (file_exists($thumbs.$value.".jpg")) {unlink($thumbs.$value.".jpg"); }
				*/
			}
			
			$this->reloadLinkPosition();	
			
	}
	
	/**
	 * Reload Correct Link position
	 */
	private function reloadLinkPosition() {
		//Reload correct position By categorie
		$menus_array = $this->load_menus();
		
		for ($i=0; $i < count($menus_array); $i++) {

			$current_position=1;
			$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='image' AND parent='".$menus_array[$i]['id']."' ORDER BY position",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());

			while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
				
				mysql_query("UPDATE ".$this->slash->database_prefix."menu set position=".$current_position." WHERE id=".$row['id'],$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
				$current_position++;
			}
		}
	}
	
	
	/**
	* Load Gallery Config
	*/
	protected function load_config($name) {
	 	$result = mysql_query("SELECT * FROM ".$this->slash->database_prefix."menu WHERE type='config' AND enabled=1 AND title='".$name."'",$this->slash->db_handle) or $this->slash->show_fatal_error("QUERY_ERROR",mysql_error());
		$row = mysql_fetch_array($result, MYSQL_ASSOC);
		return $row["description"];
	}
	
	
					

}



?>
